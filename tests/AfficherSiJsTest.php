<?php

namespace Spip\Saisies\Tests;

use PHPUnit\Framework\TestCase;

/**
 * @covers saisies_afficher_si_js_est_statique()
 * @covers saisies_afficher_si_js()
 * @uses saisies_mapper_option()
 * @uses saisie_name2nom()
 * @uses spip_log
 * @uses spip_logger
 * @internal
 */
class AfficherSiJsTest extends TestCase {

	public static function dataAfficherSiJs() {
		require_once dirname(__DIR__) . '/inc/saisies_request.php';
		saisies_set_request('_etape', 2);
		saisies_set_request('a_dans_etape_1', 'oui');
		saisies_set_request('b_dans_etape_1/sous', 'oui');
		return [
			'standard' => [
				// Expected
				'afficher_si({&quot;champ&quot;:&quot;a&quot;,&quot;total&quot;:&quot;&quot;,&quot;operateur&quot;:&quot;==&quot;,&quot;valeur&quot;:&quot;oui&quot;,&quot;modificateur&quot;:[]})',
				// condition
				'@a@ == "oui"',
				// saisies_par_etapes
				[
					[
						'saisie' => 'input',
						'options' => [
							'nom' => 'a'
						]
					]
				]
			],
			'sous_saisie_nom_nom' => [
				// Expected
				'afficher_si({&quot;champ&quot;:&quot;a[sous]&quot;,&quot;total&quot;:&quot;&quot;,&quot;operateur&quot;:&quot;==&quot;,&quot;valeur&quot;:&quot;oui&quot;,&quot;modificateur&quot;:[]})',
				// condition
				'@a/sous@ == "oui"',
				// saisies_par_etapes
				[
					[
						'saisie' => 'input',
						'options' => [
							'nom' => 'a/sous'
						]
					]
				]
			],
			'sous_saisie_name_name' => [
				// Expected
				'afficher_si({&quot;champ&quot;:&quot;a[sous]&quot;,&quot;total&quot;:&quot;&quot;,&quot;operateur&quot;:&quot;==&quot;,&quot;valeur&quot;:&quot;oui&quot;,&quot;modificateur&quot;:[]})',
				// condition
				'@a[sous]@ == "oui"',
				// saisies_par_etapes
				[
					[
						'saisie' => 'input',
						'options' => [
							'nom' => 'a[sous]'
						]
					]
				]
			],
			'sous_saisie_nom_name' => [
				// Expected
				'afficher_si({&quot;champ&quot;:&quot;a[sous]&quot;,&quot;total&quot;:&quot;&quot;,&quot;operateur&quot;:&quot;==&quot;,&quot;valeur&quot;:&quot;oui&quot;,&quot;modificateur&quot;:[]})',
				// condition
				'@a/sous@ == "oui"',
				// saisies_par_etapes
				[
					[
						'saisie' => 'input',
						'options' => [
							'nom' => 'a[sous]'
						]
					]
				]
			],
			'sous_saisie_name_nom' => [
				// Expected
				'afficher_si({&quot;champ&quot;:&quot;a[sous]&quot;,&quot;total&quot;:&quot;&quot;,&quot;operateur&quot;:&quot;==&quot;,&quot;valeur&quot;:&quot;oui&quot;,&quot;modificateur&quot;:[]})',
				// condition
				'@a[sous]@ == "oui"',
				// saisies_par_etapes
				[
					[
						'saisie' => 'input',
						'options' => [
							'nom' => 'a/sous'
						]
					]
				]
			],
			'MATCH' => [
				// Expected
				'afficher_si({&quot;champ&quot;:&quot;a&quot;,&quot;total&quot;:&quot;&quot;,&quot;operateur&quot;:&quot;MATCH&quot;,&quot;valeur&quot;:&quot;oui&quot;,&quot;modificateur&quot;:[],&quot;regexp_modif&quot;:&quot;&quot;})',
				// condition
				'@a@ MATCH "/oui/"',
				// saisies_par_etapes
				[
					[
						'saisie' => 'input',
						'options' => [
							'nom' => 'a'
						]
					]
				]
			],
			'sans_type' => [
				// Expected
				'',
				// condition
				'@a@ == "oui"',
				// saisies_par_etapes
				[
					[
						'options' => [
							'nom' => 'a'
						]
					]
				]
			],
			'etape_courante' => [
				// Expected
				'afficher_si({&quot;champ&quot;:&quot;b&quot;,&quot;total&quot;:&quot;&quot;,&quot;operateur&quot;:&quot;==&quot;,&quot;valeur&quot;:&quot;oui&quot;,&quot;modificateur&quot;:[]})',
				// condition
				'@b@ == "oui"',
				// saisies_par_etapes
				[
					'etape_1' => [
						'options' => [
							'nom' => 'etape_1'
						],
						'saisies' => [
							[
								'saisie' => 'input',
								'options' => [
									'nom' => 'a'
								]
							]
						]
					],
					'etape_2' => [
						'options' => [
							'nom' => 'etape_2'
						],
						'saisies' => [
							[
								'saisie' => 'input',
								'options' => [
									'nom' => 'b'
								]
							]
						]
					]
				]
			],
			'substr_1' => [
				// Expected
				'afficher_si({&quot;champ&quot;:&quot;c&quot;,&quot;total&quot;:&quot;&quot;,&quot;operateur&quot;:&quot;==&quot;,&quot;valeur&quot;:&quot;a&quot;,&quot;modificateur&quot;:{&quot;fonction&quot;:&quot;substr&quot;,&quot;type_champ&quot;:&quot;string&quot;,&quot;arguments&quot;:[&quot;1&quot;,&quot;1&quot;]}})',
				'@c@:SUBSTR(1, 1) == "a"',
				// saisies_par_etapes
				[
					[
						'saisie' => 'input',
						'options' => [
							'nom' => 'c'
						]
					]
				]
			],
			'substr_2' => [
				// Expected
				'afficher_si({&quot;champ&quot;:&quot;c&quot;,&quot;total&quot;:&quot;&quot;,&quot;operateur&quot;:&quot;==&quot;,&quot;valeur&quot;:&quot;def&quot;,&quot;modificateur&quot;:{&quot;fonction&quot;:&quot;substr&quot;,&quot;type_champ&quot;:&quot;string&quot;,&quot;arguments&quot;:[&quot;3&quot;,&quot;&quot;]}})',
				// condition
				'@c@:SUBSTR(3) == "def"',
				// saisies_par_etapes
				[
					[
						'saisie' => 'input',
						'options' => [
							'nom' => 'c'
						]
					]
				]
			],
			'etape_courante_sous' => [
				// Expected
				'afficher_si({&quot;champ&quot;:&quot;b[sous]&quot;,&quot;total&quot;:&quot;&quot;,&quot;operateur&quot;:&quot;==&quot;,&quot;valeur&quot;:&quot;oui&quot;,&quot;modificateur&quot;:[]})',
				// condition
				'@b[sous]@ == "oui"',
				// saisies_par_etapes
				[
					'etape_1' => [
						'options' => [
							'nom' => 'etape_1'
						],
						'saisies' => [
							[
								'saisie' => 'input',
								'options' => [
									'nom' => 'a'
								]
							]
						]
					],
					'etape_2' => [
						'options' => [
							'nom' => 'etape_2'
						],
						'saisies' => [
							[
								'saisie' => 'input',
								'options' => [
									'nom' => 'b[sous]'
								]
							]
						]
					]
				]
			],
			'etape_passee' => [
				// Expected
				'true',
				// condition
				'@a_dans_etape_1@ == "oui"',
				// saisies_par_etapes
				[
					'etape_1' => [
						'options' => [
							'nom' => 'etape_1'
						],
						'saisies' => [
							[
								'saisie' => 'input',
								'options' => [
									'nom' => 'a_dans_etape_1'
								]
							]
						]
					],
					'etape_2' => [
						'options' => [
							'nom' => 'etape_2'
						],
						'saisies' => [
							[
								'saisie' => 'input',
								'options' => [
									'nom' => 'b'
								]
							]
						]
					]
				]
			],
			'etape_passee_sous' => [
				// Expected
				'true',
				// condition
				'@b_dans_etape_1[sous]@ == "oui"',
				// saisies_par_etapes
				[
					'etape_1' => [
						'options' => [
							'nom' => 'etape_1'
						],
						'saisies' => [
							[
								'saisie' => 'input',
								'options' => [
									'nom' => 'b_dans_etape_1[sous]'
								]
							]
						]
					],
					'etape_2' => [
						'options' => [
							'nom' => 'etape_2'
						],
						'saisies' => [
							[
								'saisie' => 'input',
								'options' => [
									'nom' => 'b'
								]
							]
						]
					]
				]
			],
			'plugin' => [
				// Expected
				'false',
				// condition
				'@plugin:b@',
				// saisies_par_etapes
				[
					[
						'saisie' => 'input',
						'options' => [
							'nom' => 'b'
						]
					]
				]
			],
			'config' => [
				// Expected
				'false',
				// condition
				'@config:b@',
				// saisies_par_etapes
				[
					[
						'saisie' => 'input',
						'options' => [
							'nom' => 'b'
						]
					]
				]
			],
			'bool' => [
				// Expected
				'false',
				// condition
				'false',
				// saisies_par_etapes
				[
					[
						'saisie' => 'input',
						'options' => [
							'nom' => 'b'
						]
					]
				]
			],
			'sans_condition' => [
				// Expected
				'',
				// condition
				'',
				// saisies_par_etapes
				[
					[
						'saisie' => 'input',
						'options' => [
							'nom' => 'a'
						]
					]
				]
			],
			'sans_champ' => [
				// Expected
				'',
				// condition
				'@a@ == "oui"',
				// saisies_par_etapes
				[
					[
						'saisie' => 'input',
						'options' => [
							'nom' => 'b'
						]
					]
				]
			],
			'syntaxe_incorrecte' => [
				// Expected
				'',
				// condition
				'titi',
				// saisies_par_etapes
				[
					[
						'saisie' => 'input',
						'options' => [
							'nom' => 'b'
						]
					]
				]
			],
			'syntaxe_incorrecte_ensemble' => [
				// Expected
				'',
				// condition
				'@b@ == "on" && ',
				// saisies_par_etapes
				[
					[
						'saisie' => 'input',
						'options' => [
							'nom' => 'b'
						]
					]
				]
			]
		];
	}

	/**
	 * @dataProvider dataAfficherSiJs
	 * @covers saisies_afficher_si_js_defaut()
	 * @uses saisies_afficher_si_parser_valeur_MATCH()
	 * @uses saisie_nom2name()
	 * @uses saisies_afficher_si_evaluer_plugin()
	 * @uses saisies_afficher_si_filtrer_parse_condition()
	 * @uses saisies_afficher_si_secure()
	 * @uses saisies_afficher_si_verifier_syntaxe()
	 * @uses saisies_lister_par_nom()
	 * @uses saisies_parser_condition_afficher_si()
	 * @uses saisies_afficher_si_get_valeur_config()
	 * @uses saisies_request()
	 * @uses saisies_tester_condition_afficher_si()
	 * @uses saisies_get_valeur_saisie()
	 * @uses saisies_saisie_est_fichier()
	 * @uses saisies_tester_condition_afficher_si_string()
	 * @uses saisies_verifier_coherence_afficher_si_par_champ()
	 * @uses saisies_chaine2tableau()
	 * */
	public function testAfficherSiJs($expected, $condition, $etapes) {
		$actual = saisies_afficher_si_js($condition, $etapes);
		$this->assertSame($expected, $actual);
	}

	public static function dataAfficherSiJsEstStatique() {
		return [
			'true' => [
				// Expected
				true,
				// Provided
				'!(false && false || false)'
			],
			'false' => [
				// Expected
				false,
				// Provided
				'!(fale && false || false) && afficher_si({})'
			],
		];
	}

	/**
	 * @dataProvider dataAfficherSiJsEstStatique
	 * */
	public function testAfficherSiJsEstStatique($expected, $test) {
		$actual = saisies_afficher_si_js_est_statique($test);
		$this->assertEquals($expected, $actual);
	}

}
