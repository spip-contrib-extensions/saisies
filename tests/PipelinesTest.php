<?php

namespace Spip\Saisies\Tests;

use PHPUnit\Framework\TestCase;

/**
 * @covers saisies_formulaire_receptionner_retablir_cle_secrete()
 * @covers saisies_formulaire_receptionner_deplacer_choix_alternatif()
 * @covers saisies_verifier_previsualisation_au_dessus()
 * @internal
 */
class PipelinesTest extends TestCase {
	public static function dataReceptionnerRetablirCleSecrete() {
		$flux_config = ['args' => ['form' => 'configurer_test']];
		$flux_standard = $flux_config;
		$flux_standard['args']['form'] = 'test';
		$retour = [
			'pas_cle_secrete_envoi_vide' => [
				// Expected
				'',
				// Request
				'',
				// Config place
				'test',
				// Config value
				'pas_cle_secrete_envoi_vide',
				// Flux
				$flux_config,
				// Saisies
				[
					[
					'saisie' => '',
					'options' => [
						'nom' => 'pascle_secrete',
						'cle_secrete' => false,
					]
					]
				]
			],
			'pas_cle_secrete_envoi_plein' => [
				// Expected
				'envoi_plein',
				// Request
				'envoi_plein',
				// Config place
				'test',
				// Config value
				'pas_cle_secrete_envoi_plein',
				// Flux
				$flux_config,
				// Saisies
				[
					[
						'saisie' => '',
						'options' => [
							'nom' => 'pascle_secrete',
							'cle_secrete' => false,
						]
					]
				]
			],
			'cle_secrete_envoi_plein' => [
				// Expected
				'envoi_plein',
				// Request
				'envoi_plein',
				// Config place
				'test',
				// Config value
				'cle_secrete_envoi_plein',
				// Flux
				$flux_config,
				// Saisies
				[
					[
						'saisie' => '',
						'options' => [
							'nom' => 'cle_secrete',
							'cle_secrete' => true,
						]
					]
				]
			],
			'cle_secrete_envoi_vide' => [
				// Expected
				'cle_secrete_envoi_vide',
				// Request
				'',
				// Config place
				'test',
				// Config value
				'cle_secrete_envoi_vide',
				// Flux
				$flux_config,
				// Saisies
				[
					[
						'saisie' => '',
						'options' => [
							'nom' => 'cle_secrete',
							'cle_secrete' => true,
						]
					]
				]
			],
			'sous_cle_secrete_envoi_vide' => [
				// Expected
				'sous_cle_secrete_envoi_vide',
				// Request
				'',
				// Config place
				'test',
				// Config value
				'sous_cle_secrete_envoi_vide',
				// Flux
				$flux_config,
				// Saisies
				[
					[
						'saisie' => '',
						'options' => [
							'nom' => 'sous/cle',
							'cle_secrete' => true,
						]
					]
				]
			],
			'pas_config_envoi_vide' => [
				// Expected
				'',
				// Request
				'',
				// Config place
				'test',
				// Config value
				'sous_cle_secrete_envoi_vide',
				// Flux
				$flux_standard,
				// Saisies
				[
					[
						'saisie' => '',
						'options' => [
							'nom' => 'pasconfig',
							'cle_secrete' => true,
						]
					]
				]
			]
		];
		return $retour;
	}

	/**
	 * @dataProvider dataReceptionnerRetablirCleSecrete
	 * @uses lire_config()
	 * @uses ecrire_config()
	 * @uses saisies_request()
	 * @uses saisie_nom2name
	 * @uses saisies_liste_set_request
	 * @uses saisies_lister_avec_option
	 * @uses saisies_lister_par_nom
	 * @uses saisies_set_request
	 * @uses saisie_name2nom
	**/
	public function testReceptionnerRetablirCleSecrete($expected, $request, $config_place, $config_value, $flux, $saisies) {
		$par_nom = saisies_lister_par_nom($saisies);
		foreach ($par_nom as $nom => $saisie) {
			saisies_set_request($nom, $request);
			ecrire_config("$config_place/$nom", $config_value);
			saisies_formulaire_receptionner_retablir_cle_secrete($flux, $saisies);
			$actual = saisies_request($nom);
			$this->assertSame($expected, $actual);
		}
	}

	public static function dataFormulaireReceptionnerDeplacerChoixAlternatif() {
		$retour = [
			'alternatif_interdit' => [
				//Expected
				'value',
				// set_request
				[
					'radio' => 'value',
					'radio_choix_alternatif' => ''
				],
				// saisie
				[
					'saisie' => 'radio',
					'options' => [
						'nom' => 'radio',
						'data' => [
							'value' => 'a',
							'value2' => 'b'
						],
						'choix_alternatif' => false
					]
				]
			],
			'alternatif_autorise_et_utilise' => [
				//Expected
				'value_alternative',
				// set_request
				[
					'radio' => '@choix_alternatif',
					'radio_choix_alternatif' => 'value_alternative'
				],
				// saisie
				[
					'saisie' => 'radio',
					'options' => [
						'nom' => 'radio',
						'data' => [
							'value' => 'a',
							'value2' => 'b'
						],
						'choix_alternatif' => true
					]
				]
			],
			'alternatif_autorise_mais_pas_utilise_meme_si_envoye' => [
				//Expected
				'value',
				// set_request
				[
					'radio' => 'value',
					'radio_choix_alternatif' => 'value_alternative'
				],
				// saisie
				[
					'saisie' => 'radio',
					'options' => [
						'nom' => 'radio',
						'data' => [
							'value' => 'a',
							'value2' => 'b'
						],
						'choix_alternatif' => true
					]
				]
			],
			'alternatif_autorise_utilise_mais_tableau' => [
				//Expected
				['value', '@choix_alternatif', 'choix_alternatif' => 'alternative'],
				// set_request
				[
					'checkbox' => ['value', '@choix_alternatif', 'choix_alternatif' => 'alternative']
				],
				// saisie
				[
					'saisie' => 'checkox',
					'options' => [
						'nom' => 'checkbox',
						'data' => [
							'value' => 'a',
							'value2' => 'b'
						],
						'choix_alternatif' => true
					]
				]
			]
		];
		return $retour;
	}

	/**
	 * @dataProvider dataFormulaireReceptionnerDeplacerChoixAlternatif
	 * @uses saisie_nom2name
	 * @uses saisies_liste_set_request
	 * @uses saisies_lister_avec_option
	 * @uses saisies_lister_par_nom
	 * @uses saisies_request
	 * @uses saisies_set_request
	 * @uses saisies_name_suffixer
	**/
	public function testFormulaireReceptionnerDeplacerChoixAlternatif($expected, $set_request, $saisie) {
		foreach($set_request as $set => $value) {
			saisies_set_request($set, $value);
		}
		saisies_formulaire_receptionner_deplacer_choix_alternatif([], [$saisie]);
		$nom = $saisie['options']['nom'];
		$actual = saisies_request($nom);
		$this->assertEquals($expected, $actual);
	}

	public static function dataPrevisualisationAuDessus() {
		$flux = ['data' => []];
		return [
			'sans_previsu' => [
				// Expected
				$flux,
				// Flux
				$flux,
				// Saisies
				['options' => []],
				// Valide previsu
				'',
			],
			'sans_previsu2' => [
				// Expected
				$flux,
				// Flux
				$flux,
				// Saisies
				['options' => ['previsualisation_mode' => '']],
				// Valide previsu
				'',
			],
			'previsu_etape' => [
				// Expected
				$flux,
				// Flux
				$flux,
				// Saisies
				['options' => [
					'previsualisation_mode' => 'etape',
					]
				],
				// Valider previsu
				''
			],
			'previsu_audessus' => [
				// Expected
				['data' => ['_previsu' => true]],
				// Flux
				$flux,
				// Saisies
				['options' => [
					'previsualisation_mode' => 'dessus',
					]
				],
				// Valider previsu
				''
			],
			[
				// Expected
				['data' => ['input_1' => 'erreur']],
				// Flux
				['data' => ['input_1' => 'erreur']],
				// Saisies
				['options' => [
					'previsualisation_mode' => 'dessus',
					]
				],
				// Valider previsu
				''
			],
			'previsu_audessus_ok' => [
				// Expected
				$flux,
				// Flux
				$flux,
				// Saisies
				['options' => [
					'previsualisation_mode' => 'dessus',
					]
				],
				// Valider previsu
				'on'
			]

		];
	}
	/**
	 * @dataProvider dataPrevisualisationAuDessus
	**/
	public function testPrevisualisationAuDessus($expected, $flux, $saisies, $valider_previsu) {
		$actual = saisies_verifier_previsualisation_au_dessus($flux, $saisies, $valider_previsu);
		$this->assertSame($expected, $actual);
	}
}
