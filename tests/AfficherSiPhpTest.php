<?php

namespace Spip\Saisies\Tests;


use PHPUnit\Framework\TestCase;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\Attributes\RunInSeparateProcess;

/**
 * @covers saisies_evaluer_afficher_si()
 * @covers saisies_verifier_afficher_si()
 * @covers saisies_afficher_si_masquees_set_request_empty_string()
 * @covers saisies_set_request_recursivement()
 * @covers saisies_transformer_condition_afficher_si()
 * @covers saisies_afficher_si_get_valeur_champ()
 * @covers saisies_afficher_si_liste_masquees()
 * @uses saisies_request()
 * @uses saisie_nom2name()
 * @uses saisie_name2nom()
 * @uses saisies_lister_par_nom()
 * @uses saisies_saisie_est_tabulaire()
 * @uses saisies_saisie_est_fichier()
 * @uses saisies_tester_condition_afficher_si()
 * @uses saisies_tester_condition_afficher_si_string()
 * @uses saisies_tester_condition_afficher_si_array()
 * @uses saisies_afficher_si_secure()
 * @uses saisies_afficher_si_verifier_syntaxe()
 * @uses saisies_afficher_si_parser_valeur_MATCH()
 * @uses saisies_parser_condition_afficher_si()
 * @uses saisies_set_request()
 * @uses saisies_chercher()
 * @uses saisies_lister_champs()
 * @uses saisies_lister_par_nom()
 * @uses saisies_afficher_si_get_valeur_config()
 * @uses saisies_afficher_si_filtrer_parse_condition()
 * @uses saisies_afficher_si_evaluer_plugin()
 * @uses saisies_aplatir_tableau()
 * @uses saisies_chaine2tableau()
 * @uses saisies_trouver_data()
 * @uses saisies_valeur2tableau()
 * @uses saisies_liste_set_request()
 * @uses spip_log
 * @uses spip_logger
 * @internal
 */
class AfficherSiPhpTest extends TestCase {


	public static function dataEvaluerAfficherSi() {
		// Quelques fonctions internes à saisies pour écrire plus facilement les tests
		// Préparer les request et config

		require_once dirname(__DIR__) . '/inc/saisies_request.php';
		require_once dirname(__DIR__) . '/inc/saisies_name.php';
		require_once dirname(__DIR__) . '/inc/saisies_lister.php';
		saisies_set_request('input_1', 'toto@domaine.ext');
		saisies_set_request('case_1', 'oui');
		saisies_set_request('case_2', '');
		saisies_set_request('a', 'a');
		saisies_set_request('b', 'b');
		saisies_set_request('c', 'c');
		saisies_set_request('d', 'd');
		saisies_set_request('zero', '0');
		saisies_set_request('url', 'http://www.spip.net');
		saisies_set_request('tableau_1', ['a', 'b', 'c']);
		saisies_set_request('tableau_2', ['e', 'f', 'g']);
		saisies_set_request('nombre', '20');
		saisies_set_request('nombre_negatif', '-20');
		saisies_set_request('cascade', ['a' => 'a']);
		saisies_set_request('cascade_bis/a', 'a');
		ecrire_config('tests_saisies_config', ['a' => 'a', 'sous' => ['b' => 'b', 'c' => 'c']]);

		// Des saisies pour faire des tests directement sur un tableau de saisies
		$saisies_par_nom = \saisies_lister_par_nom([
			[
				'saisie' => 'input',
				'options' =>
				[
					'label' => 'Ligne de texte',
					'type' => 'text',
					'afficher_si' => '!@truc@',
					'size' => '40',
					'autocomplete' => 'defaut',
					'nom' => 'input_1',
				],
			],
			[
				'saisie' => 'choix_grille',
				'options' =>
				[
					'label' => 'Grille de question',
					'nom' => 'choix_grille_1',
				],
			]
		]);
		// Le jeu de tests proprement dit
		$essais =  [
			'simple_egalite' =>
			[
				true,
				"@a@ == 'a'"
			],
			'simple_inegalite' =>
			[
				false,
				"@a@ == 'b'"
			],
			'double_egalite' =>
			[
				true,
				"@a@ == 'a' && @b@ == 'b'"
			],
			'double_egalite_fausse' =>
			[
				false,
				"@a@ == 'a' && @b@ == 'c'"
			],
			'egalite_alternative' =>
			[
				true,
				"@a@ == 'a' || @b@ == 'c'"
			],
			'egalite_alternative_fausse' =>
			[
				false,
				"@a@ == 'b' || @b@ == 'c'"
			],
			'presence_tableau_alternative' =>
			[
				true,
				"@tableau_1@ IN 'b' || @tableau_2@ == 'c'"
			],
			'presence_tableau_cumulative' =>
			[
				false,
				"@tableau_1@ IN 'b' && @tableau_2@ == 'c'"
			],
			'absence_tableau_cumulative' =>
			[
				false,
				"@tableau_1@ !IN 'b' && @tableau_2@ !IN 'c'"
			],
			'absence_tableau_alternative' =>
			[
				true,
				"@tableau_1@ !IN 'b' || @tableau_2@ !IN 'c'"
			],
			'absence_tableau_alternative_espacement' =>
			[
				true,
				"@tableau_1@ !   IN 'b' || @tableau_2@ !  IN 'c'"
			],
			'champ_uniquement' => [
				true,
				'@case_1@'
			],
			'champ_uniquement_faux' => [
				false,
				'@case_2@'
			],
			'champ_uniquement_negation' => [
				true,
				'!@case_2@'
			],
			'champ_uniquement_negation_faux' => [
				false,
				'!@case_1@'
			],
			'nombre_superieur_vrai' => [
				true,
				'@nombre@ > 10'
			],
			'nombre_superieur_faux' => [
				false,
				'@nombre@ > 100'
			],
			'nombre_superieur_egal_vrai' => [
				true,
				'@nombre@ >= 20'
			],
			'nombre_superieur_egal_faux' => [
				false,
				'@nombre@ >= 100'
			],
			'nombre_inferieur_vrai' => [
				true,
				'@nombre@ < 100'
			],
			'nombre_inferieur_faux' => [
				false,
				'@nombre@ < 10'
			],
			'nombre_inferieur_egal_vrai' => [
				true,
				'@nombre@ <= 20'
			],
			'nombre_inferieur_egal_faux' => [
				false,
				'@nombre@ <= 10'
			],
			'nombre_negatif_inferieur_vrai' => [
				true,
				'@nombre_negatif@ > -30'
			],
			'nombre_negatif_inferieur_faux' => [
				false,
				'@nombre_negatif@ > -10'
			],
			'false' => [
				false,
				'false'
			],
			'true' => [
				true,
				'true'
			],
			'anti_false' => [
				true,
				'!false'
			],
			'anti_true' => [
				false,
				'!true'
			],
			'hack' => [
				true,
				"spip_log('s') || @input_1@=='s')"
			],
			'premier_niveau' => [
				true,
				'@config:tests_saisies_config:a@==\'a\'',
			],
			'second_niveau' => [
				false,
				'@config:tests_saisies_config:sous:b@==\'c\'',
			],
			'premier_niveau_nie' => [
				false,
				'!@config:tests_saisies_config:a@==\'a\'',
			],
			'second_niveau_nie' => [
				true,
				'!@config:tests_saisies_config:sous:b@==\'c\'',
			],
			'second_niveau_bis' => [
				true,
				'@config:tests_saisies_config:sous:c@==\'c\'',
			],
			'second_niveau_et' => [
				false,
				'@config:tests_saisies_config:sous:c@==\'c\' && @config:tests_saisies_config:sous:b@==\'c\'',
			],
			'second_niveau_ou' => [
				true,
				'@config:tests_saisies_config:sous:c@==\'c\' || @config:tests_saisies_config:sous:b@==\'c\'',
			],
			'second_niveau_ou_crochet' => [
				true,
				'@config:tests_saisies_config:sous[c]@==\'c\' || @config:tests_saisies_config:sous[b]@==\'c\'',
			],
			'plugin_actif' => [
				true,
				'@plugin:saisies@'
			],
			'plugin_inactif' => [
				false,
				'@plugin:tartempion_de_test@' // en espérant que personne ne nomme un plugin tartempion_de_test
			],
			'plugin_actif_nie' => [
				false,
				'!@plugin:saisies@'
			],
			'plugin_inactif_nie' => [
				true,
				'!@plugin:tartempion_de_test@' // en espérant que personne ne nomme un plugin tartempion_de_test
			],
			'hack' => [
				true,
				"spip_log('s') || @input_1@=='s')"
			],
			'cascade' => [
				true,
				'@cascade[a]@ == \'a\''
			],
			'cascade_slash' => [
				true,
				'@cascade/a@ == \'a\''
			],
			'cascade_bis' => [
				true,
				'@cascade_bis/a@ == \'a\''
			],
			'total_tableau_sup' => [
				true,
				'@tableau_1@:TOTAL > 2'
			],
			'total_tableau_inf' => [
				false,
				'@tableau_1@:TOTAL < 2'
			],
			'match' => [
				true,
				'@input_1@ MATCH \'/domaine.ext$/\'',
			],
			'match_negation' => [
				false,
				'!@input_1@ MATCH \'/domaine.ext$/\'',
			],
			'not_match' => [
				false,
				'@input_1@ !MATCH \'/domaine.ext$/\'',
			],
			'not_match_negation' => [
				true,
				'!@input_1@ !MATCH \'/domaine.ext$/\'',
			],
			'match_slash' => [
				true,
				'@url@ MATCH "/^https?:\/\//i"'
			],
			'match_slash_bis' => [
				true,
				'@url@ MATCH "/^HTTP:\/\//i"'
			],
			'zero_rapide' => [
				true,
				'@zero@',
			],
			'champ_inexistant' => [
				true,
				'@champ_inexistant@ == \'s\'',
				null,
				$saisies_par_nom
			],
			'champ_existant' => [
				false,
				'@input_1@ == \'s\'',
				null,
				$saisies_par_nom
			],
			'champ_existant_choix_grille_nom' => [
				false,
				'@choix_grille_1/a@ == \'s\'',
				null,
				$saisies_par_nom
			],
			'champ_existant_choix_grille_name' => [
				false,
				'@choix_grille_1[b]@ == \'s\'',
				null,
				$saisies_par_nom
			]
		];
		return $essais;
	}
	/**
	 * @dataProvider dataEvaluerAfficherSi
	 * @covers saisies_evaluer_afficher_si()
	 * @covers saisies_parser_condition_afficher_si()
	 * @covers saisies_tester_condition_afficher_si_string()
	 * @uses saisies_afficher_si_evaluer_plugin()
	 * @uses saisies_afficher_si_secure()
	 * @uses saisies_afficher_si_verifier_syntaxe()
	 */
	public function testEvaluerAfficherSi($expected, $condition, $env = null, $saisies_par_nom = [], $no_arobase = null) {
		$actual = saisies_evaluer_afficher_si($condition, $env, $saisies_par_nom, $no_arobase);
		$this->assertEquals($expected, $actual);
	}

	public static function dataVerifierAfficherSi() {
		return [
			'_request' => [
				// Expected
				[
					[
						'saisie' => 'fieldset',
						'saisies' => [
							[
								'saisie' => 'case',
								'options' => [
									'nom' => 'case_1'
								]
							]
						],
					]
				],
				//Provided
				[
					[
						'saisie' => 'fieldset',
						'saisies' => [
							[
								'saisie' => 'case',
								'options' => [
									'nom' => 'case_1'
								]
							],
							[
								'saisie' => 'input',
								'options' => [
									'nom' => 'input_1',
									'afficher_si' => '!@case_1@'
								]
							]
						],
					]
				],
				null
			],
			'dans_env' => [
				// Expected
				[
					[
						'saisie' => 'fieldset',
						'saisies' => [
							[
								'saisie' => 'case',
								'options' => [
									'nom' => 'dans_env'
								]
							]
						],
					]
				],
				//Provided
				[
					[
						'saisie' => 'fieldset',
						'saisies' => [
							[
								'saisie' => 'case',
								'options' => [
									'nom' => 'dans_env'
								]
							],
							[
								'saisie' => 'input',
								'options' => [
									'nom' => 'input_1',
									'afficher_si' => '@dans_env@'
								]
							]
						],
					]
				],
				['dans_env' => '']
			],
			'dans_env_remplissage_uniquement' => [
				// Expected
				[
					[
						'saisie' => 'fieldset',
						'saisies' => [
							[
								'saisie' => 'case',
								'options' => [
									'nom' => 'dans_env'
								]
							],
							[
								'saisie' => 'input',
								'options' => [
									'nom' => 'input_1',
									'afficher_si' => '@dans_env@',
									'afficher_si_remplissage_uniquement' => 'on',
								]
							]
						],
					]
				],
				//Provided
				[
					[
						'saisie' => 'fieldset',
						'saisies' => [
							[
								'saisie' => 'case',
								'options' => [
									'nom' => 'dans_env'
								]
							],
							[
								'saisie' => 'input',
								'options' => [
									'nom' => 'input_1',
									'afficher_si' => '@dans_env@',
									'afficher_si_remplissage_uniquement' => 'on',
								]
							]
						],
					]
				],
				['dans_env' => '']
			]
		];
	}

	#[DataProvider('dataVerifierAfficherSi')]
	public function testVerifierAfficherSi($expected, $saisies, $env) {
		$actual = saisies_verifier_afficher_si($saisies, $env);
		$this->assertEquals($expected, $actual);
	}

	public static function dataAfficherSiMasqueesSetRequestEmptyString() {
		$return = [
			'defaut' => [
				// Expected
				[
					'input_1' => '',
					'input_2' => ''
				],
				// Provided Saisies
				[
					[
						'saisie' => 'input',
						'options' => [
							'nom' => 'input_1',
						]
					],
					[
						'saisie' => 'fieldset',
						'options' => [
							'nom' => 'fieldset_1'
						],
						'saisies' => [
							[
								'saisie' => 'input',
								'options' => [
									'nom' => 'input_2',
								]
							]
						]
					],
				],
				// Value initial of request
				[
					'input_1' => 'toto',
					'input_2' => 'titi',
				],
				// Rules to call `saisies_afficher_si_liste_masquee',
				[
					'input_1' => true,
					'fieldset_1' => true
				],
			],
			'avec_post_global' => [
				// Expected
				[
					'input_1a' => 'toto',
					'input_2a' => 'titi'
				],
				// Provided Saisies
				[
					'options' => [
						'afficher_si_avec_post' => 'on',
					],
					[
						'saisie' => 'input',
						'options' => [
							'nom' => 'input_1a',
						]
					],
					[
						'saisie' => 'fieldset',
						'options' => [
							'nom' => 'fieldset_1a'
						],
						'saisies' => [
							[
								'saisie' => 'input',
								'options' => [
									'nom' => 'input_2a',
								]
							]
						]
					],
				],
				// Value initial of request
				[
					'input_1a' => 'toto',
					'input_2a' => 'titi',
				],
				// Rules to call `saisies_afficher_si_liste_masquee',
				[
					'input_1a' => true,
					'fieldset_1a' => true
				]
			],
			'avec_post_local' =>	[
				// Expected
				[
					'input_1b' => 'toto',
					'input_2b' => ''
				],
				// Provided Saisies
				[
					[
						'saisie' => 'input',
						'options' => [
							'afficher_si_avec_post' => 'on',
							'nom' => 'input_1b',
						]
					],
					[
						'saisie' => 'fieldset',
						'options' => [
							'nom' => 'fieldset_1b'
						],
						'saisies' => [
							[
								'saisie' => 'input',
								'options' => [
									'nom' => 'input_2b',
								]
							]
						]
					],
				],
				// Value initial of request
				[
					'input_1b' => 'toto',
					'input_2b' => 'titi',
				],
				// Rules to call `saisies_afficher_si_liste_masquee',
				[
					'input_1b' => true,
					'fieldset_1b' => true
				]
			]
		];
		return $return;
	}

	/**
	 * @dataProvider dataAfficherSiMasqueesSetRequestEmptyString
	**/
	public function testAfficherSiMasqueesSetRequestEmptyString($expected, $saisies, $request_initial, $rules_masquees) {
		foreach ($request_initial as $r => $v) {
			saisies_set_request($r, $v);
		}
		foreach ($rules_masquees as $r => $v) {
			if ($v) {
				$saisie = saisies_chercher($saisies, $r);
				saisies_afficher_si_liste_masquees('set', $saisie);
			}
		}
		saisies_afficher_si_masquees_set_request_empty_string($saisies);
		foreach ($expected as $name => $exp) {
			$actual = saisies_request($name);
			$this->assertSame($exp, $actual);
		}
	}

	public static function dataSetRequestRecursivement() {
		return [
			[
				// Expected
				['fieldset_set_request_recursivement_input' => ''],
				// Saisie
				[
					'saisie' => 'fieldset',
					'options' => [
						'nom' => 'fieldset_set_request_recursivement',
					],
					'saisies' => [
						[
							'saisie' => 'input',
							'options' => [
								'nom' => 'fieldset_set_request_recursivement_input'
							]
						]
					]
				],
				// Initial value
				[
					'fieldset_set_request_recursivement_input' => 'toto'
				]
			]
		];
	}

	/**
	 * @dataProvider dataSetRequestRecursivement
	**/
	public function testSetRequestRecursivement($expected, $saisies, $request_initial) {
		foreach ($request_initial as $r => $v) {
			saisies_set_request($r, $v);
		}
		saisies_set_request_recursivement($saisies, '');
		foreach ($expected as $name => $exp) {
			$actual = saisies_request($name);
			$this->assertSame($exp, $actual);
		}
	}

	/*
	 * For this test, we test only
	 * - the `no_arobase` form, the form without `no_arobase` is tested indirectly,
	 *    through `saisies_evaluer_afficher_si`;
	 * - the case the condition is an blank string (should not happen in real life, normally)
	**/
	public static function dataTransformerConditionAfficherSi() {
		return [
			[
				// Expected
				'false || false',
				// Condition
				'== "y" || == "z"',
				// Env
				['plop' => 'on'],
				// Saisies
				[],
				// no_arobase
				'plop'
			],
			[
				'',
				' ',
				[],
				[],
				'plop'
			]
		];
	}
	/**
	 * @dataProvider dataTransformerConditionAfficherSi
	**/
	public function testTransformerConditionAfficherSi($expected, $condition, $env, $saisies, $no_arobase) {
		$actual = saisies_transformer_condition_afficher_si($condition, $env, $saisies, $no_arobase);
		$expected = str_replace(' ', '', $expected);
		$actual = str_replace(' ', '', $actual);
		$this->assertEquals($expected, $actual);
	}

	public static function dataAfficherSiGetValeurChamp(): array {
		ecrire_config('configget', 'cfg');
		saisies_set_request('input', 'cfg_input');
		$saisies_par_nom = [
			'input_get' => [
				'saisie' => 'input',
				'options' => [
					'nom' => 'configget'
				]
			],
			'checkbox' => [
				'saisie' => 'checkbox',
				'options' => [
					'nom' => 'checkbox',
					'data' => [
						'choix1' => 'Un',
						'choix2' => 'Deux',
					]
				]
			]
		];
		return [
			'configget' => [
				// Expected
				'cfg',
				// Champ
				'config:configget',
				// Env
				null,
				// saisies_par_nom
				$saisies_par_nom
			],
			'plugin' => [
				// Expected
				true,
				// Champ
				'plugin:saisies',
				// Env
				null,
				// saisies_par_nom
				$saisies_par_nom
			],
			'env' => [
				// Expected
				'cfg_env',
				// Champ
				'toto',
				// Env
				['valeurs' => ['toto' => 'cfg_env']],
				// saisies_par_nom
				$saisies_par_nom
			],
			'env_tabulaire' => [
				// Expected
				['choix1', 'choix2'],
				// Champ
				'checkbox',
				// Env
				['valeurs' => ['checkbox' => 'choix1,choix2']],
				// saisies_par_nom
				$saisies_par_nom
			],
			'request' => [
				// Expected
				'cfg_input',
				// Champ
				'input',
				// Env
				null,
				// saisies_par_nom
				$saisies_par_nom
			]
		];
	}

	/**
	 * @dataProvider dataAfficherSiGetValeurChamp
	*/
	public function testAfficherSiGetValeurChamp($expected, $champ, $env, $saisies_par_nom) {
		$actual = saisies_afficher_si_get_valeur_champ($champ, $env, $saisies_par_nom);
		$this->assertEquals($expected, $actual);
	}

	public static function dataAfficherSiListeMasquees() {
		$saisie1 = [
			'saisie' => 'input1',
		];
		$saisie2 = [
			'saisie' => 'input2',
		];
		return [
			[[
				'set1' => [
					// Expected
					null,
					// Action
					'set',
					// Saisie
					$saisie1
				],
				'set2' => [
					// Expected
					null,
					// Action
					'set',
					// Saisie
					$saisie2
				],
				'get' => [
					// Expected
					[$saisie1, $saisie2],
					//Action
					'get'
				]
			]]
		];
	}

	#[DataProvider('dataAfficherSiListeMasquees')]
	#[RunInSeparateProcess]
	public function testAfficherSiListeMasquees($data) {
		foreach ($data as $v) {
			$expected = $v[0];
			$action = $v[1];
			$saisie = $v[2] ?? '';
			$actual = saisies_afficher_si_liste_masquees($action, $saisie);
			$this->assertEquals($expected, $actual);
		}
	}
}
