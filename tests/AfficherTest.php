<?php

namespace Spip\Saisies\Tests;

use PHPUnit\Framework\TestCase;

/**
 * @covers saisies_trouver_erreur()
 * @covers saisie_editable()
 * @covers saisies_afficher_normaliser_options_attributs()
 * @uses saisie_name2nom()
 * @uses saisie_nom2name()
 * @uses saisies_liste_set_request()
 * @uses saisies_lister_par_nom()
 * @uses table_valeur
 * @uses spip_log
 * @uses spip_logger
 * @internal
 */

class AfficherTest extends TestCase {

	/**
	 * Note : on ne vérifie pas le interdire_script car dépend SPIP, on fait confiance à ce dernier pour avoir des tests unitaires correctes
	**/
	public static function dataTrouverErreur() {
		$essais = [
			'null' => [
				'',
				null,
				'racine',
			],
			'racine' => [
				'oups',
				['racine' => 'oups'],
				'racine'
			],
			'SPIP_SPIP' => [
				'oups',
				['niveau1/niveau2' => 'oups'],
				'niveau1/niveau2'
			],
			'SPIP_HTML' => [
				'oups',
				['niveau1/niveau2' => 'oups'],
				'niveau1[niveau2]'
			],
			'HTML_HTML' => [
				'oups',
				['niveau1[niveau2]' => 'oups'],
				'niveau1[niveau2]'
			],
			'HTML_SPIP' => [
				'oups',
				['niveau1[niveau2]' => 'oups'],
				'niveau1/niveau2'
			],
			'PHP_HTML' => [
				'oups',
				['niveau1' => ['niveau2' => 'oups']],
				'niveau1[niveau2]'
			],
			'PHP_SPIP' => [
				'oups',
				['niveau1' => ['niveau2' => 'oups']],
				'niveau1/niveau2'
			]

		];
		return $essais;
	}

	/**
	 * @dataProvider dataTrouverErreur
	 */
	public function testTrouverErreur($expected, $erreurs, $nom_ou_name) {
		$actual = saisies_trouver_erreur($erreurs, $nom_ou_name);
		$this->assertEquals($expected, $actual);
	}

	public static function dataEditable() {
		return [
			'editable_implicite' => [
				// Expected
				true,
				// Champ
				[
					'saisie' => 'input',
					'options' => [
						'nom' => 'input_1'
					]
				],
				// env
				['input_1' => 'toto'],
				true
			],
			'editable_explicite' => [
				// Expected
				true,
				// Champ
				[
					'saisie' => 'input',
					'options' => [
						'nom' => 'input_1'
					],
					'editable' => 1
				],
				// env
				['input_1' => 'toto'],
				true
			],
			'non_editable_explicite' => [
				// Expected
				false,
				// Champ
				[
					'saisie' => 'input',
					'options' => [
						'nom' => 'input_1'
					],
					'editable' => 0
				],
				// env
				['input_1' => 'toto'],
				true
			],
			'editable_selon_env' => [
				// Expected
				true,
				// Champ
				[
					'saisie' => 'input',
					'options' => [
						'nom' => 'input_1'
					],
					'editable' => -1
				],
				// env
				['input_1' => 'toto'],
				true
			],
			'editable_selon_env_pas_utiliser_editable' => [
				// Expected
				true,
				// Champ
				[
					'saisie' => 'input',
					'options' => [
						'nom' => 'input_1'
					],
				],
				// env
				['input_1' => 'toto'],
				false
			],
			'editable_selon_enfant' => [
				// Expected
				true,
				// Champ
				[
					'saisie' => 'fieldset',
					'options' => ['nom' => 'fieldset_1'],
					'saisies' => [
						[
							'saisie' => 'input',
							'options' => [
								'nom' => 'input_1'
							],
							'editable' => -1
						]
					],
					'editable' => -1
				],
				// env
				['input_1' => 'toto'],
				true
			],
			'non_editable_selon_env' => [
				// Expected
				false,
				// Champ
				[
					'saisie' => 'input',
					'options' => [
						'nom' => 'input_1'
					],
					'editable' => -1
				],
				// env
				['input_22' => 'toto'],
				true
			],
			'non_editable_selon_env_pas_utiliser_editable' => [
				// Expected
				false,
				// Champ
				[
					'saisie' => 'input',
					'options' => [
						'nom' => 'input_1'
					],
				],
				// env
				['input_22' => 'toto'],
				false
			],
			'editable_selon_enfant_avec_false' => [
				// Expected
				true,
				// Champ
				[
					'saisie' => 'fieldset',
					'options' => ['nom' => 'fieldset_1'],
					'saisies' => [
						[
							'saisie' => 'input',
							'options' => [
								'nom' => 'input_1'
							],
							'editable' => -1
						]
					],
					'editable' => -1
				],
				// env
				['input_1' => 'toto'],
				false
			],
			'non_editable_selon_enfant' => [
				// Expected
				false,
				// Champ
				[
					'saisie' => 'fieldset',
					'options' => ['nom' => 'fieldset_1'],
					'saisies' => [
						[
							'saisie' => 'input',
							'options' => [
								'nom' => 'input_1'
							],
							'editable' => 0
						]
					],
					'editable' => -1
				],
				// env
				['input_12' => 'toto'],
				true
			],
			'depublie_mais_env' => [
				// Expected
				true,
				// Champ
				[
					'saisie' => 'input',
					'options' => [
						'nom' => 'input_1',
						'depublie' => 'on',
					],
				],
				// env
				['input_1' => 'toto'],
			],
			'depublie_mais_pas_env' => [
				// Expected
				false,
				// Champ
				[
					'saisie' => 'input',
					'options' => [
						'nom' => 'input_1',
						'depublie' => 'on',
					],
				],
				// env
				['input_2' => 'toto'],
			],
			'enfant_depublie' => [
				// Expected
				false,
				// Champ
				[
					'saisies' => [
						[
						'saisie' => 'input',
						'options' => [
							'nom' => 'input_1',
							'depublie' => 'on',
							],
						],
					]
				],
				// env
				['input_2' => 'toto'],
			],
			'enfant_depublie_mais_present' => [
				// Expected
				true,
				// Champ
				[
					'saisies' => [
						[
						'saisie' => 'input',
						'options' => [
							'nom' => 'input_1',
							'depublie' => 'on',
							],
						],
					]
				],
				// env
				['input_1' => 'toto'],
			],
			'enfant1_depublie_enfant2_publie' => [
				// Expected
				true,
				// Champ
				[
					'saisies' => [
						[
						'saisie' => 'input',
						'options' => [
							'nom' => 'input_1',
							'depublie' => 'on',
							],
						],
						[
						'saisie' => 'input',
						'options' => [
							'nom' => 'input_2',
							],
						]
					]
				],
				// env
				[],
			]
		];
	}

	/**
	 * @dataProvider dataEditable
	 **/
	public function testEditable($expected, $champ, $env, $utiliser_editable = true) {
		$actual = saisie_editable($champ, $env, $utiliser_editable);
		$this->assertEquals($expected, $actual);
	}


	public static function dataNormaliserOptionsAttributs() {
		return [
			'sans_attributs_data' => [
				// Expected
				[
					'attributs' => 'attributs = "a"'
				],
				// Provided
				[
					'attributs' => 'attributs = "a"'
				],
			],
			'attributs_data_null' => [
				// Expected
				[
					'attributs' => 'attributs = "a" data-valeur'
				],
				// Provided
				[
					'attributs' => 'attributs = "a"',
					'attributs_data' => ['valeur' => null]
				],
			],
			'attributs_data_string' => [
				// Expected
				[
					'attributs' => 'attributs = "a" data-valeur1="1" data-valeur2="2"'
				],
				// Provided
				[
					'attributs' => 'attributs = "a"',
					'attributs_data' => ['valeur1' => '1', 'valeur2' => '2']
				],
			],
			'attributs_data_string_with_double_quote' => [
				// Expected
				[
					'attributs' => 'attributs = "a" data-valeur="' . attribut_html('Iel lui dit "je t\'aime"') . '"',
				],
				// Provided
				[
					'attributs' => 'attributs = "a"',
					'attributs_data' => ['valeur' => 'Iel lui dit "je t\'aime"']
				],
			],
			'attributs_data_array' => [
				// Expected
				[
					'attributs' => 'attributs = "a" data-valeur="[&#034;a&#034;,&#034;b&#034;]"'
				],
				// Provided
				[
					'attributs' => 'attributs = "a"',
					'attributs_data' => ['valeur' => ['a', 'b']]
				],
			],
			'attributs_data_array_double_quotes' => [
				// Expected
				[
				'attributs' => 'attributs = "a" data-valeur="[&#034;Iel lui dit \&#034;je t&#039;aime\&#034;&#034;]"'
				],
				// Provided
				[
					'attributs' => 'attributs = "a"',
					'attributs_data' => ['valeur' => ['Iel lui dit "je t\'aime"']]
				],
			]
		];
	}
	/**
	 * @dataProvider dataNormaliserOptionsAttributs
	**/
	public function testNormaliserOptionsAttributs($expected, $provided) {
		$actual = saisies_afficher_normaliser_options_attributs($provided);
		$this->assertEquals($expected, $actual);
	}

}
