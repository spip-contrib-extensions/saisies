<?php

namespace Spip\Saisies\Tests;

use PHPUnit\Framework\TestCase;

/**
 * @covers saisies_tableau2chaine()
 * @covers saisies_lister_necessite_retenir_ancienne_valeur()
 * @covers saisies_chaine2tableau()
 * @covers saisies_aplatir_chaine()
 * @covers saisies_valeur2tableau()
 * @covers saisies_trouver_data()
 * @covers saisies_trouver_choix_alternatif()
 * @covers saisies_aplatir_tableau()
 * @covers saisies_normaliser_liste_choix()
 * @covers saisies_depublier_data()
 * @internal
 */

class DataTest extends TestCase {

	public static function dataAplatirChaine() {
		return [
			'basic' => [
				"Hop|Hop\r\n"
				. "Hip|Hip",
				"\r\n"
				. "/*\r\n"
				. "* Allo\r\n"
				. "Hop|Hop\r\n"
				. "*Allo2\r\n"
				. "Hip|Hip",
			]
		];
	}

	/**
	 * @dataProvider dataAplatirChaine
	**/
	public function testsAplatirChaine($expected, $input) {
		$actual = saisies_aplatir_chaine($input);
		$this->assertEquals($expected, $actual);
	}

	public static function dataChaine2tableau() {
		return [
			'null' => [
				[],
				null,
			],
			'deja_tableau' => [
				[
					'Group' => [
							'hap' => 'hap',
							'hep' => 'hep',
					],
					'Group 2' => [
						'hip' => 'hip',
						'hop' => 'hop',
					],
					'hup' => 'hup'
				],
				[
					'Group' => [
							'hap' => 'hap',
							'hep' => 'hep',
					],
					'Group 2' => [
						'hip' => 'hip',
						'hop' => 'hop',
					],
					'hup' => 'hup'
				]
			],
			'basic' => [
				[
					'Group' => [
							'hap' => 'hap',
							'hep' => 'hep',
					],
					'Group 2' => [
						'hip' => 'hip',
						'hop' => 'hop',
						6 => 'hopbis',
					],
					'hup' => 'hup',
					9 => 'voici',
				],
				"*Group\r\n"
				. "hap|hap\r\n"
				. "hep|hep\r\n"
				. "*Group 2\r\n"
				. "hip|hip\r\n"
				. "hop|hop\r\n"
				. "hopbis\r\n"
				. "/*\r\n"
				. "hup|hup\r\n"
				. 'voici',
			]
		];
	}
	/**
	 * @dataProvider dataChaine2tableau
	 * For this test, we disable _T_ou_type()
	 **/
	public function testChaine2tableau($expected, $string) {
		$actual = saisies_chaine2tableau($string);
		$this->assertEquals($expected, $actual);
	}


	public static function dataTableau2chaine() {
		return [
			'basic' => [
				"*Group\n"
				. "hap|hap\n"
				. "hep|hep\n"
				. "*Group 2\n"
				. "hip|hip\n"
				. "hop|hop\n"
				. "/*\n"
				. 'hup|hup',
				[
					'Group' => [
						'hap' => 'hap',
						'hep' => 'hep',
					],
					'Group 2' => [
						'hip' => 'hip',
						'hop' => 'hop',
					],
					'hup' => 'hup'
				],
			],
			'deja_chaine' => [
				"*Group\n"
				. "hap|hap\n"
				. "hep|hep\n"
				. "*Group 2\n"
				. "hip|hip\n"
				. "hop|hop\n"
				. "/*\n"
				. 'hup|hup',
				"*Group\n"
				. "hap|hap\n"
				. "hep|hep\n"
				. "*Group 2\n"
				. "hip|hip\n"
				. "hop|hop\n"
				. "/*\n"
				. 'hup|hup',
			],
			'null' => [
				'',
				null,
			]
		];
	}
	/**
	 * @dataProvider dataTableau2chaine
	 * For this test, we disable _T_ou_type()
	 **/
	public function testTableau2chaine($expected, $string) {
		$actual = saisies_tableau2chaine($string);
		$this->assertEquals($expected, $actual);
	}

	public static function dataValeur2tableau() {
		return [
			'null' => [
				// Expected
				[],
				// Valeur
				null
			],
			'formidable' => [
				// Expected
				[
					0 => 'choix1',
					1 => 'choix2',
					'choix_alternatif' => 'Alternatif',
				],
				// Valeur
				[
					0 => 'choix1',
					1 => 'choix2',
					'choix_alternatif' => 'Alternatif',
				],
				// Data
				[
					'choix1' => 'Un',
					'choix2' => 'Deux',
					'choix3' => 'Trois',
				]
			],
			'cextras' => [
				// Expected
				[
					0 => 'choix1',
					1 => 'choix2',
					'choix_alternatif' => 'Alternatif',
				],
				// Valeur
				'choix1,choix2,Alternatif',
				// Data
				[
					'choix1' => 'Un',
					'choix2' => 'Deux',
					'choix3' => 'Trois',
				]
			],
			'unknow' => [
				// Expected
				[
					0 => 'choix1',
					1 => 'choix2',
					'choix_alternatif' => 'Alternatif',
				],
				// Valeur
				"choix1\r\nchoix2\r\nAlternatif",
				// Data
				[
					'choix1' => 'Un',
					'choix2' => 'Deux',
					'choix3' => 'Trois',
				]
			]
		];
	}

	/**
	 * @dataProvider dataValeur2tableau
	 */
	public function testValeur2tableau($expected, $valeur, $data = []) {
		$actual = saisies_valeur2tableau($valeur, $data);
		$this->assertEquals($expected, $actual);
	}

	public static function dataTrouverChoixAlternatif() {
		return [
			[
				'alternatif',
				[
					'choix1' => 'Un',
					'choix2' => 'Deux',
					'choix3' => 'Trois',
				],
				[
					'choix1',
					'choix_alternatif' => 'alternatif'
				]
			],
			[
				'',
				"choix1|Un\r\n"
				. "choix2|Deux\r\n"
				. "choix3|Trois\r\n",
				[
					'choix1',
				]
			],
			[
				'alternatif',
				[
					'choix1' => 'Un',
					'choix2' => 'Deux',
					'choix3' => 'Trois',
				],
				"choix1|choix1\nchoix_alternatif|alternatif",
			]
		];
	}

	/**
	 * @dataProvider dataTrouverChoixAlternatif
	 */
	public function testTrouverChoixAlternatif($expected, $data, $valeur) {
		$actual = saisies_trouver_choix_alternatif($data, $valeur);
		$this->assertEquals($expected, $actual);
	}

	public static function dataAplatirTableau() {
		return [
			'masquer_true' => [
				// Expected
				[
					'choix1' => '1',
					'choix2' => '2',
					'choix3' => '3',
					'choix4' => '4'
				],
				//Provided
				[
					'sous' => [
						'choix1' => '1',
						'choix2' => '2',
					],
					'sous2' => [
						'choix3' => '3',
					],
					'choix4' => '4'
				],
				True
			],
			'masquer_false' => [
				// Expected
				[
					'choix1' => _T('saisies:saisies_aplatir_tableau_montrer_groupe',['valeur' => '1', 'groupe' => 'sous']),
					'choix2' => _T('saisies:saisies_aplatir_tableau_montrer_groupe',['valeur' => '2', 'groupe' => 'sous']),
					'choix3' => _T('saisies:saisies_aplatir_tableau_montrer_groupe',['valeur' => '3', 'groupe' => 'sous2']),
					'choix4' => '4'
				],
				//Provided
				[
					'sous' => [
						'choix1' => '1',
						'choix2' => '2',
					],
					'sous2' => [
						'choix3' => '3',
					],
					'choix4' => '4'
				],
				False
			],
			'chaine_masquer_true' => [
				// Expected
				[
					'choix1' => '1',
					'choix2' => '2',
					'choix3' => '3',
					'choix4' => '4'
				],
				//Provided
				"*sous*\r\n"
				. "choix1|1\r\n"
				. "choix2|2\r\n"
				. "*sous2\r\n"
				. "choix3|3\r\n"
				. "/*\r\n"
				. "choix4|4\r\n"
				,
				True
			]
		];
	}
	/**
	 * @dataProvider dataAplatirTableau
	**/
	public function testAplatirTableau($expected, $provided, $masquer_sous_groupe = false) {
		$actual = saisies_aplatir_tableau($provided, $masquer_sous_groupe);
		$this->assertEquals($expected, $actual);
	}

	public static function dataTrouverData() {
		require_once dirname(__DIR__) . '/inc/saisies_data.php';
		$data = [
			'choix1' => '1',
			'choix2' => '2',
			'choix3' => '3',
		];
		$disable_choix = 'choix1 , choix3';
		return [
			'datas' => [
				//Expected
				$data,
				// Saisie
				[
					'saisie' => 'select',
					'options' => [
						'datas' => $data
					]
				],
				//Disable choix
				false
			],
			'data' => [
				//Expected
				$data,
				// Saisie
				[
					'saisie' => 'select',
					'options' => [
						'data' => $data
					]
				],
				//Disable choix
				false
			],
			'string' => [
				//Expected
				$data,
				// Saisie
				[
					'saisie' => 'select',
					'options' => [
						'data' => saisies_tableau2chaine($data)
					]
				],
				//Disable choix
				false
			],
			'nope' => [
				//Expected
				[],
				// Saisie
				[
					'saisie' => 'select',
					'options' => [
					]
				],
				//Disable choix
				false
			],
			'disable' => [
				//Expected
				['choix2' => 2],
				// Saisie
				[
					'saisie' => 'select',
					'options' => [
						'data' => $data,
						'disable_choix' => $disable_choix
					]
				],
				//Disable choix
				True
			],
		];
	}

	/**
	 * @dataProvider dataTrouverData
	*/
	public function testTrouverData($expected, $saisie, $disable_choix) {
		$actual = saisies_trouver_data($saisie, $disable_choix);
		$this->assertEquals($expected, $actual);
	}

	public static function dataNormaliserListeChoix() {
		return [
			'normal' => [
				['choix1','choix2','choix3'],
				' choix1,choix2 ,choix3, , '
			],
			'null' => [
				[],
				null
			],
			'array' => [
				['choix1','choix2','choix3'],
				['choix1  ','  choix2','choix3']
			],
			'wtf' => [
				[],
				''
			],
		];
	}

	/**
	 * @dataProvider dataNormaliserListeChoix
	**/
	public function testNormaliserListeChoix($expected, $provided) {
		$actual = saisies_normaliser_liste_choix($provided);
		$this->assertEquals($expected, $actual);
	}

	public static function dataDepublierData() {
		return [
			'chaine_chaine' => [
				// Expected
				[
					'choix1' => '1',
					'choix2' => '2',
					'choix3' => '3',
				],
				// Data
				[
					'choix1' => '1',
					'choix2' => '2',
					'choix3' => '3',
					'choix4' => '4',
				],
				// Depublie_choix
				'choix4,   choix3',
				// Valeur
				'choix3'
			],
			'array_chaine' => [
				// Expected
				[
					'choix1' => '1',
					'choix2' => '2',
					'choix3' => '3',
				],
				// Data
				[
					'choix1' => '1',
					'choix2' => '2',
					'choix3' => '3',
					'choix4' => '4',
				],
				// Depublie_choix
				['choix4','choix3'],
				// Valeur
				'choix3'
			],
			'sous_data_array_chaine' => [
				// Expected
				[
					'groupe' => [
						'choix1' => '1',
						'choix2' => '2',
						'choix3' => '3',
					]
				],
				// Data
				[
					'groupe' => [
						'choix1' => '1',
						'choix2' => '2',
						'choix3' => '3',
						'choix4' => '4',
					]
				],
				// Depublie_choix
				['choix4','choix3'],
				// Valeur
				'choix3'
			],
			'sous_data_array_chaine_ca_vide' => [
				// Expected
				[],
				// Data
				[
					'groupe' => [
						'choix3' => '3',
						'choix4' => '4',
					]
				],
				// Depublie_choix
				['choix4','choix3'],
				// Valeur
				''
			],
			'array_array' => [
				// Expected
				[
					'choix1' => '1',
					'choix2' => '2',
					'choix3' => '3',
					'choix5' => '5',
				],
				// Data
				[
					'choix1' => '1',
					'choix2' => '2',
					'choix3' => '3',
					'choix4' => '4',
					'choix5' => '5',
				],
				// Depublie_choix
				['choix4','choix3'],
				// Valeur
				['choix3', 'choix5']
			],
			'null' => [
				// Expected
				[
				],
				// Data
				null,
				// Depublie_choix
				null,
				// Valeur
				null
			]

		];
	}

	/**
	 * @dataProvider dataDepublierData
	**/
	public function testDepublierData($expected, $data, $depublie_choix, $valeur) {
		$actual = saisies_depublier_data($data, $depublie_choix, $valeur);
		$this->assertEquals($expected, $actual);
	}

	public static function dataListerNecessiteRetenirAncienneValeur() {
		$base = [
			[
				'saisie' => 'radio',
				'options' => [
					'nom' => 'si_global',
					'depublie_choix' => 'a'
				]
			],
			[
				'saisie' => 'radio',
				'options' => [
					'nom' => 'toujours',
					'depublie_choix' => 'a'
				],
				'verifier' => [
					[
						'type' => 'valeurs_acceptables'
					]
				]
			],
			[
				'saisie' => 'radio',
				'options' => [
					'nom' => 'toujours2',
					'depublie_choix' => 'a'
				],
				'verifier' => [
					'type' => 'valeurs_acceptables',
					'options' => []
				]
			],
			[
				'saisie' => 'radio',
				'options' => [
					'nom' => 'jamais'
				]
			]
		];
		return [
			'global' => [
				//Expected
				['toujours', 'toujours2', 'si_global'],
				// provided
				array_merge($base, ['options' => ['verifier_valeurs_acceptables' => true]])
			],
			'toujours' => [
				//Expected
				['toujours2', 'toujours'],
				// provided
				$base
			]
		];
	}

	/**
	 * @dataProvider dataListerNecessiteRetenirAncienneValeur
	 * @uses saisies_lister_avec_option()
	 * @uses saisies_lister_par_nom()
	**/
	public function testListerNecessiteRetenirAncienneValeur($expected, $saisies) {
		$actual = saisies_lister_necessite_retenir_ancienne_valeur($saisies);
		$this->assertEqualsCanonicalizing($expected, $actual);
	}
}
