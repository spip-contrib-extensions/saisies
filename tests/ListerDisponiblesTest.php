<?php

namespace Spip\Saisies\Tests;

use PHPUnit\Framework\TestCase;

/**
 * @covers saisies_charger_infos()
 * @covers saisies_recuperer_heritage()
 * @covers saisies_lister_disponibles()
 * @covers saisies_lister_disponibles_sql()
 * @covers saisies_lister_disponibles_par_categories()
 * @covers saisies_lister_disponibles_par_categories_usort()
 * @covers saisies_lister_disponibles_sql_par_categories()
 * @uses saisies_supprimer_identifiants()
 * @uses saisies_lister_categories()
 * @uses saisie_identifier()
 * @uses saisies_chercher()
 * @uses saisies_inserer()
 * @uses saisies_inserer_apres()
 * @uses saisies_inserer_avant()
 * @uses saisies_inserer_selon_chemin()
 * @uses saisies_modifier()
 * @uses saisies_supprimer()
 * @uses saisies_lister_champs()
 * @uses saisies_lister_par_nom()
 * @uses saisies_lister_par_etapes()
 * @uses saisies_afficher_si_liste_masquees()
 * @uses saisie_editable()
 * @uses saisies_supprimer_option()
 * @uses caractere_utf_8
 * @uses charset2unicode
 * @uses corriger_caracteres
 * @uses corriger_caracteres_windows
 * @uses html2unicode
 * @uses init_mb_string
 * @uses load_charset
 * @uses saisie_nom2name
 * @uses saisies_request
 * @uses supprimer_caracteres_illegaux
 * @uses translitteration
 * @uses translitteration_rapide
 * @uses unicode_to_utf_8
 * @uses spip_log
 * @uses spip_logger
 * @internal
 */

class ListerDisponiblesTest extends TestCase {

	public static function dataListerDisponibles() {
		return [
			'obsolete' => [
				// Expected
				['ancetre', 'enfant_avant_apres', 'enfant_fusion', 'enfant_pasfusion', 'obsolete', 'petitenfant', 'sans_sql'],
				'saisies',
				true
			],
			'pas_obsolete' => [
				// Expected
				['ancetre', 'enfant_avant_apres', 'enfant_fusion', 'enfant_pasfusion', 'petitenfant', 'sans_sql'],
				'saisies',
				true
			]
		];
	}


	/**
	 * @dataProvider dataListerDisponibles()
	 * @covers saisies_lister_disponibles()
	 * @covers saisies_recuperer_heritage()
	 * @runInSeparateProcess
	**/
	public function testListerDisponibles($expected, $saisies_repertoire, $inclure_obsolete) {
		define('_DIR_PLUGIN_YAML', true);
		$actual = saisies_lister_disponibles($saisies_repertoire, $inclure_obsolete);
		// We simplify test
		$actual = array_keys($actual);// Only check the saisie name
		$actual = sort($actual);// Order does not matter for this test
		$expected = sort($expected);
		$this->assertEquals($expected, $actual);
	}


	public static function dataListerDisponiblesSql() {
		return [
			'obsolete' => [
				// Expected
				['ancetre', 'enfant_avant_apres', 'enfant_fusion', 'enfant_pasfusion', 'obsolete', 'petitenfant'],
				'saisies',
				true
			],
			'pas_obsolete' => [
				// Expected
				['ancetre', 'enfant_avant_apres', 'enfant_fusion', 'enfant_pasfusion', 'petitenfant'],
				'saisies',
				true
			]
		];
	}

	/**
	 * @dataProvider dataListerDisponiblesSql()
	 * @runInSeparateProcess
	**/
	public function testListerDisponiblesSql($expected, $saisies_repertoire, $inclure_obsolete) {
		define('_DIR_PLUGIN_YAML', true);
		$actual = saisies_lister_disponibles_sql($saisies_repertoire, $inclure_obsolete);
		// We simplify test
		$actual = array_keys($actual);// Only check the saisie name
		$actual = sort($actual);// Order does not matter for this test
		$expected = sort($expected);
		$this->assertEquals($expected, $actual);
	}
	public static function dataChargerInfos() {

		return  [
			[
				[
					'titre' => 'toto',
					'description' => 'toto_description',
					'icone' => '',
					'categorie' => [
						'type' => 'libre',
						'rang' => 3
					],
					'options' =>
					[
						[
							'saisie' => 'fieldset',
							'options' =>
							[
								'nom' => 'description',
								'label' => '<:saisies:option_groupe_description:>',
							],
							'saisies' =>
							[
								[
									'saisie' => 'input',
									'options' =>
									[
										'nom' => 'une_option_en_plus_dans_la_description_tout_au_debut',
										'label' => 'une_option_en_plus',
									],
								],
								[
									'saisie' => 'input',
									'options' =>
									[
										'nom' => 'label',
										'label' => '<:saisies:option_label_label:>',
										'explication' => '<:saisies:option_label_explication:>',
									],
								],
								[
									'options' =>
									[
										'nom' => 'defaut',
										'label' => 'toto',
									],
									'verifier' =>
									[
										'type' => 'toto_verifier',
										'options' =>
										[
											'normaliser' => true,
										],
									],
									'saisie' => 'toto',
								],
								[
									'saisie' => 'input',
									'options' =>
									[
										'nom' => 'explication',
										'label' => '<:saisies:option_explication_label:>',
										'explication' => '<:saisies:option_explication_explication:>',
									],
								],
								[
									'saisie' => 'input',
									'options' =>
									[
										'nom' => 'une_option_en_plus_dans_la_description_tout_a_la_fin',
										'label' => 'une_option_en_plus',
									],
								],
							],
						],
						[
							'saisie' => 'fieldset',
							'options' =>
							[
								'nom' => 'utilisation',
								'label' => '<:saisies:option_groupe_utilisation:>',
							],
							'saisies' =>
							[
								[
									'saisie' => 'input',
									'options' =>
									[
										'nom' => 'maxlength',
										'label' => '<:saisies:option_maxlength_label:>',
										'explication' => '<:saisies:option_maxlength_explication:>',
									],
									'verifier' =>
									[
										'type' => 'entier',
										'options' =>
										[
											'min' => 1,
										],
									],
								],
								[
									'saisie' => 'case',
									'options' =>
									[
										'nom' => 'disable',
										'label_case' => '<:saisies:option_disable_label:>',
										'explication' => '<:saisies:option_disable_explication:>',
									],
								],
								[
									'saisie' => 'case',
									'options' =>
									[
										'nom' => 'disable_avec_post',
										'label_case' => '<:saisies:option_disable_avec_post_label:>',
										'explication' => '<:saisies:option_disable_avec_post_explication:>',
									],
								],
								[
									'saisie' => 'case',
									'options' =>
									[
										'nom' => 'readonly',
										'label_case' => '<:saisies:option_readonly_label:>',
										'explication' => '<:saisies:option_readonly_explication:>',
									],
								],
							],
						],
						[
							'saisie' => 'fieldset',
							'options' =>
							[
								'nom' => 'affichage',
								'label' => '<:saisies:option_groupe_affichage:>',
							],
							'saisies' =>
							[
								[
									'saisie' => 'textarea',
									'options' =>
									[
										'nom' => 'afficher_si',
										'label' => '<:saisies:option_afficher_si_label:>',
										'explication' => '<:saisies:option_afficher_si_explication:>',
										'rows' => 5,
									],
									'verifier' =>
									[
										'type' => 'afficher_si',
									],
								],
								[
									'saisie' => 'case',
									'options' =>
									[
										'nom' => 'afficher_si_remplissage_uniquement',
										'label' => '<:saisies:option_afficher_si_remplissage_uniquement_label:>',
										'label_case' => '<:saisies:option_afficher_si_remplissage_uniquement_label_case:>',
										'explication' => '<:saisies:option_afficher_si_remplissage_uniquement_explication:>',
									],
								],
								[
									'saisie' => 'input',
									'options' =>
									[
										'nom' => 'attention',
										'label' => '<:saisies:option_attention_label:>',
										'explication' => '<:saisies:option_attention_explication:>',
									],
								],
								[
									'saisie' => 'input',
									'options' =>
									[
										'nom' => 'class',
										'label' => '<:saisies:option_class_label:>',
									],
								],
								[
									'saisie' => 'input',
									'options' =>
									[
										'nom' => 'conteneur_class',
										'label' => '<:saisies:option_conteneur_class_label:>',
									],
								],
								[
									'saisie' => 'input',
									'options' =>
									[
										'nom' => 'size',
										'label' => '<:saisies:option_size_label:>',
										'explication' => '<:saisies:option_size_explication:>',
									],
									'verifier' =>
									[
										'type' => 'entier',
										'options' =>
										[
											'min' => 1,
										],
									],
								],
								[
									'saisie' => 'radio',
									'options' =>
									[
										'nom' => 'autocomplete',
										'label' => '<:saisies:option_autocomplete_label:>',
										'explication' => '<:saisies:option_autocomplete_explication:>',
										'datas' =>
										[
											'defaut' => '<:saisies:option_autocomplete_defaut:>',
											'on' => '<:saisies:option_autocomplete_on:>',
											'off' => '<:saisies:option_autocomplete_off:>',
										],
										'defaut' => 'defaut',
									],
								],
							],
						],
						[
							'saisie' => 'fieldset',
							'options' =>
							[
								'nom' => 'validation',
								'label' => '<:saisies:option_groupe_validation:>',
							],
							'saisies' =>
							[
								[
									'saisie' => 'case',
									'options' =>
									[
										'nom' => 'obligatoire',
										'label_case' => '<:saisies:option_obligatoire_label:>',
									],
								],
								[
									'saisie' => 'input',
									'options' =>
									[
										'nom' => 'info_obligatoire',
										'label' => '<:saisies:option_info_obligatoire_label:>',
										'explication' => '<:saisies:option_info_obligatoire_explication:>',
									],
								],
								[
									'saisie' => 'input',
									'options' =>
									[
										'nom' => 'erreur_obligatoire',
										'label' => '<:saisies:option_erreur_obligatoire_label:>',
										'explication' => '<:saisies:option_erreur_obligatoire_explication:>',
									],
								],
							],
						],
					],
					'defaut' =>
					[
						'options' =>
						[
							'label' => 'toto',
							'sql' => 'BIGINT (255)',
							'readonly' => 'on',
						],
						'verifier' =>
						[
							'type' => 'toto',
							'options' =>
							[
								'normaliser' => true,
							],
						],
					],
				],
				'enfant_pasfusion',
			],
			[
				[
					'titre' => 'titi',
					'description' => 'titi_description',
					'categorie' => [
						'type' => 'defaut',
						'rang' => 10
					],
					'icone' => '',
					'options' =>
					[
						[
							'saisie' => 'fieldset',
							'options' =>
							[
								'nom' => 'description',
								'label' => '<:saisies:option_groupe_description:>',
							],
							'saisies' =>
							[
								[
									'saisie' => 'input',
									'options' =>
									[
										'nom' => 'une_option_en_plus_dans_la_description_tout_au_debut',
										'label' => 'une_option_en_plus',
									],
								],
								[
									'saisie' => 'input',
									'options' =>
									[
										'nom' => 'label',
										'label' => '<:saisies:option_label_label:>',
										'explication' => '<:saisies:option_label_explication:>',
									],
								],
								[
									'options' =>
									[
										'nom' => 'defaut',
										'label' => 'toto',
									],
									'verifier' =>
									[
										'type' => 'toto_verifier',
										'options' =>
										[
											'normaliser' => true,
										],
									],
									'saisie' => 'toto',
								],
								[
									'saisie' => 'input',
									'options' =>
									[
										'nom' => 'explication',
										'label' => '<:saisies:option_explication_label:>',
										'explication' => '<:saisies:option_explication_explication:>',
									],
								],
								[
									'saisie' => 'input',
									'options' =>
									[
										'nom' => 'une_option_en_plus_dans_la_description_tout_a_la_fin',
										'label' => 'une_option_en_plus',
									],
								],
							],
						],
						[
							'saisie' => 'fieldset',
							'options' =>
							[
								'nom' => 'utilisation',
								'label' => '<:saisies:option_groupe_utilisation:>',
							],
							'saisies' =>
							[
								[
									'saisie' => 'input',
									'options' =>
									[
										'nom' => 'maxlength',
										'label' => '<:saisies:option_maxlength_label:>',
										'explication' => '<:saisies:option_maxlength_explication:>',
									],
									'verifier' =>
									[
										'type' => 'entier',
										'options' =>
										[
											'min' => 1,
										],
									],
								],
								[
									'saisie' => 'case',
									'options' =>
									[
										'nom' => 'disable',
										'label_case' => '<:saisies:option_disable_label:>',
										'explication' => '<:saisies:option_disable_explication:>',
									],
								],
								[
									'saisie' => 'case',
									'options' =>
									[
										'nom' => 'disable_avec_post',
										'label_case' => '<:saisies:option_disable_avec_post_label:>',
										'explication' => '<:saisies:option_disable_avec_post_explication:>',
									],
								],
								[
									'saisie' => 'case',
									'options' =>
									[
										'nom' => 'readonly',
										'label_case' => '<:saisies:option_readonly_label:>',
										'explication' => '<:saisies:option_readonly_explication:>',
									],
								],
							],
						],
						[
							'saisie' => 'fieldset',
							'options' =>
							[
								'nom' => 'affichage',
								'label' => '<:saisies:option_groupe_affichage:>',
							],
							'saisies' =>
							[
								[
									'saisie' => 'textarea',
									'options' =>
									[
										'nom' => 'afficher_si',
										'label' => '<:saisies:option_afficher_si_label:>',
										'explication' => '<:saisies:option_afficher_si_explication:>',
										'rows' => 5,
									],
									'verifier' =>
									[
										'type' => 'afficher_si',
									],
								],
								[
									'saisie' => 'case',
									'options' =>
									[
										'nom' => 'afficher_si_remplissage_uniquement',
										'label' => '<:saisies:option_afficher_si_remplissage_uniquement_label:>',
										'label_case' => '<:saisies:option_afficher_si_remplissage_uniquement_label_case:>',
										'explication' => '<:saisies:option_afficher_si_remplissage_uniquement_explication:>',
									],
								],
								[
									'saisie' => 'input',
									'options' =>
									[
										'nom' => 'attention',
										'label' => '<:saisies:option_attention_label:>',
										'explication' => '<:saisies:option_attention_explication:>',
									],
								],
								[
									'saisie' => 'input',
									'options' =>
									[
										'nom' => 'class',
										'label' => '<:saisies:option_class_label:>',
									],
								],
								[
									'saisie' => 'input',
									'options' =>
									[
										'nom' => 'conteneur_class',
										'label' => '<:saisies:option_conteneur_class_label:>',
									],
								],
								[
									'saisie' => 'input',
									'options' =>
									[
										'nom' => 'size',
										'label' => '<:saisies:option_size_label:>',
										'explication' => '<:saisies:option_size_explication:>',
									],
									'verifier' =>
									[
										'type' => 'entier',
										'options' =>
										[
											'min' => 1,
										],
									],
								],
								[
									'saisie' => 'radio',
									'options' =>
									[
										'nom' => 'autocomplete',
										'label' => '<:saisies:option_autocomplete_label:>',
										'explication' => '<:saisies:option_autocomplete_explication:>',
										'datas' =>
										[
											'defaut' => '<:saisies:option_autocomplete_defaut:>',
											'on' => '<:saisies:option_autocomplete_on:>',
											'off' => '<:saisies:option_autocomplete_off:>',
										],
										'defaut' => 'defaut',
									],
								],
							],
						],
						[
							'saisie' => 'fieldset',
							'options' =>
							[
								'nom' => 'validation',
								'label' => '<:saisies:option_groupe_validation:>',
							],
							'saisies' =>
							[
								[
									'saisie' => 'case',
									'options' =>
									[
										'nom' => 'obligatoire',
										'label_case' => '<:saisies:option_obligatoire_label:>',
									],
								],
								[
									'saisie' => 'input',
									'options' =>
									[
										'nom' => 'info_obligatoire',
										'label' => '<:saisies:option_info_obligatoire_label:>',
										'explication' => '<:saisies:option_info_obligatoire_explication:>',
									],
								],
								[
									'saisie' => 'input',
									'options' =>
									[
										'nom' => 'erreur_obligatoire',
										'label' => '<:saisies:option_erreur_obligatoire_label:>',
										'explication' => '<:saisies:option_erreur_obligatoire_explication:>',
									],
								],
							],
						],
					],
					'defaut' =>
					[
						'options' =>
						[
							'label' => 'titi',
							'sql' => 'BIGINT (255)',
							'readonly' => 'on',
						],
						'verifier' =>
						[
							'type' => 'toto',
							'options' =>
							[
								'normaliser' => false,
							],
						],
					],
				],
				'petitenfant',
			],
			[
				[
					'titre' => 'tata',
					'description' => 'tata_description',
					'categorie' => [
						'type' => 'libre',
						'rang' => 4,
					],
					'icone' => '',
					'options' =>
					[
						[
							'saisie' => 'fieldset',
							'options' =>
							[
								'nom' => 'description',
								'label' => '<:saisies:option_groupe_description:>',
							],
							'saisies' =>
							[
								[
									'saisie' => 'input',
									'options' =>
									[
										'nom' => 'une_option_en_plus_dans_la_description_tout_au_debut',
										'label' => 'une_option_en_plus',
									],
								],
								[
									'saisie' => 'input',
									'options' =>
									[
										'nom' => 'label',
										'label' => '<:saisies:option_label_label:>',
										'explication' => '<:saisies:option_label_explication:>',
									],
								],
								[
									'saisie' => 'tata',
									'options' =>
									[
										'nom' => 'defaut',
										'label' => 'tata',
										'label' => '<:saisies:option_defaut_label:>',
									],
									'verifier' =>
									[
										'type' => 'tata',
										'options' =>
										[
											'normaliser' => true,
										],
									],
								],
								[
									'saisie' => 'input',
									'options' =>
									[
										'nom' => 'explication',
										'label' => '<:saisies:option_explication_label:>',
										'explication' => '<:saisies:option_explication_explication:>',
									],
								],
								[
									'saisie' => 'input',
									'options' =>
									[
										'nom' => 'une_option_en_plus_dans_la_description_tout_a_la_fin',
										'label' => 'une_option_en_plus',
									],
								],
							],
						],
						[
							'saisie' => 'fieldset',
							'options' =>
							[
								'nom' => 'utilisation',
								'label' => '<:saisies:option_groupe_utilisation:>',
							],
							'saisies' =>
							[
								[
									'saisie' => 'input',
									'options' =>
									[
										'nom' => 'maxlength',
										'label' => '<:saisies:option_maxlength_label:>',
										'explication' => '<:saisies:option_maxlength_explication:>',
									],
									'verifier' =>
									[
										'type' => 'entier',
										'options' =>
										[
											'min' => 1,
										],
									],
								],
								[
									'saisie' => 'case',
									'options' =>
									[
										'nom' => 'disable',
										'label_case' => '<:saisies:option_disable_label:>',
										'explication' => '<:saisies:option_disable_explication:>',
									],
								],
								[
									'saisie' => 'case',
									'options' =>
									[
										'nom' => 'disable_avec_post',
										'label_case' => '<:saisies:option_disable_avec_post_label:>',
										'explication' => '<:saisies:option_disable_avec_post_explication:>',
									],
								],
								[
									'saisie' => 'case',
									'options' =>
									[
										'nom' => 'readonly',
										'label_case' => '<:saisies:option_readonly_label:>',
										'explication' => '<:saisies:option_readonly_explication:>',
									],
								],
							],
						],
						[
							'saisie' => 'fieldset',
							'options' =>
							[
								'nom' => 'affichage',
								'label' => '<:saisies:option_groupe_affichage:>',
							],
							'saisies' =>
							[
								[
									'saisie' => 'textarea',
									'options' =>
									[
										'nom' => 'afficher_si',
										'label' => '<:saisies:option_afficher_si_label:>',
										'explication' => '<:saisies:option_afficher_si_explication:>',
										'rows' => 5,
									],
									'verifier' =>
									[
										'type' => 'afficher_si',
									],
								],
								[
									'saisie' => 'case',
									'options' =>
									[
										'nom' => 'afficher_si_remplissage_uniquement',
										'label' => '<:saisies:option_afficher_si_remplissage_uniquement_label:>',
										'label_case' => '<:saisies:option_afficher_si_remplissage_uniquement_label_case:>',
										'explication' => '<:saisies:option_afficher_si_remplissage_uniquement_explication:>',
									],
								],
								[
									'saisie' => 'input',
									'options' =>
									[
										'nom' => 'attention',
										'label' => '<:saisies:option_attention_label:>',
										'explication' => '<:saisies:option_attention_explication:>',
									],
								],
								[
									'saisie' => 'input',
									'options' =>
									[
										'nom' => 'class',
										'label' => '<:saisies:option_class_label:>',
									],
								],
								[
									'saisie' => 'input',
									'options' =>
									[
										'nom' => 'conteneur_class',
										'label' => '<:saisies:option_conteneur_class_label:>',
									],
								],
								[
									'saisie' => 'input',
									'options' =>
									[
										'nom' => 'size',
										'label' => '<:saisies:option_size_label:>',
										'explication' => '<:saisies:option_size_explication:>',
									],
									'verifier' =>
									[
										'type' => 'entier',
										'options' =>
										[
											'min' => 1,
										],
									],
								],
								[
									'saisie' => 'radio',
									'options' =>
									[
										'nom' => 'autocomplete',
										'label' => '<:saisies:option_autocomplete_label:>',
										'explication' => '<:saisies:option_autocomplete_explication:>',
										'datas' =>
										[
											'defaut' => '<:saisies:option_autocomplete_defaut:>',
											'on' => '<:saisies:option_autocomplete_on:>',
											'off' => '<:saisies:option_autocomplete_off:>',
										],
										'defaut' => 'defaut',
									],
								],
							],
						],
						[
							'saisie' => 'fieldset',
							'options' =>
							[
								'nom' => 'validation',
								'label' => '<:saisies:option_groupe_validation:>',
							],
							'saisies' =>
							[
								[
									'saisie' => 'case',
									'options' =>
									[
										'nom' => 'obligatoire',
										'label_case' => '<:saisies:option_obligatoire_label:>',
									],
								],
								[
									'saisie' => 'input',
									'options' =>
									[
										'nom' => 'info_obligatoire',
										'label' => '<:saisies:option_info_obligatoire_label:>',
										'explication' => '<:saisies:option_info_obligatoire_explication:>',
									],
								],
								[
									'saisie' => 'input',
									'options' =>
									[
										'nom' => 'erreur_obligatoire',
										'label' => '<:saisies:option_erreur_obligatoire_label:>',
										'explication' => '<:saisies:option_erreur_obligatoire_explication:>',
									],
								],
							],
						],
					],
					'defaut' =>
					[
						'options' =>
						[
							'label' => 'tata',
							'sql' => 'BIGINT (255)',
							'readonly' => 'on',
						],
						'verifier' =>
						[
							'type' => 'tata',
							'options' =>
							[
								'normaliser' => true,
							],
						],
					],
				],
				'enfant_fusion',
			],
			[
				[
					'titre' => 'tata',
					'description' => 'tata_description',
					'icone' => '',
					'categorie' => [
						'type' => 'libre',
						'rang' => 2,
					],
					'options' =>
					[
						[
							'saisie' => 'fieldset',
							'options' =>
							[
								'nom' => 'description',
								'label' => '<:saisies:option_groupe_description:>',
							],
							'saisies' =>
							[
								[
									'saisie' => 'input',
									'options' =>
									[
										'nom' => 'une option en plus avant le label',
										'label' => 'une_option_en_plus',
									],
								],
								[
									'saisie' => 'input',
									'options' =>
									[
										'nom' => 'label',
										'label' => '<:saisies:option_label_label:>',
										'explication' => '<:saisies:option_label_explication:>',
									],
								],
								[
									'saisie' => 'input',
									'options' =>
									[
										'nom' => 'defaut',
										'label' => '<:saisies:option_defaut_label:>',
									],
								],
								[
									'saisie' => 'input',
									'options' =>
									[
										'nom' => 'explication',
										'label' => '<:saisies:option_explication_label:>',
										'explication' => '<:saisies:option_explication_explication:>',
									],
								],
								[
									'saisie' => 'input',
									'options' =>
									[
										'nom' => 'une option en plus après explication',
										'label' => 'une_option_en_plus',
									],
								],
							],
						],
						[
							'saisie' => 'fieldset',
							'options' =>
							[
								'nom' => 'utilisation',
								'label' => '<:saisies:option_groupe_utilisation:>',
							],
							'saisies' =>
							[
								[
									'saisie' => 'input',
									'options' =>
									[
										'nom' => 'maxlength',
										'label' => '<:saisies:option_maxlength_label:>',
										'explication' => '<:saisies:option_maxlength_explication:>',
									],
									'verifier' =>
									[
										'type' => 'entier',
										'options' =>
										[
											'min' => 1,
										],
									],
								],
								[
									'saisie' => 'case',
									'options' =>
									[
										'nom' => 'disable',
										'label_case' => '<:saisies:option_disable_label:>',
										'explication' => '<:saisies:option_disable_explication:>',
									],
								],
								[
									'saisie' => 'case',
									'options' =>
									[
										'nom' => 'disable_avec_post',
										'label_case' => '<:saisies:option_disable_avec_post_label:>',
										'explication' => '<:saisies:option_disable_avec_post_explication:>',
									],
								],
								[
									'saisie' => 'case',
									'options' =>
									[
										'nom' => 'readonly',
										'label_case' => '<:saisies:option_readonly_label:>',
										'explication' => '<:saisies:option_readonly_explication:>',
									],
								],
							],
						],
						[
							'saisie' => 'fieldset',
							'options' =>
							[
								'nom' => 'affichage',
								'label' => '<:saisies:option_groupe_affichage:>',
							],
							'saisies' =>
							[
								[
									'saisie' => 'textarea',
									'options' =>
									[
										'nom' => 'afficher_si',
										'label' => '<:saisies:option_afficher_si_label:>',
										'explication' => '<:saisies:option_afficher_si_explication:>',
										'rows' => 5,
									],
									'verifier' =>
									[
										'type' => 'afficher_si',
									],
								],
								[
									'saisie' => 'case',
									'options' =>
									[
										'nom' => 'afficher_si_remplissage_uniquement',
										'label' => '<:saisies:option_afficher_si_remplissage_uniquement_label:>',
										'label_case' => '<:saisies:option_afficher_si_remplissage_uniquement_label_case:>',
										'explication' => '<:saisies:option_afficher_si_remplissage_uniquement_explication:>',
									],
								],
								[
									'saisie' => 'input',
									'options' =>
									[
										'nom' => 'attention',
										'label' => '<:saisies:option_attention_label:>',
										'explication' => '<:saisies:option_attention_explication:>',
									],
								],
								[
									'saisie' => 'input',
									'options' =>
									[
										'nom' => 'class',
										'label' => '<:saisies:option_class_label:>',
									],
								],
								[
									'saisie' => 'input',
									'options' =>
									[
										'nom' => 'conteneur_class',
										'label' => '<:saisies:option_conteneur_class_label:>',
									],
								],
								[
									'saisie' => 'input',
									'options' =>
									[
										'nom' => 'size',
										'label' => '<:saisies:option_size_label:>',
										'explication' => '<:saisies:option_size_explication:>',
									],
									'verifier' =>
									[
										'type' => 'entier',
										'options' =>
										[
											'min' => 1,
										],
									],
								],
								[
									'saisie' => 'radio',
									'options' =>
									[
										'nom' => 'autocomplete',
										'label' => '<:saisies:option_autocomplete_label:>',
										'explication' => '<:saisies:option_autocomplete_explication:>',
										'datas' =>
										[
											'defaut' => '<:saisies:option_autocomplete_defaut:>',
											'on' => '<:saisies:option_autocomplete_on:>',
											'off' => '<:saisies:option_autocomplete_off:>',
										],
										'defaut' => 'defaut',
									],
								],
							],
						],
						[
							'saisie' => 'fieldset',
							'options' =>
							[
								'nom' => 'validation',
								'label' => '<:saisies:option_groupe_validation:>',
							],
							'saisies' =>
							[
								[
									'saisie' => 'case',
									'options' =>
									[
										'nom' => 'obligatoire',
										'label_case' => '<:saisies:option_obligatoire_label:>',
									],
								],
								[
									'saisie' => 'input',
									'options' =>
									[
										'nom' => 'info_obligatoire',
										'label' => '<:saisies:option_info_obligatoire_label:>',
										'explication' => '<:saisies:option_info_obligatoire_explication:>',
									],
								],
								[
									'saisie' => 'input',
									'options' =>
									[
										'nom' => 'erreur_obligatoire',
										'label' => '<:saisies:option_erreur_obligatoire_label:>',
										'explication' => '<:saisies:option_erreur_obligatoire_explication:>',
									],
								],
							],
						],
					],
					'defaut' =>
					[
						'options' =>
						[
							'label' => 'tata',
							'sql' => 'BIGINT (255)',
							'readonly' => 'on',
						],
						'verifier' =>
						[
							'type' => 'tata',
							'options' =>
							[
								'normaliser' => true,
							],
						],
					],
				],
				'enfant_avant_apres',
			],
			'sans_titre' => [
				// Expected
				[
					'titre' => 'sans_titre',
					'description' => 'sans_titre_d',
					'icone' => ''
				],
				// Provided
				'sans_titre',
			]
		];
	}

	/**
	 * @dataProvider dataChargerInfos
	 * @covers saisies_charger_infos()
	 * @covers saisies_recuperer_heritage()
	 * @runInSeparateProcess
	 */
	public function testChargerInfos($expected, $type_saisie, $saisies_repertoire = 'saisies') {
		define('_DIR_PLUGIN_YAML', true);
		$actual = saisies_supprimer_identifiants(saisies_charger_infos($type_saisie, $saisies_repertoire));
		$this->assertEquals($expected, $actual);
	}


	public static function dataListerDisponiblesSqlParCategories() {
		return [
			'sans_obsolete' => [
				// Expected
				[
					'libre' => [
						'ancetre',
						'enfant_avant_apres',
						'enfant_pasfusion',
						'enfant_fusion',
					],
					'defaut' => [
						'petitenfant',
					]
				],
			],
		];
	}

	/**
	 * @dataProvider dataListerDisponiblesSqlParCategories
	 * @covers saisies_lister_disponibles_sql_par_categories()
	 * @covers saisies_regrouper_disponibles_par_categories()
	 * @runInSeparateProcess
	**/
	public function testListerDisponiblesSqlParCategories($expected, $options = []) {
		define('_DIR_PLUGIN_YAML', true);
		$actual = saisies_lister_disponibles_sql_par_categories($options);
		// Simplify comparison
		foreach ($actual as &$cate) {
			$cate = array_keys($cate['saisies'] ?? []);
		}
		$actual = array_filter($actual);

		$this->assertEquals($expected, $actual);
	}

	public static function dataListerDisponiblesParCategories() {
		return [
			'avec_obsolete' => [
				// Expected
				[
					'libre' => [
						'ancetre',
						'enfant_avant_apres',
						'enfant_pasfusion',
						'enfant_fusion',
					],
					'structure' => [
						'obsolete',
					],
					'defaut' => [
						'sans_sql',
						'petitenfant',
						'sans_titre',
					]
				],
				// Options
				['inclure_obsoletes' => true],
			],
			'sans_obsolete' => [
				// Expected
				[
					'libre' => [
						'ancetre',
						'enfant_avant_apres',
						'enfant_pasfusion',
						'enfant_fusion',
					],
					'defaut' => [
						'sans_sql',
						'petitenfant',
						'sans_titre',
					]
				],
				// Options
				['inclure_obsoletes' => false],
			]
		];
	}

	/**
	 * @dataProvider dataListerDisponiblesParCategories
	 * @covers saisies_lister_disponibles_par_categories()
	 * @covers saisies_regrouper_disponibles_par_categories()
	 * @runInSeparateProcess
	**/
	public function testListerDisponiblesParCategories($expected, $options = []) {
		define('_DIR_PLUGIN_YAML', true);
		$actual = saisies_lister_disponibles_par_categories($options);
		// Simplify comparison
		foreach ($actual as &$cate) {
			$cate = array_keys($cate['saisies'] ?? []);
		}
		$actual = array_filter($actual);

		$this->assertEquals($expected, $actual);
	}


	public static function dataNecessiteYAML() {
		return [
			['saisies_lister_disponibles'],
			['saisies_charger_infos']
		];
	}
	/**
	 * @dataProvider dataNecessiteYAML
	 * @covers saisies_lister_disponibles()
	 * @covers saisies_charger_infos()
	**/
	public function testNecessiteYAML($f) {
		$this->expectException(\Exception::class);
		$f('hop');
	}

	public static function dataListerDisponiblesParCategoriesUsort() {
		return [
			'explicite' => [
				// Expected
				3,
				// saisie1
				[
					'titre' => 'hop',
					'categorie' => [
						'rang' => 22,
					]
				],
				// saisie2
				[
					'titre' => 'hip',
					'categorie' => [
						'rang' => 19,
					]
				]
			],
			'implicite1' => [
				// Expected
				1,
				// saisie1
				[
					'titre' => 'hop',
				],
				// saisie2
				[
					'titre' => 'hip',
					'categorie' => [
						'rang' => 19,
					]
				]
			],
			'implicite2' => [
				// Expected
				-1,
				// saisie1
				[
					'titre' => 'hop',
					'categorie' => [
						'rang' => 19,
					]
				],
				// saisie2
				[
					'titre' => 'hip',
				]
			],
			'selon_titre' => [
				// Expected
				1,
				// saisie1
				[
					'titre' => 'hop',
				],
				// saisie2
				[
					'titre' => 'hip',
				]
			],
			'selon_titre_inverse' => [
				// Expected
				-1,
				// saisie1
				[
					'titre' => 'hip',
				],
				// saisie2
				[
					'titre' => 'hop',
				]
			],
			'selon_titre_egaux' => [
				// Expected
				0,
				// saisie1
				[
					'titre' => 'hip',
				],
				// saisie2
				[
					'titre' => 'hip',
				]
			]
		];
	}

	/**
	 * @dataProvider dataListerDisponiblesParCategoriesUsort
	*/
	public function testListerDisponiblesParCategoriesUsort($expected, $saisie1, $saisie2) {
		$actual = saisies_lister_disponibles_par_categories_usort($saisie1, $saisie2);
		$this->assertEquals($expected, $actual);
	}

	public static function dataListerCategories() {
		return [
			'defaut+pipeline' => [
				[
				'libre' => [
					'nom' => _T('saisies:categorie_libre_label'),
				],
				'choix' => [
					'nom' =>  _T('saisies:categorie_choix_label'),
				],
				'structure' => [
					'nom' =>  _T('saisies:categorie_structure_label'),
				],
				'objet' => [
					'nom' =>  _T('saisies:categorie_objet_label'),
				],
				'viapipeline' => [
					'nom' => 'viapipeline'
				],
				'defaut' => [
					'nom' =>  _T('saisies:categorie_defaut_label'),
				]
			]
			]
		];
	}

	/**
	 * @dataProvider dataListerCategories
	 * @covers saisies_lister_categories()
	 **/
	public function testListerCategories($expected) {
		$this->assertEquals($expected, saisies_lister_categories());
	}

	public static function dataRegrouperDisponiblesParCategories() {
		$cate_disponibles = saisies_lister_categories();
		return [
			'mal_range' => [
				// Expected
				array_merge($cate_disponibles, [
					'defaut' => [
						'nom' => 'saisies:categorie_defaut_label',
						'saisies' => [
							1 => [
								'titre' => 'mauvaise_cate',
								'categorie' => ['type' => 'mauvaise_cate'],
							],
							0 => [
								'titre' => 'sans_cate',
								'categorie' => ['type' => ''],
							],
						]
					]
				]),
				// Provided
				[
					[
						'titre' => 'sans_cate',
						'categorie' => ['type' => ''],
					],
					[
						'titre' => 'mauvaise_cate',
						'categorie' => ['type' => 'mauvaise_cate'],
					]
				]
			],
			'demande_defaut' => [
				// Expected
				array_merge($cate_disponibles, [
					'defaut' => [
						'nom' => 'saisies:categorie_defaut_label',
						'saisies' => [
							1 => [
								'titre' => 'mauvaise_cate',
								'categorie' => ['type' => 'mauvaise_cate'],
							],
							0 => [
								'titre' => 'sans_cate',
							],
						]
					]
				]),
				// Provided
				[
					[
						'titre' => 'sans_cate',
					],
					[
						'titre' => 'mauvaise_cate',
						'categorie' => ['type' => 'mauvaise_cate'],
					],
					[
						'titre' => 'hors_cate',
						'categorie' => ['type' => 'choix'],
					]
				],
				'defaut'
			]
		];
	}

	/**
	 * @dataProvider dataRegrouperDisponiblesParCategories
	 * @covers saisies_regrouper_disponibles_par_categories()
	**/
	public function testRegrouperDisponiblesParCategories($expected, $saisies, $categorie_demande = null) {
		$actual = saisies_regrouper_disponibles_par_categories($saisies, $categorie_demande);
		$this->assertSame($expected, $actual);
	}

}
