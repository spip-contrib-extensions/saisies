<?php

namespace Spip\Saisies\Tests;

use PHPUnit\Framework\TestCase;

/**
 * @covers saisies_utf8_restaurer_planes()
 * @covers saisies_picker_preselect_objet()
 * @internal
 */

class FonctionsTest extends TestCase {

	public static function dataUtf8RestaurerPlanes() {
		return [
			'SPIP1' => [
				'🐿',
				'&#128063;',
			],
			'SPIP2' => [
				'🐿&#65039;',
				'&#128063;&#65039;',
			],
			'PasSPIP' => [
				'👩🏾',
				'&#128105;&#127998;'
			],
			'null' => [
				'',
				null,
			],
			'empty' => [
				'',
				'',
			]
		]
		;
	}

	/**
	 * @dataProvider dataUtf8RestaurerPlanes
	 */
	public function testUtf8RestaurerPlanes($expected, $actual) {
		$actual = saisies_utf8_restaurer_planes($actual);
		$this -> assertSame($expected, $actual);
	}

	public static function dataPickerPreselectObjet() {
		return [
			'deja_ok' => [
				// Expected
				['article|1', 'article|2'],
				// Valeur
				['article|1', 'article|2'],
				// Objet
				''
			],
			'unique' => [
				// Expected
				['article|1'],
				// Valeur
				1,
				// Objet
				'article'
			],
			'multile' => [
				// Expected
				['article|1', 'article|2'],
				// Valeur
				[1,2],
				// Objet
				'article'
			],
		];
	}


	/**
	 * @dataProvider dataPickerPreselectObjet
	 */
	public function testPickerPreselectObjet($expected, $valeur, $objet) {
		$actual = saisies_picker_preselect_objet($valeur, $objet);
		$this->assertEquals($expected, $actual);
	}

}
