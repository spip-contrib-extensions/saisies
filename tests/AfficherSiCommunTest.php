<?php

namespace Spip\Saisies\Tests;

use PHPUnit\Framework\TestCase;
use PHPUnit\Framework\Attributes\RunInSeparateProcess;
use PHPUnit\Framework\Attributes\DataProvider;
/**
 * @covers saisies_afficher_si_verifier_syntaxe()
 * @covers saisies_tester_condition_afficher_si()
 * @covers saisies_afficher_si_secure()
 * @covers saisies_afficher_si_parser_valeur_MATCH()
 * @covers saisies_afficher_si_get_valeur_config()
 * @covers saisies_afficher_si_filtrer_parse_condition()
 * @covers saisies_afficher_si_evaluer_plugin()
 * @covers saisies_verifier_coherence_afficher_si()
 * @uses saisie_name2nom()
 * @uses saisie_nom2name()
 * For lire_config() Mock
 * @uses saisies_request()
 * For lire_config() Mock
 * @uses spip_log
 * @uses spip_logger
 * @uses singulier_ou_pluriel
 * @internal
 */

class AfficherSiCommunTest extends TestCase {

	public static function dataAfficherSiVerifierSyntaxe() {
		$essais =  [
			'simple_marche' =>
			[
				true,
				"@a@ == 'a'"
			],
			'simple_marche_pas' =>
			[
				false,
				"@a@ == 'a"
			],
			'simple_marche_pas_2' =>
			[
				false,
				"@a@ === 'a'"
			],
			'simple_marche_pas_3' =>
			[
				false,
				"@a == 'a'"
			],
			'double_marche' =>
			[
				true,
				"@a@ == 'a' && @b@ == 'b'"
			],
			'double_marche_pas' =>
			[
				false,
				"@a@ == 'a' && @b@ == 'b' &&"
			],
			'double_marche_pas_2' =>
			[
				false,
				"@a@ == 'a' && @b@ == 'b' ||"
			],
			'double_marche_pas_3' =>
			[
				false,
				"&& @a@ == 'a' && @b@ == 'b'"
			],
			'double_marche_pas_4' =>
			[
				false,
				"|| @a@ == 'a' && @b@ == 'b'"
			],
			'double_marche_pas_5' =>
			[
				false,
				"@a@ == 'a' &&  && @b@ == 'b'"
			],
			'parenthese_marche_pas_5' =>
			[
				false,
				"(@a@ == 'a'"
			],
			'false' =>
			[
				true,
				'false'
			],
			'true' =>
			[
				true,
				'true'
			],
			'total' =>
			[
				true,
				'@checkbox_1@:TOTAL > 1'
			],
			'MATCH_ok' =>
			[
				true,
				'@input_1@  MATCH \'/domaine.ext/\''
			],
			'MATCH_modificateur_ok' =>
			[
				true,
				'@input_1@  MATCH \'/domaine.ext/gm\''
			],
			'MATCH_pas_ok' =>
			[
				false,
				'@input_1@  MATCH \'/domaine.ext\''
			],
			'vide' => [
				true,
				''
			]
		];

		require_once dirname(__DIR__) . '/inc/saisies_afficher_si_commun.php';
		foreach ($essais as $nom => $param) {
			$essais[$nom][2] = \saisies_parser_condition_afficher_si($param[1]);
		}
		return $essais;
	}

	/**
	 * @dataProvider dataAfficherSiVerifierSyntaxe
	 */
	public function testAfficherSiVerifierSyntaxe($expected, $condition, $tests) {
		$actual = saisies_afficher_si_verifier_syntaxe($condition, $tests);
		$this->assertEquals($expected, $actual);
	}


	public static function dataTesterConditionAfficherSi() {
		return  [
			'champ_uniquement' => [
				true,
				'case_1',
				[],
			],
			'chaines_egales_test_egalite' =>  [
				true,
				'a',
				[],
				'==',
				'a',
			],
			'chaines_inegales_test_egalite' =>  [
				false,
				'a',
				[],
				'!=',
				'a',
			],
			'chaines_egales_test_inegalite' =>  [
				false,
				'a',
				[],
				'!=',
				'a',
			],
			'chaines_inegales_test_egalite' =>  [
				false,
				'a',
				[],
				'!=',
				'a',
			],
			'array_presence_test_double_egal' =>  [
				true,
				['2','3'],
				[],
				'==',
				'2',
			],
			'array_presence_test_double_egal_serialize' =>  [
				true,
				serialize(['2','3']),
				[],
				'==',
				'2',
			],
			'array_presence_test_IN' =>  [
				true,
				['2','3'],
				[],
				'IN',
				'2',
			],
			'array_presence_test_IN_serialize' =>  [
				true,
				serialize(['2','3']),
				[],
				'IN',
				'2',
			],
			'array_presence_test_double_egal_faux' =>  [
				false,
				['2','3'],
				[],
				'==',
				'4',
			],
			'array_presence_test_double_egal_faux_serialize' =>  [
				false,
				serialize(['2','3']),
				[],
				'==',
				'4',
			],
			'array_presence_test_IN_faux' =>  [
				false,
				['2','3'],
				[],
				'IN',
				'4',
			],
			'array_presence_test_IN_faux_serialize' =>  [
				false,
				serialize(['2','3']),
				[],
				'IN',
				'4',
			],
			'array_absence_test_negation_faux' =>  [
				false,
				['2','3'],
				[],
				'!=',
				'2',
			],
			'array_absence_test_negation_faux_serialize' =>  [
				false,
				serialize(['2','3']),
				[],
				'!=',
				'2',
			],
			'array_absence_test_NOT_IN_faux' =>  [
				false,
				['2','3'],
				[],
				'!IN',
				'2',
			],
			'array_absence_test_NOT_IN_serialize' =>  [
				false,
				serialize(['2','3']),
				[],
				'!IN',
				'2',
			],
			'array_absence_test_neagation' =>  [
				true,
				['2','3'],
				[],
				'!=',
				'4',
			],
			'array_absence_test_negation_serialize' =>  [
				true,
				serialize(['2','3']),
				[],
				'!=',
				'4',
			],
			'array_absence_test_NOT_IN' =>  [
				true,
				['2','3'],
				[],
				'!IN',
				'4',
			],
			'array_presence_test_NOT_IN_serialize' =>  [
				true,
				serialize(['2','3']),
				[],
				'!IN',
				'4',
			],
			'array_presence_IN_multiple' => [
				true,
				['2'],
				[],
				'IN',
				'2,3'
			],
			'array_absence_IN_multiple' => [
				false,
				['4', '5'],
				[],
				'IN',
				'2,3'
			],
			'array_absence_NOT_IN_multiple' => [
				true,
				['4', '5'],
				[],
				'!IN',
				'2,3'
			],
			'array_presence_NOT_IN_multiple' => [
				false,
				['2', '1'],
				[],
				'!IN',
				'2,3'
			],
			'array_total' => [
				true,
				['1', '2'],
				['fonction' => 'total', 'type_champ' => 'array'],
				'==',
				'2'
			],
			'string_presence_IN_multiple' => [
				true,
				'choix1',
				[],
				'IN',
				'choix1,choix2'
			],
			'string_presence_NOT_IN_multiple' => [
				true,
				'choix3',
				[],
				'!IN',
				'choix1,choix2'
			],
			'string_absence_IN_multiple' => [
				false,
				'choix3',
				[],
				'IN',
				'choix1,choix2'
			],
			'string_absence_NOT_IN_multiple' => [
				false,
				'choix2',
				[],
				'!IN',
				'choix1,choix2'
			],
			'mauvais_operateur_ne_devrait_pas_arriver' => [
				false,
				'choix2',
				[],
				'!!!!!',
				'choix2',
			],
			'zero_sans_operateur' => [
				true,
				'0',
				[],
				'',
				'',
			],
			'negation_sans_operateur' => [
				false,
				'22',
				[],
				'',
				'',
				'!',
			],
			'negation_avec_operateur' => [
				false,
				'22',
				[],
				'==',
				'22',
				'!',
			],
			'null' => [
				true,
				null,
				[],
				'==',
				'',
			],
			'IN_pas_tableau_entree' => [
				true,
				0,
				[],
				'IN',
				0,
			],
			'IN_null_tableau_entree' => [
				false,
				null,
				[],
				'IN',
				0,
			],
			'substr_ok' => [
				true,
				'abcd',
				[
					'fonction' => 'substr',
					'type_champ' => 'string',
					'arguments' => [0, 2],
				],
				'==',
				'ab'
			],
			'substr_pasok' => [
				false,
				'dcba',
				[
					'fonction' => 'substr',
					'type_champ' => 'string',
					'arguments' => [0, 2],
				],
				'==',
				'ab'
			]
		];
	}


	/**
	 * @dataProvider dataTesterConditionAfficherSi
	 * @covers saisies_tester_condition_afficher_si_string()
	 * @covers saisies_tester_condition_afficher_si_array()
	 */
	public function testTesterConditionAfficherSi($expected, $valeur_champ, $modificateur = [], $operateur = '', $valeur = '', $negation = '') {
		$actual = saisies_tester_condition_afficher_si($valeur_champ, $modificateur, $operateur, $valeur, $negation);
		$this->assertEquals($expected, $actual);
	}

	static public function dataAfficherSiSecure() {
		return [
			'false' => [
				// Expected
				false,
				// Condition
				'(eval("alert")) || @champ_1@ > 0',
				[
					[
						'expression' => '@champ_1@ > 0'
					]
				]
			],
			'true' => [
				// Expected
				true,
				// Condition
				'@champ_2@ > 2 || @champ_1@ > 0',
				[
					[
						'expression' => '@champ_1@ > 0'
					],
					[
						'expression' => '@champ_2@ > 2'
					]
				]
			],
			'true_repete' => [
				// Expected
				true,
				// Condition
				'@champ_1@  && @champ_1@ != "tot"',
				[
					[
						'expression' => '@champ_1@'
					],
					[
						'expression' => '@champ_1@ != "tot"'
					]
				]
			]
		];
	}

	/**
	 * Most of the parsing tests are made through
	 * dataEvaluerAfficherSi()
	 * Here, we test only the `no_arobase` case, which is specific.
	**/
	public static function dataParserConditionAfficherSi() {
		return [
			'no_arobase' => [
				// Expected
				[
					[
						'operateur' => '==',
						'valeur' => 'titi',
						'negation' => '',
						'champ' => '',
						'total' => '',
						'modificateur' => [],
						'expression' => '== "titi"'
					]
				],
				//test
				'== "titi"',
				// no_arobase
				true
			],
			'no_arobase_double' => [
				// Expected
				[
					[
						'operateur' => '==',
						'valeur' => 'titi',
						'negation' => '',
						'champ' => '',
						'total' => '',
						'modificateur' => [],
						'expression' => '== "titi"'
					],
					[
						'operateur' => '==',
						'valeur' => 'tata',
						'negation' => '',
						'champ' => '',
						'total' => '',
						'modificateur' => [],
						'expression' => ' == "tata"'
					]
				],
				//test
				'== "titi" || == "tata"',
				// no_arobase
				true
			]
		];
	}

	/**
	 * @dataProvider dataParserConditionAfficherSi
	 * @covers saisies_parser_condition_afficher_si()
	 */
	public function testParserConditionAfficherSi(array $expected, string $condition, $no_arobase = null) {
		$actual = saisies_parser_condition_afficher_si($condition, $no_arobase);
		$this->assertEquals($expected, $actual);
	}

	/**
	 * @dataProvider dataAfficherSiSecure
	 **/
	public function testAfficherSiSecure($expected, $condition, $tests) {
		$actual = saisies_afficher_si_secure($condition, $tests);
		$this->assertEquals($expected, $actual);
	}

	public static function dataAfficherSiParserValeurMATCH() {
		return [
			'sans_modif' => [
				// Expected
				[
					'regexp' => '(.*)',
					'regexp_modif' => ''
				],
				// Provided
				'/(.*)/'
			],
			'avec_modif' => [
				// Expected
				[
					'regexp' => '(.*)',
					'regexp_modif' => 'g'
				],
				// Provided
				'/(.*)/g'
			],
			'nope' => [
				// Expected
				[
					'regexp' => '',
					'regexp_modif' => ''
				],
				// Provided
				'patate'
			]
		];
	}

	/**
	 * @dataProvider dataAfficherSiParserValeurMATCH
	**/
	public function testAfficherSiParserValeurMATCH($expected, $condition) {
		$actual = saisies_afficher_si_parser_valeur_MATCH($condition);
		$this->assertEquals($expected, $actual);
	}

	public static function dataAfficherSiGetValeurConfig() {
		// Quelques fonctions internes à saisies pour écrire plus facilement les tests
		require_once dirname(__DIR__) . '/inc/saisies_lister.php';
		require_once dirname(__DIR__) . '/inc/saisies_request.php';
		require_once dirname(__DIR__) . '/inc/saisies_name.php';
		ecrire_config('cfg', 'hop');
		return [
			'hop' => [
				'hop',
				'config:cfg',
			],
			'vide' => [
				'',
				'config:plop',
			],
			'pasconfig' => [
				'',
				'nononon',
			],
		];

	}

	#[DataProvider('dataAfficherSiGetValeurConfig')]
	public function testAfficherSiGetValeurConfig($expected, $champ) {
		$actual = saisies_afficher_si_get_valeur_config($champ);
		$this->assertEquals($expected, $actual);
	}


	public static function dataAfficherSiFiltrerParseCondition() {
		return [
			'basic' => [
				// Expected
				[
					'expression' => '@toto@ > 1',
					'champ' => 'toto',
					'total' => '',
					'modificateur' => [],
					'operateur' => '>',
					'valeur' => 1
				],
				// Provided
				[
					0 => '@toto@ > 1',
					'champ' => 'toto',
					'operateur' => '>',
					'valeur_numerique' => 1,
					'arobase' => '@toto@',
					'guillemet' => '',
					'total' => '',
					'substr' => '',
					'position' => ''
				],
			],
			'total' => [
				// Expected
				[
					'expression' => '@toto@ > 1',
					'champ' => 'toto',
					'total' => ':TOTAL',
					'modificateur' => [
						'fonction' => 'total',
					  'type_champ' => 'array'
					],
					'operateur' => '>',
					'valeur' => 1
				],
				// Provided
				[
					0 => '@toto@ > 1',
					'champ' => 'toto',
					'operateur' => '>',
					'total' => ':TOTAL',
					'valeur_numerique' => 1,
					'arobase' => '@toto@',
					'guillemet' => '',
					'substr' => '',
					'position' => ''
				],
			],
			'substr' => [
				// Expected
				[
					'expression' => '@toto@ > 1',
					'champ' => 'toto',
					'total' => '',
					'modificateur' => [
						'fonction' => 'substr',
						'type_champ' => 'string',
						'arguments' => [1,2]
					],
					'operateur' => '>',
					'valeur' => 1
				],
				// Provided
				[
					0 => '@toto@ > 1',
					'champ' => 'toto',
					'operateur' => '>',
					'total' => '',
					'valeur_numerique' => 1,
					'arobase' => '@toto@',
					'guillemet' => '',
					'substr' => ':SUBSTR',
					'position' => '1',
					'longueur' => '2'
				],
			]
		];
	}

	/**
	 * @dataProvider dataAfficherSiFiltrerParseCondition
	*/
	public function  testAfficherSiFiltrerParseCondition($expected, $parse) {
		$actual = saisies_afficher_si_filtrer_parse_condition($parse);
		$this->assertEquals($expected, $actual);
	}

	public static function dataAfficherSiEvaluerPlugin() {
		return [
			'saisies' => [
				//Expected
				true,
				// Champ
				'plugin:saisies'
			],
			'saisies_negation' => [
				//Expected
				false,
				// Champ
				'plugin:saisies',
				// Negation
				'!'
			],
			'pas_saisies' => [
				//Expected
				false,
				// Champ
				'plugin:pas_saisies'
			],
			'pas_saisies_negation' => [
				//Expected
				true,
				// Champ
				'plugin:pas_saisies',
				// Negation
				'!'
			],
			'pas_plugin' => [
				//Expected
				false,
				// Champ
				'champ',
			]
		];
	}

	/**
	 * @dataProvider dataAfficherSiEvaluerPlugin
	**/
	public function testAfficherSiEvaluerPlugin($expected, $champ, $negation = '') {
		$actual = saisies_afficher_si_evaluer_plugin($champ, $negation);
		$this->assertEquals($expected, $actual);
	}


	public static function dataVerifierCoherenceAfficherSi() {
		return [
			'config' => [
				//Expected
				'',
				// provided
				[
					[
						'saisie' => 'input',
						'options' =>  [
							'afficher_si' => '@config:toto@',
							'nom' => 'input',
						]
					]
				]
			],
			'plugin' => [
				//Expected
				'',
				// provided
				[
					[
						'saisie' => 'input',
						'options' =>  [
							'afficher_si' => '@plugin:toto@',
							'nom' => 'input',
						]
					]
				]
			],
			'ok' => [
				//Expected
				'',
				// provided
				[
					[
						'saisie' => 'input',
						'options' =>  [
							'afficher_si' => '@ok@',
							'nom' => 'input',
							'label' => 'input'
						]
					],
					[
						'saisie' => 'input',
						'options' =>  [
							'nom' => 'ok',
						]
					]
				]
			],
			'pas_ok_1' => [
				//Expected
				'saisies:coherence_afficher_si_erreur_singulier{"nb":1}<br /> - '._T('saisies:coherence_afficher_si_appel', ['label' => 'input', 'erreurs' => '@pas_ok@']),
				// provided
				[
					[
						'saisie' => 'input',
						'options' =>  [
							'afficher_si' => '@pas_ok@',
							'nom' => 'input',
							'label' => 'input',
						]
					],
					[
						'saisie' => 'input',
						'options' =>  [
							'nom' => 'ok',
						]
					]
				]
			],
			'pas_ok_2' => [
				//Expected
				'saisies:coherence_afficher_si_erreur_pluriel{"nb":2}<br /> - '._T('saisies:coherence_afficher_si_appel', ['label' => 'input', 'erreurs' => '@pas_ok@']).'<br /> - '._T('saisies:coherence_afficher_si_appel', ['label' => 'ok', 'erreurs' => '@pas_ok2@']),
				// provided
				[
					[
						'saisie' => 'input',
						'options' =>  [
							'afficher_si' => '@pas_ok@',
							'nom' => 'input',
							'label' => 'input',
						]
					],
					[
						'saisie' => 'input',
						'options' =>  [
							'nom' => 'ok',
							'label' => 'ok',
							'afficher_si' => '@pas_ok2@',
						]
					]
				]
			],
			'pas_ok_3' => [
				//Expected
				'saisies:coherence_afficher_si_erreur_singulier{"nb":1}<br /> - '._T('saisies:coherence_afficher_si_appel', ['label' => 'input', 'erreurs' => '@pas_ok@, @pas_ok2@']),
				// provided
				[
					[
						'saisie' => 'input',
						'options' =>  [
							'afficher_si' => '@pas_ok@ && @pas_ok2@',
							'nom' => 'input',
							'label' => 'input',
						]
					],
					[
						'saisie' => 'input',
						'options' =>  [
							'nom' => 'ok',
						]
					]
				]
			],
			'grille_ok' => [
				//Expected
				'',
				// provided
				[
					[
						'saisie' => 'input',
						'options' =>  [
							'afficher_si' => '@choix_grille_1[a]@ || @choix_grille_1[b]@',
							'nom' => 'input',
							'label' => 'input',
						]
					],
					[
						'saisie' => 'choix_grille',
						'options' =>  [
							'nom' => 'choix_grille_1',
							'label' => 'choix_grille_1',
							'data_rows' => "a|a\n\rb|b",
							'data_cols' => ['aa' => 'aa', 'bb' => 'bb'],
						]
					]
				]
			],
			'grille_ok_nom' => [
				//Expected
				'',
				// provided
				[
					[
						'saisie' => 'input',
						'options' =>  [
							'afficher_si' => '@choix_grille_1/a@ || @choix_grille_1/b@',
							'nom' => 'input',
							'label' => 'input',
						]
					],
					[
						'saisie' => 'choix_grille',
						'options' =>  [
							'nom' => 'choix_grille_1',
							'label' => 'choix_grille_1',
							'data_rows' => "a|a\n\rb|b",
							'data_cols' => ['aa' => 'aa', 'bb' => 'bb'],
						]
					]
				]
			],
			'grille_pas_ok' => [
				//Expected
				'saisies:coherence_afficher_si_erreur_singulier{"nb":1}<br /> - saisies:coherence_afficher_si_appel{"label":"input","erreurs":"@choix_grille_1[c]@"}',
				// provided
				[
					[
						'saisie' => 'input',
						'options' =>  [
							'afficher_si' => '@choix_grille_1[c]@',
							'nom' => 'input',
							'label' => 'input',
						]
					],
					[
						'saisie' => 'choix_grille',
						'options' =>  [
							'nom' => 'choix_grille_1',
							'label' => 'choix_grille_1',
							'data_rows' => ['a' => 'a', 'b' => 'b'],
							'data_cols' => ['aa' => 'aa', 'bb' => 'bb'],
						]
					]
				]
			],
			'avec_crochet_ok' => [
				//Expected
				'',
				// provided
				[
					[
						'saisie' => 'input',
						'options' =>  [
							'afficher_si' => '@truc[sous]@',
							'nom' => 'input',
							'label' => 'input',
						]
					],
					[
						'saisie' => 'truc',
						'options' =>  [
							'nom' => 'truc[sous]',
							'label' => 'truc/sous',
							'data_rows' => ['a' => 'a', 'b' => 'b'],
							'data_cols' => ['aa' => 'aa', 'bb' => 'bb'],
						]
					]
				]
			]
		];
	}
	/**
	 * @dataProvider dataVerifierCoherenceAfficherSi
	 * @covers saisies_verifier_coherence_afficher_si()
	 * @uses saisies_lister_avec_option()
	 * @uses saisies_lister_par_nom()
	 * @uses saisies_chaine2tableau()
	 * @uses saisie_nom2name()
	 * @covers saisies_verifier_coherence_afficher_si_par_champ()
	*/
	public function testVerifierCoherenceAfficherSi($expected, $saisies) {
		$actual = saisies_verifier_coherence_afficher_si($saisies);
		$this->assertEquals($expected, $actual);
	}
}
