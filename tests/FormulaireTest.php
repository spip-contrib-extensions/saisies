<?php

namespace Spip\Saisies\Tests;


use PHPUnit\Framework\TestCase;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\Attributes\RunInSeparateProcess;
/**
 * @covers saisies_generer_nom()
 * @covers saisies_determiner_deplacement_rapide()
 * @covers saisies_determiner_avance_rapide()
 * @covers saisies_determiner_recul_rapide()
 * @covers saisies_resumer_etapes_futures()
 * @covers saisies_determiner_options_demandees_resumer_etapes_futures()
 * @uses saisies_lister_champs()
 * @uses saisies_lister_par_nom()
 * @uses saisies_lister_par_etapes()
 * @uses saisies_afficher_si_liste_masquees()
 * @uses saisie_editable()
 * @uses saisies_supprimer_option()
 * @uses caractere_utf_8
 * @uses charset2unicode
 * @uses corriger_caracteres
 * @uses corriger_caracteres_windows
 * @uses html2unicode
 * @uses init_mb_string
 * @uses load_charset
 * @uses saisie_nom2name
 * @uses saisies_request
 * @uses supprimer_caracteres_illegaux
 * @uses translitteration
 * @uses translitteration_rapide
 * @uses unicode_to_utf_8
 * @uses spip_log
 * @uses spip_logger
 */
class FormulaireTest extends TestCase {

	public static function dataGenereNom() {
		return [
			'input' => [
				// Expected
				'input_3',
				// Formulaire
				[
					[
						'saisie' => 'input',
						'options' => [
							'nom' => 'input_1'
						]
					],
					[
						'saisie' => 'input',
						'options' => [
							'nom' => 'input_2'
						]
					],
					[
						'saisie' => 'input',
						'options' => [
							'nom' => 'input_4'
						]
					],
				],
				// Type_saisie
				'input'
			]
		];
	}

	/**
	 * @dataProvider dataGenereNom
	*/
	public function testGenererNom($expected, $formulaire, $type_saisie) {
		$actual = saisies_generer_nom($formulaire, $type_saisie);
		$this->assertEquals($expected, $actual);
	}

	public static function dataDeterminerDeplacementRapide() {
		$nom_saisies_masquees = ['fieldset_2', 'fieldset_3', 'fieldset_4'];
		$saisies_masquees = [];
		$saisies = [];

		$i = 1;
		while ($i < 6) {
			$saisies[] = [
				'saisie' => 'fieldset',
				'options' => [
					'nom' => "fieldset_$i",
					'label' => "fieldset_$i",
					'afficher_si' => 'false',
					'bidon' => 'bidon',
				]
			];
			if (in_array("fieldset_$i", $nom_saisies_masquees)) {
				$saisies_masquees[] = $saisies[$i -1];
			}
			$i++;
		}
		$saisies['options'] = [
			'etapes_activer' => 'on',
			'etapes_ignorer_recapitulatif' => 'on',
		];

		// 6 and 7
		$saisies[] = [
			'saisie' => 'fieldset',
			'options' => [
				'nom' => "fieldset_$i",
				'label' => "fieldset_$i",
			]
		];
		$saisies[] = [
			'saisie' => 'fieldset',
			'options' => [
				'nom' => "fieldset_$i",
				'label' => "fieldset_$i",
			]
		];
		return [
			'avance_rapide' => [
				// Expected
				5,
				// Saisies
				$saisies,
				// Etape
				1,
				// Sens
				1,
				// Saisies masquées
				$saisies_masquees
			],
			'avance_rapide_deja_trie' => [
				// Expected
				5,
				// Saisies
				saisies_lister_par_etapes($saisies),
				// Etape
				1,
				// Sens
				1,
				// Saisies masquées
				$saisies_masquees
			],
			'avance_lent' => [
				// Expected
				6,
				// Saisies
				$saisies,
				// Etape
				5,
				// Sens
				1,
				// Saisies masquées
				$saisies_masquees
			],
			'recul_rapide' => [
				// Expected
				1,
				// Saisies
				$saisies,
				// Etape
				5,
				// Sens
				-1,
				// Saisies masquées
				$saisies_masquees
			],
			'recul_lent' => [
				// Expected
				5,
				// Saisies
				$saisies,
				// Etape
				6,
				// Sens
				-1,
				// Saisies masquées
				$saisies_masquees
			],
			'mauvaise_sens' => [
				// Expected
				6,
				// Saisies
				$saisies,
				// Etape
				6,
				// Sens
				-222,
				// Saisies masquées
				$saisies_masquees
			]
		];
	}

	#[DataProvider('dataDeterminerDeplacementRapide')]
	#[Uses('saisies_supprimer_option')]
	public function testDeterminerDeplacementRapide($expected, $saisies, $etape, $sens, $saisies_masquees) {
		foreach ($saisies_masquees as $s) {
			saisies_afficher_si_liste_masquees('set', $s);
		}
		// Utiliser le nom comme point de référence, donc si jamais pour x raison les sous saisies ne sont pas les memes, et bien tant pis
		$saisies = saisies_supprimer_option($saisies, 'bidon');
		$actual = saisies_determiner_deplacement_rapide($saisies, $etape, $sens);
		$this->assertEquals($expected, $actual);
	}

	#[DataProvider('dataDeterminerDeplacementRapide')]
	public function testDeterminerReculRapide($expected, $saisies, $etape, $sens, $saisies_masquees) {
		foreach ($saisies_masquees as $s) {
			saisies_afficher_si_liste_masquees('set', $s);
		}
		if ($sens === -1) {
			$actual = saisies_determiner_recul_rapide($saisies, $etape);
			$this->assertEquals($expected, $actual);
		} else {
			$this->assertTrue(True);
		}
	}

	#[DataProvider('dataDeterminerDeplacementRapide')]
	public function testDeterminerAvanceRapide($expected, $saisies, $etape, $sens, $saisies_masquees) {
		foreach ($saisies_masquees as $s) {
			saisies_afficher_si_liste_masquees('set', $s);
		}
		if ($sens === 1) {
			$actual = saisies_determiner_avance_rapide($saisies, $etape);
			$this->assertEquals($expected, $actual);
		} else {
			$this->assertTrue(True);
		}
	}

	static public function dataResumerEtapesFutures(): array {
		return [
			'avec_titre' => [
				// Expected
				[
						'etape_3' => [
							'afficher_si' => '@toto@ == "hop"',
							'label' => 'etape 3',
						],
						'etape_4' => [
							'afficher_si' => 'false',
							'label' => 'etape 4',
						],
						'etape_5' => [
							'afficher_si' => 'true',
							'label' => 'etape 5',
						],
				],
				// Etapes
				[
					'etape_1' => [
						'options' => [
							'label' => 'etape 1',
							'afficher_si' => 'false',
						],
						'saisies' => []
					],
					'etape_2' => [
						'options' => [
							'label' => 'etape 2',
						],
						'saisies' => [
							[
								'saisie' => 'input',
								'options' => [
									'nom' => 'toto',
								]
							]
						]
					],
					'etape_3' => [
						'options' => [
							'label' => 'etape 3',
							'afficher_si' => '@toto@ == "hop"',
						],
						'saisies' => []
					],
					'etape_4' => [
						'options' => [
							'label' => 'etape 4',
							'afficher_si' => 'false',
						],
						'saisies' => []
					],
					'etape_5' => [
						'options' => [
							'label' => 'etape 5',
							'afficher_si' => 'true',
						],
						'saisies' => []
					]
				],
				// Etape
				2,
				// Options demandées
				[
					'afficher_si',
					'label'
				]
			],
			'sans_titre' => [
				// Expected
				[
						'etape_3' => [
							'afficher_si' => '@toto@ == "hop"',
						],
						'etape_4' => [
							'afficher_si' => 'false',
						],
						'etape_5' => [
							'afficher_si' => 'true',
						],
				],
				// Etapes
				[
					'etape_1' => [
						'options' => [
							'label' => 'etape 1',
							'afficher_si' => 'false',
						],
						'saisies' => []
					],
					'etape_2' => [
						'options' => [
							'label' => 'etape 2',
						],
						'saisies' => [
							[
								'saisie' => 'input',
								'options' => [
									'nom' => 'toto',
								]
							]
						]
					],
					'etape_3' => [
						'options' => [
							'label' => 'etape 3',
							'afficher_si' => '@toto@ == "hop"',
						],
						'saisies' => []
					],
					'etape_4' => [
						'options' => [
							'label' => 'etape 4',
							'afficher_si' => 'false',
						],
						'saisies' => []
					],
					'etape_5' => [
						'options' => [
							'label' => 'etape 5',
							'afficher_si' => 'true',
						],
						'saisies' => []
					]
				],
				// Etape
				2,
				// Options demandeées
				[
						'afficher_si'
				]
			]
		];
	}
	/**
	 * @dataProvider dataResumerEtapesFutures
	 * @uses saisies_afficher_si_evaluer_plugin()
	 * @uses saisies_afficher_si_secure()
	 * @uses saisies_afficher_si_verifier_syntaxe()
	 * @uses saisies_lister_par_nom()
	 * @uses saisie_nom2name()
	 * @uses saisies_afficher_si_js_defaut()
	 * @uses saisies_evaluer_afficher_si()
	**/
	public function testResumerEtapesFutures(array $expected, array $etapes, int $etape, array $options_demandees): void {
		$actual = saisies_resumer_etapes_futures($etapes, $etape, $options_demandees);
		$this->assertSame($expected, $actual);
	}

	static function dataDeterminerOptionsDemandeesResumerEtapesFutures() {
		return [
			'standard' => [
				// Expected
				['afficher_si'],
				// Provided
				[]
			],
			'etapes_precedent_suivant_titrer_on' =>  [
				// Expected
				['afficher_si', 'label'],
				// Provided
				['etapes_precedent_suivant_titrer' => 'on']
			],
			'etapes_precedent_suivant_titrer_true' =>  [
				// Expected
				['afficher_si', 'label'],
				// Provided
				['etapes_precedent_suivant_titrer' => true]
			],
			'etapes_precedent_suivant_titrer_off' =>  [
				// Expected
				['afficher_si'],
				// Provided
				['etapes_precedent_suivant_titrer' => '']
			],
		];
	}

	/**
	 @dataProvider dataDeterminerOptionsDemandeesResumerEtapesFutures
	*/
	public function testDeterminerOptionsDemandeesResumerEtapesFutures($expected, $provided) {
		$actual = saisies_determiner_options_demandees_resumer_etapes_futures($provided);
		$this->assertEquals($expected, $actual);
	}
}
