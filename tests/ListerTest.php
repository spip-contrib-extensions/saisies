<?php

namespace Spip\Saisies\Tests;


use PHPUnit\Framework\TestCase;
/**
 * @covers saisies_lister_par_identifiant()
 * @covers saisies_lister_par_nom()
 * @covers saisies_lister_finales()
 * @covers saisies_lister_avec_option()
 * @covers saisies_lister_avec_sql()
 * @covers saisies_lister_avec_type()
 * @covers saisies_lister_par_type()
 * @covers saisies_lister_par_etapes()
 * @covers saisies_lister_champs()
 * @covers saisies_lister_labels()
 * @covers saisies_charger_champs()
 * @covers saisies_lister_valeurs_defaut()
 * @covers saisies_comparer_rappel()
 * @covers saisies_comparer_par_identifiant()
 * @covers saisies_comparer()
 * @covers saisies_autonomes()
 * @covers saisies_chercher()
 * @covers saisies_lister_champs_par_section()
 * @covers saisies_dont_avec_option()
 * @uses saisie_nom2name()
 * @uses saisie_editable()
 */

class ListerTest extends TestCase {

	public static function dataListerParIdentifiant() {
		return [
			'avec_conteneur' => [
				// Expected
				[
					'fieldset' => [
						'saisie' => 'fieldset',
						'options' => [
							'nom' => 'fieldset',
							'label' => 'fieldset',
						],
						'identifiant' => 'fieldset',
						'saisies' => [
							[
								'saisie' => 'fieldset',
								'options' => [
									'nom' => 'sfieldset',
									'label' => 'sfieldset',
								],
								'identifiant' => 'sfieldset',
								'saisies' => [
									[
										'saisie' => 'input',
										'options' => [
											'nom' => 'input',
											'label' => 'input',
										],
										'identifiant' => 'input',
									],
									[
										'saisie' => 'input',
										'options' => [
											'nom' => 'input2',
											'label' => 'input2',
										],
										'identifiant' => 'input2',
									]
								]
							]
						]
					],
					'sfieldset' => [
						'saisie' => 'fieldset',
						'options' => [
							'nom' => 'sfieldset',
							'label' => 'sfieldset',
						],
						'identifiant' => 'sfieldset',
						'saisies' => [
							[
								'saisie' => 'input',
								'options' => [
									'nom' => 'input',
									'label' => 'input',
								],
								'identifiant' => 'input',
							],
							[
								'saisie' => 'input',
								'options' => [
									'nom' => 'input2',
									'label' => 'input2',
								],
								'identifiant' => 'input2',
							]
						]
					],
					'input' => [
						'saisie' => 'input',
						'options' => [
							'nom' => 'input',
							'label' => 'input',
						],
						'identifiant' => 'input',
					],
					'input2' => [
						'saisie' => 'input',
						'options' => [
							'nom' => 'input2',
							'label' => 'input2',
						],
						'identifiant' => 'input2',
					],
				],
				// Provided
				[
					[
						'saisie' => 'fieldset',
						'options' => [
							'nom' => 'fieldset',
							'label' => 'fieldset',
						],
						'identifiant' => 'fieldset',
						'saisies' => [
							[
								'saisie' => 'fieldset',
								'options' => [
									'nom' => 'sfieldset',
									'label' => 'sfieldset',
								],
								'identifiant' => 'sfieldset',
								'saisies' => [
									[
										'saisie' => 'input',
										'options' => [
											'nom' => 'input',
											'label' => 'input',
										],
										'identifiant' => 'input',
									],
									[
										'saisie' => 'input',
										'options' => [
											'nom' => 'input2',
											'label' => 'input2',
										],
										'identifiant' => 'input2',
									]
								]
							]
						]
					],
				]
			],
			'sans_conteneur' => [
				// Expected
				[
					'input' => [
						'saisie' => 'input',
						'options' => [
							'nom' => 'input',
							'label' => 'input',
						],
						'identifiant' => 'input',
					],
					'input2' => [
						'saisie' => 'input',
						'options' => [
							'nom' => 'input2',
							'label' => 'input2',
						],
						'identifiant' => 'input2',
					],
				],
				// Provided
				[
					[
						'saisie' => 'fieldset',
						'options' => [
							'nom' => 'fieldset',
							'label' => 'fieldset',
						],
						'identifiant' => 'fieldset',
						'saisies' => [
							[
								'saisie' => 'fieldset',
								'options' => [
									'nom' => 'sfieldset',
									'label' => 'sfieldset',
								],
								'identifiant' => 'sfieldset',
								'saisies' => [
									[
										'saisie' => 'input',
										'options' => [
											'nom' => 'input',
											'label' => 'input',
										],
										'identifiant' => 'input',
									],
									[
										'saisie' => 'input',
										'options' => [
											'nom' => 'input2',
											'label' => 'input2',
										],
										'identifiant' => 'input2',
									]
								]
							]
						]
					],
				],
				false
			]
		];
	}

	/**
	 * @dataProvider dataListerParIdentifiant
	 **/
	public function testListerParIdentifiant($expected, $contenu, $avec_conteneur = true) {
		$actual = saisies_lister_par_identifiant($contenu, $avec_conteneur);

		$this->assertEquals($expected, $actual);
	}

	public static function dataListerParNom() {
		return [
			'avec_conteneur' => [
				// Expected
				[
					'fieldset' => [
						'saisie' => 'fieldset',
						'options' => [
							'nom' => 'fieldset',
							'label' => 'fieldset',
						],
						'saisies' => [
							[
								'saisie' => 'fieldset',
								'options' => [
									'nom' => 'sfieldset',
									'label' => 'sfieldset',
								],
								'saisies' => [
									[
										'saisie' => 'input',
										'options' => [
											'nom' => 'input',
											'label' => 'input',
										],
									],
									[
										'saisie' => 'input',
										'options' => [
											'nom' => 'input2',
											'label' => 'input2',
										],
									]
								]
							]
						]
					],
					'sfieldset' => [
						'saisie' => 'fieldset',
						'options' => [
							'nom' => 'sfieldset',
							'label' => 'sfieldset',
						],
						'saisies' => [
							[
								'saisie' => 'input',
								'options' => [
									'nom' => 'input',
									'label' => 'input',
								],
							],
							[
								'saisie' => 'input',
								'options' => [
									'nom' => 'input2',
									'label' => 'input2',
								],
							]
						]
					],
					'input' => [
						'saisie' => 'input',
						'options' => [
							'nom' => 'input',
							'label' => 'input',
						],
					],
					'input2' => [
						'saisie' => 'input',
						'options' => [
							'nom' => 'input2',
							'label' => 'input2',
						],
					],
				],
				// Provided
				[
					[
						'saisie' => 'fieldset',
						'options' => [
							'nom' => 'fieldset',
							'label' => 'fieldset',
						],
						'saisies' => [
							[
								'saisie' => 'fieldset',
								'options' => [
									'nom' => 'sfieldset',
									'label' => 'sfieldset',
								],
								'saisies' => [
									[
										'saisie' => 'input',
										'options' => [
											'nom' => 'input',
											'label' => 'input',
										],
									],
									[
										'saisie' => 'input',
										'options' => [
											'nom' => 'input2',
											'label' => 'input2',
										],
									]
								]
							]
						]
					],
				]
			],
			'sans_conteneur' => [
				// Expected
				[
					'input' => [
						'saisie' => 'input',
						'options' => [
							'nom' => 'input',
							'label' => 'input',
						],
					],
					'input2' => [
						'saisie' => 'input',
						'options' => [
							'nom' => 'input2',
							'label' => 'input2',
						],
					],
				],
				// Provided
				[
					[
						'saisie' => 'fieldset',
						'options' => [
							'nom' => 'fieldset',
							'label' => 'fieldset',
						],
						'saisies' => [
							[
								'saisie' => 'fieldset',
								'options' => [
									'nom' => 'sfieldset',
									'label' => 'sfieldset',
								],
								'saisies' => [
									[
										'saisie' => 'input',
										'options' => [
											'nom' => 'input',
											'label' => 'input',
										],
									],
									[
										'saisie' => 'input',
										'options' => [
											'nom' => 'input2',
											'label' => 'input2',
										],
									]
								]
							]
						]
					],
				],
				false
			]
		];
	}

	/**
	 * @dataProvider dataListerParNom
	 **/
	public function testListerParNom($expected, $contenu, $avec_conteneur = true) {
		$actual = saisies_lister_par_nom($contenu, $avec_conteneur);

		$this->assertEquals($expected, $actual);
	}


	public static function dataListerFinales() {
		return [
			[
				// Expected
				[
					[
						'saisie' => 'input',
						'options' => [
							'nom' => 'input',
							'label' => 'input',
						],
					],
					[
						'saisie' => 'input',
						'options' => [
							'nom' => 'input2',
							'label' => 'input2',
						],
					],
				],
				// Provided
				[
					[
						'saisie' => 'fieldset',
						'options' => [
							'nom' => 'fieldset',
							'label' => 'fieldset',
						],
						'saisies' => [
							[
								'saisie' => 'fieldset',
								'options' => [
									'nom' => 'sfieldset',
									'label' => 'sfieldset',
								],
								'saisies' => [
									[
										'saisie' => 'input',
										'options' => [
											'nom' => 'input',
											'label' => 'input',
										],
									],
									[
										'saisie' => 'input',
										'options' => [
											'nom' => 'input2',
											'label' => 'input2',
										],
									]
								]
							]
						]
					],
				]
			]
		];
	}

	/**
	 * @dataProvider dataListerFinales
	 **/
	public function testListerFinales($expected, $contenu) {
		$actual = saisies_lister_finales($contenu);
		$this->assertEquals($expected, $actual);
	}


	public static function dataListerAvecOption() {
		return [
			'par_nom' => [
				// Expected
				[
					'input2' => [
						'saisie' => 'input',
						'options' => [
							'nom' => 'input2',
							'label' => 'input2',
							'obligatoire' => 'on',
						],
					],
				],
				// Provided
				'obligatoire',
				[
					[
						'saisie' => 'fieldset',
						'options' => [
							'nom' => 'fieldset',
							'label' => 'fieldset',
						],
						'saisies' => [
							[
								'saisie' => 'fieldset',
								'options' => [
									'nom' => 'sfieldset',
									'label' => 'sfieldset',
								],
								'saisies' => [
									[
										'saisie' => 'input',
										'options' => [
											'nom' => 'input',
											'label' => 'input',
										],
									],
									[
										'saisie' => 'input',
										'options' => [
											'nom' => 'input2',
											'label' => 'input2',
											'obligatoire' => 'on'
										],
									]
								]
							]
						]
					],
				],
				'nom',
			],
			'par_identifiant' => [
				// Expected
				[
					'seul_id' => [
						'saisie' => 'input',
						'options' => [
							'nom' => 'input2',
							'label' => 'input2',
							'obligatoire' => 'on',
						],
						'identifiant' => 'seul_id',
					],
				],
				// Provided
				'obligatoire',
				[
					[
						'saisie' => 'fieldset',
						'options' => [
							'nom' => 'fieldset',
							'label' => 'fieldset',
						],
						'saisies' => [
							[
								'saisie' => 'fieldset',
								'options' => [
									'nom' => 'sfieldset',
									'label' => 'sfieldset',
								],
								'saisies' => [
									[
										'saisie' => 'input',
										'options' => [
											'nom' => 'input',
											'label' => 'input',
										],
									],
									[
										'saisie' => 'input',
										'options' => [
											'nom' => 'input2',
											'label' => 'input2',
											'obligatoire' => 'on'
										],
										'identifiant' => 'seul_id',
									]
								]
							]
						]
					],
				],
				'identifiant'
			]
		];
	}

	/**
	 * @dataProvider dataListerAvecOption
	 **/
	public function testListerAvecOption($expected, $option, $saisies, $tri = 'nom') {
		$actual = saisies_lister_avec_option($option, $saisies, $tri);
		$this->assertEquals($expected, $actual);
	}

	public static function dataListerAvecSql() {
		return [
			'par_nom' => [
				// Expected
				[
					'input2' => [
						'saisie' => 'input',
						'options' => [
							'nom' => 'input2',
							'label' => 'input2',
							'obligatoire' => 'on',
							'sql' => 'TEXT',
						],
					],
				],
				// Provided
				[
					[
						'saisie' => 'fieldset',
						'options' => [
							'nom' => 'fieldset',
							'label' => 'fieldset',
						],
						'saisies' => [
							[
								'saisie' => 'fieldset',
								'options' => [
									'nom' => 'sfieldset',
									'label' => 'sfieldset',
								],
								'saisies' => [
									[
										'saisie' => 'input',
										'options' => [
											'nom' => 'input',
											'label' => 'input',
										],
									],
									[
										'saisie' => 'input',
										'options' => [
											'nom' => 'input2',
											'label' => 'input2',
											'obligatoire' => 'on',
											'sql' => 'TEXT',
										],
									]
								]
							]
						]
					],
				],
				'nom',
			],
			'par_identifiant' => [
				// Expected
				[
					'seul_id' => [
						'saisie' => 'input',
						'options' => [
							'nom' => 'input2',
							'label' => 'input2',
							'obligatoire' => 'on',
							'sql' => 'TEXT',
						],
						'identifiant' => 'seul_id',
					],
				],
				// Provided
				[
					[
						'saisie' => 'fieldset',
						'options' => [
							'nom' => 'fieldset',
							'label' => 'fieldset',
						],
						'saisies' => [
							[
								'saisie' => 'fieldset',
								'options' => [
									'nom' => 'sfieldset',
									'label' => 'sfieldset',
								],
								'saisies' => [
									[
										'saisie' => 'input',
										'options' => [
											'nom' => 'input',
											'label' => 'input',
										],
									],
									[
										'saisie' => 'input',
										'options' => [
											'nom' => 'input2',
											'label' => 'input2',
											'obligatoire' => 'on',
											'sql' => 'TEXT',
										],
										'identifiant' => 'seul_id',
									]
								]
							]
						]
					],
				],
				'identifiant'
			]
		];
	}

	/**
	 * @dataProvider dataListerAvecSql
	 **/
	public function testListerAvecSql($expected, $saisies, $tri = 'nom') {
		$actual = saisies_lister_avec_sql($saisies, $tri);
		$this->assertEquals($expected, $actual);
	}


	public static function dataListerAvecType() {
		return [
			'input_par_nom' => [
				// Expected
				[
					'input' => [
						'saisie' => 'input',
						'options' => [
							'nom' => 'input',
							'label' => 'input',
						],
					],
					'input2' => [
						'saisie' => 'input',
						'options' => [
							'nom' => 'input2',
							'label' => 'input2',
							'obligatoire' => 'on',
						],
					],
				],
				// Provided
				[
					[
						'saisie' => 'fieldset',
						'options' => [
							'nom' => 'fieldset',
							'label' => 'fieldset',
						],
						'saisies' => [
							[
								'saisie' => 'fieldset',
								'options' => [
									'nom' => 'sfieldset',
									'label' => 'sfieldset',
								],
								'saisies' => [
									[
										'saisie' => 'input',
										'options' => [
											'nom' => 'input',
											'label' => 'input',
										],
									],
									[
										'saisie' => 'input',
										'options' => [
											'nom' => 'input2',
											'label' => 'input2',
											'obligatoire' => 'on'
										],
									]
								]
							]
						]
					]
				],
				'input',
				'nom',
			],
			'par_identifiant' => [
				// Expected
				[
					'seul_id' => [
						'saisie' => 'input',
						'options' => [
							'nom' => 'input2',
							'label' => 'input2',
							'obligatoire' => 'on',
						],
						'identifiant' => 'seul_id',
					],
				],
				// Provided
				[
					[
						'saisie' => 'fieldset',
						'options' => [
							'nom' => 'fieldset',
							'label' => 'fieldset',
						],
						'saisies' => [
							[
								'saisie' => 'fieldset',
								'options' => [
									'nom' => 'sfieldset',
									'label' => 'sfieldset',
								],
								'saisies' => [
									[
										'saisie' => 'input',
										'options' => [
											'nom' => 'input',
											'label' => 'input',
										],
									],
									[
										'saisie' => 'input',
										'options' => [
											'nom' => 'input2',
											'label' => 'input2',
											'obligatoire' => 'on'
										],
										'identifiant' => 'seul_id',
									]
								]
							]
						]
					],
				],
				'input',
				'identifiant'
			],
			'input_fieldset_sans_conteneur' => [
				// Expected
				[
					'fieldset' => [
						'saisie' => 'fieldset',
						'options' => [
							'nom' => 'fieldset',
							'label' => 'fieldset',
						],
						'saisies' => [
							[
								'saisie' => 'fieldset',
								'options' => [
									'nom' => 'sfieldset',
									'label' => 'sfieldset',
								],
								'saisies' => [
									[
										'saisie' => 'input',
										'options' => [
											'nom' => 'input',
											'label' => 'input',
										],
									],
									[
										'saisie' => 'input',
										'options' => [
											'nom' => 'input2',
											'label' => 'input2',
											'obligatoire' => 'on'
										],
									],
									[
										'saisie' => 'radio',
										'options' => [
											'nom' => 'radio',
											'label' => 'radio',
											'obligatoire' => 'on'
										],
									]
								]
							]
						]
					],
					'sfieldset' => [
						'saisie' => 'fieldset',
						'options' => [
							'nom' => 'sfieldset',
							'label' => 'sfieldset',
						],
						'saisies' => [
							[
								'saisie' => 'input',
								'options' => [
									'nom' => 'input',
									'label' => 'input',
								],
							],
							[
								'saisie' => 'input',
								'options' => [
									'nom' => 'input2',
									'label' => 'input2',
									'obligatoire' => 'on'
								],
							],
							[
								'saisie' => 'radio',
								'options' => [
									'nom' => 'radio',
									'label' => 'radio',
									'obligatoire' => 'on'
								],
							]
						]
					]
					,
					'input' => [
						'saisie' => 'input',
						'options' => [
							'nom' => 'input',
							'label' => 'input',
						],
					],
					'input2' => [
						'saisie' => 'input',
						'options' => [
							'nom' => 'input2',
							'label' => 'input2',
							'obligatoire' => 'on',
						],
					],
				],
				// Provided
				[
					[
						'saisie' => 'fieldset',
						'options' => [
							'nom' => 'fieldset',
							'label' => 'fieldset',
						],
						'saisies' => [
							[
								'saisie' => 'fieldset',
								'options' => [
									'nom' => 'sfieldset',
									'label' => 'sfieldset',
								],
								'saisies' => [
									[
										'saisie' => 'input',
										'options' => [
											'nom' => 'input',
											'label' => 'input',
										],
									],
									[
										'saisie' => 'input',
										'options' => [
											'nom' => 'input2',
											'label' => 'input2',
											'obligatoire' => 'on'
										],
									],
									[
										'saisie' => 'radio',
										'options' => [
											'nom' => 'radio',
											'label' => 'radio',
											'obligatoire' => 'on'
										],
									]
								]
							]
						]
					]
				],
				['fieldset', 'input'],
				'nom',
				false,
			],
			'input_fieldset_avec_conteneur' => [
				// Expected
				[
					[
						'saisie' => 'fieldset',
						'options' => [
							'nom' => 'fieldset',
							'label' => 'fieldset',
						],
						'saisies' => [
							[
								'saisie' => 'fieldset',
								'options' => [
									'nom' => 'sfieldset',
									'label' => 'sfieldset',
								],
								'saisies' => [
									[
										'saisie' => 'input',
										'options' => [
											'nom' => 'input',
											'label' => 'input',
										],
									],
									[
										'saisie' => 'input',
										'options' => [
											'nom' => 'input2',
											'label' => 'input2',
											'obligatoire' => 'on'
										],
									],
								]
							]
						]
					],
				],
				// Provided
				[
					[
						'saisie' => 'fieldset',
						'options' => [
							'nom' => 'fieldset',
							'label' => 'fieldset',
						],
						'saisies' => [
							[
								'saisie' => 'fieldset',
								'options' => [
									'nom' => 'sfieldset',
									'label' => 'sfieldset',
								],
								'saisies' => [
									[
										'saisie' => 'input',
										'options' => [
											'nom' => 'input',
											'label' => 'input',
										],
									],
									[
										'saisie' => 'input',
										'options' => [
											'nom' => 'input2',
											'label' => 'input2',
											'obligatoire' => 'on'
										],
									],
									[
										'saisie' => 'radio',
										'options' => [
											'nom' => 'radio',
											'label' => 'radio',
											'obligatoire' => 'on'
										],
									]
								]
							]
						]
					]
				],
				['fieldset', 'input'],
				'nom',
				true,
			]
		];
	}

	/**
	 * @dataProvider dataListerAvecType
	 */
	public function testListerAvecType($expected, $saisies, $type, $tri = 'nom', $avec_conteneur = false) {
		$actual = saisies_lister_avec_type($saisies, $type, $tri, $avec_conteneur);
		$this->assertEquals($expected, $actual);
	}

	public static function dataListerParType() {
		return [
			[
				// Expected
				[
					'input' => [
						'input' => [
							'saisie' => 'input',
							'options' => [
								'nom' => 'input',
								'label' => 'input',
							],
						],
						'input2' => [
							'saisie' => 'input',
							'options' => [
								'nom' => 'input2',
								'label' => 'input2',
								'obligatoire' => 'on',
							],
						],
					],
					'radio' => [
						'radio2' => [
							'saisie' => 'radio',
							'options' => [
								'nom' => 'radio2',
								'label' => 'radio2',
								'obligatoire' => 'on'
							],
						]
					]
				],
				// Provided
				[
					[
						'saisie' => 'fieldset',
						'options' => [
							'nom' => 'fieldset',
							'label' => 'fieldset',
						],
						'saisies' => [
							[
								'saisie' => 'fieldset',
								'options' => [
									'nom' => 'sfieldset',
									'label' => 'sfieldset',
								],
								'saisies' => [
									[
										'saisie' => 'input',
										'options' => [
											'nom' => 'input',
											'label' => 'input',
										],
									],
									[
										'saisie' => 'input',
										'options' => [
											'nom' => 'input2',
											'label' => 'input2',
											'obligatoire' => 'on'
										],
									],
									[
										'saisie' => 'radio',
										'options' => [
											'nom' => 'radio2',
											'label' => 'radio2',
											'obligatoire' => 'on'
										],
									]
								]
							]
						]
					]
				],
			]
		];
	}

	/**
	 * @dataProvider dataListerParType
	 */
	public function testListerParType($expected, $saisies) {
		$actual = saisies_lister_par_type($saisies);
		$this->assertEquals($expected, $actual);
	}

	public static function dataListerParEtapes() {
		$retour = [

			'pas_assez_fieldset' => [
				// Expected
				false,
				// Provided
				[
					'options' => ['etapes_activer' => true],
					[
						'saisie' => 'fieldset',
						'options' => [
							'nom' => 'fieldset',
							'label' => 'fieldset',
						],
						'saisies' => [
							[
								'saisie' => 'fieldset',
								'options' => [
									'nom' => 'sfieldset',
									'label' => 'sfieldset',
								],
								'saisies' => [
									[
										'saisie' => 'input',
										'options' => [
											'nom' => 'input',
											'label' => 'input',
										],
									],
									[
										'saisie' => 'input',
										'options' => [
											'nom' => 'input2',
											'label' => 'input2',
											'obligatoire' => 'on'
										],
									],
									[
										'saisie' => 'radio',
										'options' => [
											'nom' => 'radio2',
											'label' => 'radio2',
											'obligatoire' => 'on'
										],
									]
								]
							]
						]
					]
				],
			],
			'deux_etapes' => [
				// Expected
				[
					'etape_1' => [
						'saisie' => 'fieldset',
						'options' => [
							'nom' => 'fieldset1',
							'label' => 'fieldset1',
						],
						'saisies' => [
							[
								'saisie' => 'input',
								'options' => [
									'nom' => 'input',
									'label' => 'input',
								],
							],
							[
								'saisie' => 'input',
								'options' => [
									'nom' => 'input2',
									'label' => 'input2',
									'obligatoire' => 'on'
								],
							],
							[
								'saisie' => 'radio',
								'options' => [
									'nom' => 'radio2',
									'label' => 'radio2',
									'obligatoire' => 'on'
								],
							]
						]
					],
					'etape_2' => [
						'saisie' => 'fieldset',
						'options' => [
							'nom' => 'fieldset2',
							'label' => 'fieldset2',
						],
						'saisies' => [
							[
								'saisie' => 'input',
								'options' => [
									'nom' => 'input3',
									'label' => 'input3',
								],
							],
							[
								'saisie' => 'input',
								'options' => [
									'nom' => 'input4',
									'label' => 'input4',
									'obligatoire' => 'on'
								],
							],
						]
					],
					'etape_3' => [
						'saisie' => 'fieldset',
						'options' => [
							'nom' => '@saisies_recapitulatif',
							'label' => _T('saisies:etapes_recapitulatif_label')
						],
						'saisies' => [],
						'valeurs' => $_POST,
					]
				],
				// Provided
				[
					'options' => ['etapes_activer' => true],
					[
						'saisie' => 'fieldset',
						'options' => [
							'nom' => 'fieldset1',
							'label' => 'fieldset1',
						],
						'saisies' => [
							[
								'saisie' => 'input',
								'options' => [
									'nom' => 'input',
									'label' => 'input',
								],
							],
							[
								'saisie' => 'input',
								'options' => [
									'nom' => 'input2',
									'label' => 'input2',
									'obligatoire' => 'on'
								],
							],
							[
								'saisie' => 'radio',
								'options' => [
									'nom' => 'radio2',
									'label' => 'radio2',
									'obligatoire' => 'on'
								],
							]
						]
					],
					[
						'saisie' => 'fieldset',
						'options' => [
							'nom' => 'fieldset2',
							'label' => 'fieldset2',
						],
						'saisies' => [
							[
								'saisie' => 'input',
								'options' => [
									'nom' => 'input3',
									'label' => 'input3',
								],
							],
							[
								'saisie' => 'input',
								'options' => [
									'nom' => 'input4',
									'label' => 'input4',
									'obligatoire' => 'on'
								],
							],
						]
					]
				]
			],
			'deux_etapes_sans_recap' => [
				// Expected
				[
					'etape_1' => [
						'saisie' => 'fieldset',
						'options' => [
							'nom' => 'fieldset1',
							'label' => 'fieldset1',
						],
						'saisies' => [
							[
								'saisie' => 'input',
								'options' => [
									'nom' => 'input',
									'label' => 'input',
								],
							],
							[
								'saisie' => 'input',
								'options' => [
									'nom' => 'input2',
									'label' => 'input2',
									'obligatoire' => 'on'
								],
							],
							[
								'saisie' => 'radio',
								'options' => [
									'nom' => 'radio2',
									'label' => 'radio2',
									'obligatoire' => 'on'
								],
							]
						]
					],
					'etape_2' => [
						'saisie' => 'fieldset',
						'options' => [
							'nom' => 'fieldset2',
							'label' => 'fieldset2',
						],
						'saisies' => [
							[
								'saisie' => 'input',
								'options' => [
									'nom' => 'input3',
									'label' => 'input3',
								],
							],
							[
								'saisie' => 'input',
								'options' => [
									'nom' => 'input4',
									'label' => 'input4',
									'obligatoire' => 'on'
								],
							],
						]
					],
				],
				// Provided
				[
					'options' => [
						'etapes_activer' => true,
						'etapes_ignorer_recapitulatif' => true
					],
					[
						'saisie' => 'fieldset',
						'options' => [
							'nom' => 'fieldset1',
							'label' => 'fieldset1',
						],
						'saisies' => [
							[
								'saisie' => 'input',
								'options' => [
									'nom' => 'input',
									'label' => 'input',
								],
							],
							[
								'saisie' => 'input',
								'options' => [
									'nom' => 'input2',
									'label' => 'input2',
									'obligatoire' => 'on'
								],
							],
							[
								'saisie' => 'radio',
								'options' => [
									'nom' => 'radio2',
									'label' => 'radio2',
									'obligatoire' => 'on'
								],
							]
						]
					],
					[
						'saisie' => 'fieldset',
						'options' => [
							'nom' => 'fieldset2',
							'label' => 'fieldset2',
						],
						'saisies' => [
							[
								'saisie' => 'input',
								'options' => [
									'nom' => 'input3',
									'label' => 'input3',
								],
							],
							[
								'saisie' => 'input',
								'options' => [
									'nom' => 'input4',
									'label' => 'input4',
									'obligatoire' => 'on'
								],
							],
						]
					]
				]
			],
			'deux_etapes_sans_recap+hors_etape' => [
				// Expected
				[
					'etape_1' => [
						'saisie' => 'fieldset',
						'options' => [
							'nom' => 'fieldset1',
							'label' => 'fieldset1',
						],
						'saisies' => [
							[
								'saisie' => 'input',
								'options' => [
									'nom' => 'horsetape_debut',
									'label' => 'horsetape_debut',
								],
							],
							[
								'saisie' => 'input',
								'options' => [
									'nom' => 'input',
									'label' => 'input',
								],
							],
							[
								'saisie' => 'input',
								'options' => [
									'nom' => 'input2',
									'label' => 'input2',
									'obligatoire' => 'on'
								],
							],
							[
								'saisie' => 'radio',
								'options' => [
									'nom' => 'radio2',
									'label' => 'radio2',
									'obligatoire' => 'on'
								],
							],
							[
								'saisie' => 'input',
								'options' => [
									'nom' => 'horsetape',
									'label' => 'horsetape',
								],
							]
						]
					],
					'etape_2' => [
						'saisie' => 'fieldset',
						'options' => [
							'nom' => 'fieldset2',
							'label' => 'fieldset2',
						],
						'saisies' => [
							[
								'saisie' => 'input',
								'options' => [
									'nom' => 'horsetape_debut',
									'label' => 'horsetape_debut',
								],
							],
							[
								'saisie' => 'input',
								'options' => [
									'nom' => 'input3',
									'label' => 'input3',
								],
							],
							[
								'saisie' => 'input',
								'options' => [
									'nom' => 'input4',
									'label' => 'input4',
									'obligatoire' => 'on'
								],
							],
							[
								'saisie' => 'input',
								'options' => [
									'nom' => 'horsetape',
									'label' => 'horsetape',
								],
							]
						]
					],
				],
				// Provided
				[
					'options' => [
						'etapes_activer' => true,
						'etapes_ignorer_recapitulatif' => true
					],
					[
						'saisie' => 'input',
						'options' => [
							'nom' => 'horsetape_debut',
							'label' => 'horsetape_debut',
						],
					],
					[
						'saisie' => 'fieldset',
						'options' => [
							'nom' => 'fieldset1',
							'label' => 'fieldset1',
						],
						'saisies' => [
							[
								'saisie' => 'input',
								'options' => [
									'nom' => 'input',
									'label' => 'input',
								],
							],
							[
								'saisie' => 'input',
								'options' => [
									'nom' => 'input2',
									'label' => 'input2',
									'obligatoire' => 'on'
								],
							],
							[
								'saisie' => 'radio',
								'options' => [
									'nom' => 'radio2',
									'label' => 'radio2',
									'obligatoire' => 'on'
								],
							]
						]
					],
					[
						'saisie' => 'fieldset',
						'options' => [
							'nom' => 'fieldset2',
							'label' => 'fieldset2',
						],
						'saisies' => [
							[
								'saisie' => 'input',
								'options' => [
									'nom' => 'input3',
									'label' => 'input3',
								],
							],
							[
								'saisie' => 'input',
								'options' => [
									'nom' => 'input4',
									'label' => 'input4',
									'obligatoire' => 'on'
								],
							],
						]
					],
					[
						'saisie' => 'input',
						'options' => [
							'nom' => 'horsetape',
							'label' => 'horsetape',
						],
					]
				]
			],
			'check_only_false' => [
				// Expected
				false,
				// Provided
				[
					'options' => [
						'etapes_activer' => true,
						'etapes_ignorer_recapitulatif' => true
					],
					[
						'saisie' => 'fieldset',
						'options' => [
							'nom' => 'fieldset2',
							'label' => 'fieldset2',
						],
						'saisies' => [
							[
								'saisie' => 'input',
								'options' => [
									'nom' => 'input3',
									'label' => 'input3',
								],
							],
							[
								'saisie' => 'input',
								'options' => [
									'nom' => 'input4',
									'label' => 'input4',
									'obligatoire' => 'on'
								],
							],
						]
					]
				],
				true
			],
			'check_only_true' => [
				// Expected
				true,
				// Provided
				[
					'options' => [
						'etapes_activer' => true,
						'etapes_ignorer_recapitulatif' => true
					],
					[
						'saisie' => 'fieldset',
						'options' => [
							'nom' => 'fieldset1',
							'label' => 'fieldset1',
						],
						'saisies' => [
							[
								'saisie' => 'input',
								'options' => [
									'nom' => 'input',
									'label' => 'input',
								],
							],
							[
								'saisie' => 'input',
								'options' => [
									'nom' => 'input2',
									'label' => 'input2',
									'obligatoire' => 'on'
								],
							],
							[
								'saisie' => 'radio',
								'options' => [
									'nom' => 'radio2',
									'label' => 'radio2',
									'obligatoire' => 'on'
								],
							]
						]
					],
					[
						'saisie' => 'fieldset',
						'options' => [
							'nom' => 'fieldset2',
							'label' => 'fieldset2',
						],
						'saisies' => [
							[
								'saisie' => 'input',
								'options' => [
									'nom' => 'input3',
									'label' => 'input3',
								],
							],
							[
								'saisie' => 'input',
								'options' => [
									'nom' => 'input4',
									'label' => 'input4',
									'obligatoire' => 'on'
								],
							],
						]
					]
				],
				true
			],
			'deux_etapes_dont_1_depubliee' => [
				// Expected
				[
					'etape_1' => [
						'saisie' => 'fieldset',
						'options' => [
							'nom' => 'fieldset2',
							'label' => 'fieldset2',
						],
						'saisies' => [
							[
								'saisie' => 'input',
								'options' => [
									'nom' => 'input3',
									'label' => 'input3',
								],
							],
							[
								'saisie' => 'input',
								'options' => [
									'nom' => 'input4',
									'label' => 'input4',
									'obligatoire' => 'on'
								],
							],
						]
					],
				],
				// Provided
				[
					'options' => [
						'etapes_activer' => true,
						'etapes_ignorer_recapitulatif' => true
					],
					[
						'saisie' => 'fieldset',
						'options' => [
							'nom' => 'fieldset1',
							'label' => 'fieldset1',
							'depublie' => 'on',
						],
						'saisies' => [
							[
								'saisie' => 'input',
								'options' => [
									'nom' => 'input',
									'label' => 'input',
								],
							],
							[
								'saisie' => 'input',
								'options' => [
									'nom' => 'input2',
									'label' => 'input2',
									'obligatoire' => 'on'
								],
							],
							[
								'saisie' => 'radio',
								'options' => [
									'nom' => 'radio2',
									'label' => 'radio2',
									'obligatoire' => 'on'
								],
							]
						]
					],
					[
						'saisie' => 'fieldset',
						'options' => [
							'nom' => 'fieldset2',
							'label' => 'fieldset2',
						],
						'saisies' => [
							[
								'saisie' => 'input',
								'options' => [
									'nom' => 'input3',
									'label' => 'input3',
								],
							],
							[
								'saisie' => 'input',
								'options' => [
									'nom' => 'input4',
									'label' => 'input4',
									'obligatoire' => 'on'
								],
							],
						]
					]
				]
			],
			'deux_etapes_dont_1_faussement_depubliee' => [
				// Expected
				[
					'etape_1' => [
						'saisie' => 'fieldset',
						'options' => [
							'nom' => 'fieldset1',
							'label' => 'fieldset1',
							'depublie' => 'on',
						],
						'saisies' => [
							[
								'saisie' => 'input',
								'options' => [
									'nom' => 'input',
									'label' => 'input',
								],
							],
							[
								'saisie' => 'input',
								'options' => [
									'nom' => 'input2',
									'label' => 'input2',
									'obligatoire' => 'on'
								],
							],
							[
								'saisie' => 'radio',
								'options' => [
									'nom' => 'radio2',
									'label' => 'radio2',
									'obligatoire' => 'on'
								],
							]
						]
					],
					'etape_2' => [
						'saisie' => 'fieldset',
						'options' => [
							'nom' => 'fieldset2',
							'label' => 'fieldset2',
						],
						'saisies' => [
							[
								'saisie' => 'input',
								'options' => [
									'nom' => 'input3',
									'label' => 'input3',
								],
							],
							[
								'saisie' => 'input',
								'options' => [
									'nom' => 'input4',
									'label' => 'input4',
									'obligatoire' => 'on'
								],
							],
						]
					],
				],
				// Provided
				[
					'options' => [
						'etapes_activer' => true,
						'etapes_ignorer_recapitulatif' => true
					],
					[
						'saisie' => 'fieldset',
						'options' => [
							'nom' => 'fieldset1',
							'label' => 'fieldset1',
							'depublie' => 'on',
						],
						'saisies' => [
							[
								'saisie' => 'input',
								'options' => [
									'nom' => 'input',
									'label' => 'input',
								],
							],
							[
								'saisie' => 'input',
								'options' => [
									'nom' => 'input2',
									'label' => 'input2',
									'obligatoire' => 'on'
								],
							],
							[
								'saisie' => 'radio',
								'options' => [
									'nom' => 'radio2',
									'label' => 'radio2',
									'obligatoire' => 'on'
								],
							]
						]
					],
					[
						'saisie' => 'fieldset',
						'options' => [
							'nom' => 'fieldset2',
							'label' => 'fieldset2',
						],
						'saisies' => [
							[
								'saisie' => 'input',
								'options' => [
									'nom' => 'input3',
									'label' => 'input3',
								],
							],
							[
								'saisie' => 'input',
								'options' => [
									'nom' => 'input4',
									'label' => 'input4',
									'obligatoire' => 'on'
								],
							],
						]
					]
				],
				false,
				['input' => 'toto']
			],
			'previsualisation_mode' => [
				// Expected
				[
					'etape_1' => [
						'saisie' => 'fieldset',
						'options' => [
							'nom' => '@saisies_remplissage',
							'label' => '<:saisies:etapes_remplissage_label:>',
						],
						'saisies' => [
							[
								'saisie' => 'input',
								'options' => [
									'nom' => 'input',
									'label' => 'input',
								],
							],
							[
								'saisie' => 'input',
								'options' => [
									'nom' => 'input2',
									'label' => 'input2',
									'obligatoire' => 'on'
								],
							],
							[
								'saisie' => 'radio',
								'options' => [
									'nom' => 'radio2',
									'label' => 'radio2',
									'obligatoire' => 'on'
								],
							]
						]
					],
					'etape_2' => [
						'saisie' => 'fieldset',
						'options' => [
							'nom' => '@saisies_recapitulatif',
							'label' => _T('saisies:etapes_recapitulatif_label')
						],
						'saisies' => [],
						'valeurs' => $_POST,
					],
				],
				// Provided
				[
					'options' => [
						'previsualisation_mode' => 'etape',
					],
					[
						'saisie' => 'input',
						'options' => [
							'nom' => 'input',
							'label' => 'input',
						],
					],
					[
						'saisie' => 'input',
						'options' => [
							'nom' => 'input2',
							'label' => 'input2',
							'obligatoire' => 'on'
						],
					],
					[
						'saisie' => 'radio',
						'options' => [
							'nom' => 'radio2',
							'label' => 'radio2',
							'obligatoire' => 'on'
						],
					]
				],
			]
		];
		return $retour;
	}
	/**
	 * @dataProvider dataListerParEtapes
	 * @uses saisies_wrapper_fieldset()
	 */
	public function testListerParEtapes($expected, $saisies, $check_only = false, $env = []) {
		$actual = saisies_lister_par_etapes($saisies, $check_only, $env);
		$this->assertEquals($expected, $actual);
	}


	public static function dataListerChamps() {
		return [
			'avec_conteneur' => [
				// Expected
				[
					'fieldset' ,
					'sfieldset',
					'input',
					'input2',
				],
				// Provided
				[
					[
						'saisie' => 'fieldset',
						'options' => [
							'nom' => 'fieldset',
							'label' => 'fieldset',
						],
						'saisies' => [
							[
								'saisie' => 'fieldset',
								'options' => [
									'nom' => 'sfieldset',
									'label' => 'sfieldset',
								],
								'saisies' => [
									[
										'saisie' => 'input',
										'options' => [
											'nom' => 'input',
											'label' => 'input',
										],
									],
									[
										'saisie' => 'input',
										'options' => [
											'nom' => 'input2',
											'label' => 'input2',
										],
									]
								]
							]
						]
					],
				]
			],
			'sans_conteneur' => [
				// Expected
				[
					'input',
					'input2'
				],
				// Provided
				[
					[
						'saisie' => 'fieldset',
						'options' => [
							'nom' => 'fieldset',
							'label' => 'fieldset',
						],
						'saisies' => [
							[
								'saisie' => 'fieldset',
								'options' => [
									'nom' => 'sfieldset',
									'label' => 'sfieldset',
								],
								'saisies' => [
									[
										'saisie' => 'input',
										'options' => [
											'nom' => 'input',
											'label' => 'input',
										],
									],
									[
										'saisie' => 'input',
										'options' => [
											'nom' => 'input2',
											'label' => 'input2',
										],
									]
								]
							]
						]
					],
				],
				false
			]
		];
	}

	/**
	 * @dataProvider dataListerChamps
	 **/
	public function testListerChamps($expected, $contenu, $avec_conteneur = true) {
		$actual = saisies_lister_champs($contenu, $avec_conteneur);

		$this->assertEquals($expected, $actual);
	}


	public static function dataListerLabels() {
		return [
			'avec_conteneur' => [
				// Expected
				[
					'fieldset' => 'fieldset_label',
					'sfieldset' => 'sfieldset_label',
					'input' => 'input_label',
					'input2' => 'input2_label',
				],
				// Provided
				[
					[
						'saisie' => 'fieldset',
						'options' => [
							'nom' => 'fieldset',
							'label' => 'fieldset_label',
						],
						'saisies' => [
							[
								'saisie' => 'fieldset',
								'options' => [
									'nom' => 'sfieldset',
									'label' => 'sfieldset_label',
								],
								'saisies' => [
									[
										'saisie' => 'input',
										'options' => [
											'nom' => 'input',
											'label' => 'input_label',
										],
									],
									[
										'saisie' => 'input',
										'options' => [
											'nom' => 'input2',
											'label' => 'input2_label',
										],
									]
								]
							]
						]
					],
				]
			],
			'sans_conteneur' => [
				// Expected
				[
					'input' => 'input_label',
					'input2' => 'input2_label'
				],
				// Provided
				[
					[
						'saisie' => 'fieldset',
						'options' => [
							'nom' => 'fieldset',
							'label' => 'fieldset',
						],
						'saisies' => [
							[
								'saisie' => 'fieldset',
								'options' => [
									'nom' => 'sfieldset',
									'label' => 'sfieldset',
								],
								'saisies' => [
									[
										'saisie' => 'input',
										'options' => [
											'nom' => 'input',
											'label' => 'input_label',
										],
									],
									[
										'saisie' => 'input',
										'options' => [
											'nom' => 'input2',
											'label' => 'input2_label',
										],
									]
								]
							]
						]
					],
				],
				false
			]
		];
	}

	/**
	 * @dataProvider dataListerLabels
	 **/
	public function testListerLabels($expected, $contenu, $avec_conteneur = true) {
		$actual = saisies_lister_labels($contenu, $avec_conteneur);

		$this->assertEquals($expected, $actual);
	}


	public static function dataChargerChamps() {
		return [
			'sans_conteneur' => [
				// Expected
				[
					'input' => null,
					'input2' => null,
				],
				// Provided
				[
					[
						'saisie' => 'fieldset',
						'options' => [
							'nom' => 'fieldset',
							'label' => 'fieldset',
						],
						'saisies' => [
							[
								'saisie' => 'fieldset',
								'options' => [
									'nom' => 'sfieldset',
									'label' => 'sfieldset',
								],
								'saisies' => [
									[
										'saisie' => 'input',
										'options' => [
											'nom' => 'input',
											'label' => 'input',
										],
									],
									[
										'saisie' => 'input',
										'options' => [
											'nom' => 'input2',
											'label' => 'input2',
										],
									]
								]
							]
						]
					],
				]
			]
		];
	}

	/**
	 * @dataProvider dataChargerChamps
	 **/
	public function testChargerChamps($expected, $contenu) {
		$actual = saisies_charger_champs($contenu);

		$this->assertEquals($expected, $actual);
	}


	public static function dataListerValeursDefaut() {
		return [
			'sans_conteneur' => [
				// Expected
				[
					'input' => 'input_defaut',
					'input2' => 'input2_defaut',
					'truc' => [],
				],
				// Provided
				[
					[
						'saisie' => 'fieldset',
						'options' => [
							'nom' => 'fieldset',
							'label' => 'fieldset',
						],
						'saisies' => [
							[
								'saisie' => 'fieldset',
								'options' => [
									'nom' => 'sfieldset',
									'label' => 'sfieldset',
								],
								'saisies' => [
									[
										'saisie' => 'input',
										'options' => [
											'nom' => 'input',
											'label' => 'input',
											'defaut' => 'input_defaut',
										],
									],
									[
										'saisie' => 'input',
										'options' => [
											'nom' => 'truc[muche]',
											'label' => 'input',
											'defaut' => 'input_defaut',
										],
									],
									[
										'saisie' => 'input',
										'options' => [
											'nom' => 'input2',
											'label' => 'input2',
											'defaut' => 'input2_defaut',
										],
									]
								]
							]
						]
					],
				]
			],
			'depuis_session' => [
				// Expected
				[
					'input_depuis_session' => 'email@domaine',
					'input_pas_depuis_session' => 'input_defaut',
				],
				// Provided
				[
					[
						'saisie' => 'input',
						'options' => [
							'nom' => 'input_depuis_session',
							'label' => 'input depuis_session',
							'defaut' => 'input_defaut',
							'defaut_session' => 'email'
						],
					],
					[
						'saisie' => 'input',
						'options' => [
							'nom' => 'input_pas_depuis_session',
							'label' => 'input pas depuis_session',
							'defaut' => 'input_defaut',
							'defaut_session' => 'pasemail'
						],
					],
				]
			]
		];
	}

	/**
	 * @dataProvider dataListerValeursDefaut
	 **/
	public function testListerValeursDefaut($expected, $contenu) {
		$actual = saisies_lister_valeurs_defaut($contenu);

		$this->assertEquals($expected, $actual);
	}

	public static function dataComparer() {
		return [
			'avec_conteneur' => [
				// Expected
				[
					'ajoutees' => [
						'nouvelle' => [
							'saisie' => 'input',
							'options' => [
								'nom' => 'nouvelle',
							]
						]
					],
					'supprimees' => [
						'supprimee' => [
							'saisie' => 'input',
							'options' => [
								'nom' => 'supprimee',
							]
						]
					],
					'modifiees' => [
						'modifiees' => [
							'saisie' => 'input',
							'options' => [
								'nom' => 'modifiees',
								'option' => 'hop',
							]
						]
					],
					'identiques' => [
						'identique' => [
							'saisie' => 'input',
							'options' => [
								'nom' => 'identique',
							]
						],
						'identique_fieldset' => [
							'saisie' => 'fieldset',
							'options' => [
								'nom' => 'identique_fieldset',
							],
							'saisies' => [
								[
									[
										'saisie' => 'input',
										'options' => [
											'nom' => 'label'
										]
									]
								]
							],
						]
					]
				],
				// Anciennes
				[
					[
						'saisie' => 'input',
						'options' => [
							'nom' => 'supprimee',
						]
					],
					[
						'saisie' => 'input',
						'options' => [
							'nom' => 'modifiees',
						]
					],
					[
						'saisie' => 'input',
						'options' => [
							'nom' => 'identique',
						]
					],
					[
					'saisie' => 'fieldset',
							'options' => [
								'nom' => 'identique_fieldset',
							],
							'saisies' => [
								[
									[
										'saisie' => 'input',
										'options' => [
											'nom' => 'label'
										]
									]
								]
							],
						]
				],
				// Nouvelle
				[
					[
						'saisie' => 'input',
						'options' => [
							'nom' => 'nouvelle',
						]
					],
					[
						'saisie' => 'input',
						'options' => [
							'nom' => 'modifiees',
							'option' => 'hop',
						]
					],
					[
						'saisie' => 'input',
						'options' => [
							'nom' => 'identique',
						]
					],
					[
					'saisie' => 'fieldset',
							'options' => [
								'nom' => 'identique_fieldset',
							],
							'saisies' => [
								[
									[
										'saisie' => 'input',
										'options' => [
											'nom' => 'label'
										]
									]
								]
							],
					]
				]
			],
			'sans_conteneur' => [
				// Expected
				[
					'ajoutees' => [
						'nouvelle' => [
							'saisie' => 'input',
							'options' => [
								'nom' => 'nouvelle',
							]
						]
					],
					'supprimees' => [
						'supprimee' => [
							'saisie' => 'input',
							'options' => [
								'nom' => 'supprimee',
							]
						]
					],
					'modifiees' => [
						'modifiees' => [
							'saisie' => 'input',
							'options' => [
								'nom' => 'modifiees',
								'option' => 'hop',
							]
						]
					],
					'identiques' => [
						'identique' => [
							'saisie' => 'input',
							'options' => [
								'nom' => 'identique',
							]
						],
					]
				],
				// Anciennes
				[
					[
						'saisie' => 'input',
						'options' => [
							'nom' => 'supprimee',
						]
					],
					[
						'saisie' => 'input',
						'options' => [
							'nom' => 'modifiees',
						]
					],
					[
						'saisie' => 'input',
						'options' => [
							'nom' => 'identique',
						]
					],
					[
					'saisie' => 'fieldset',
							'options' => [
								'nom' => 'identique_fieldset',
							],
							'saisies' => [
								[
									[
										'saisie' => 'input',
										'options' => [
											'nom' => 'label'
										]
									]
								]
							],
						]
				],
				// Nouvelle
				[
					[
						'saisie' => 'input',
						'options' => [
							'nom' => 'nouvelle',
						]
					],
					[
						'saisie' => 'input',
						'options' => [
							'nom' => 'modifiees',
							'option' => 'hop',
						]
					],
					[
						'saisie' => 'input',
						'options' => [
							'nom' => 'identique',
						]
					],
					[
					'saisie' => 'fieldset',
							'options' => [
								'nom' => 'identique_fieldset',
							],
							'saisies' => [
								[
									[
										'saisie' => 'input',
										'options' => [
											'nom' => 'label'
										]
									]
								]
							],
					]
				],
				// Avec conteneur
				false
			],
			'sans_conteneur_id' => [
				// Expected
				[
					'ajoutees' => [
						'@n' => [
							'saisie' => 'input',
							'options' => [
								'nom' => 'nouvelle',
							],
							'identifiant' => '@n',
						]
					],
					'supprimees' => [
						'@s' => [
							'saisie' => 'input',
							'options' => [
								'nom' => 'supprimee',
							],
							'identifiant' => '@s',
						]
					],
					'modifiees' => [
						'@m' => [
							'saisie' => 'input',
							'options' => [
								'nom' => 'modifiees',
								'option' => 'hop',
							],
							'identifiant' => '@m',
						]
					],
					'identiques' => [
						'@i' => [
							'saisie' => 'input',
							'options' => [
								'nom' => 'identique',
							],
							'identifiant' => '@i',
						],
					]
				],
				// Anciennes
				[
					[
						'saisie' => 'input',
						'options' => [
							'nom' => 'supprimee',
						],
						'identifiant' => '@s'
					],
					[
						'saisie' => 'input',
						'options' => [
							'nom' => 'modifiees',
						],
						'identifiant' => '@m'
					],
					[
						'saisie' => 'input',
						'options' => [
							'nom' => 'identique',
						],
						'identifiant' => '@i'
					],
					[
					'saisie' => 'fieldset',
							'options' => [
								'nom' => 'identique_fieldset',
							],
							'saisies' => [
								[
									[
										'saisie' => 'input',
										'options' => [
											'nom' => 'label'
										]
									]
								]
							],
						]
				],
				// Nouvelle
				[
					[
						'saisie' => 'input',
						'options' => [
							'nom' => 'nouvelle',
						],
						'identifiant' => '@n'
					],
					[
						'saisie' => 'input',
						'options' => [
							'nom' => 'modifiees',
							'option' => 'hop',
						],
						'identifiant' => '@m'
					],
					[
						'saisie' => 'input',
						'options' => [
							'nom' => 'identique',
						],
						'identifiant' => '@i'
					],
					[
					'saisie' => 'fieldset',
							'options' => [
								'nom' => 'identique_fieldset',
							],
							'saisies' => [
								[
									[
										'saisie' => 'input',
										'options' => [
											'nom' => 'label'
										]
									]
								]
							],
					]
				],
				// Avec conteneur
				false,
				// tri
				'identifiant',
			]
		];
	}

	/**
	 * @dataProvider dataComparer
	*/
	public function testComparer($expected, $anciennes, $nouvelles, $avec_conteneur = true, $tri = 'nom') {
		$actual = saisies_comparer($anciennes, $nouvelles, $avec_conteneur, $tri);
		$this->assertEquals($expected, $actual);
	}


	public static function dataComparerRappel() {
		return [
			'ok' => [
				0,
				[
					'saisie' => 'input',
					'options' => [
						'nom' => 'input'
					]
				],
				[
					'saisie' => 'input',
					'options' => [
						'nom' => 'input'
					]
				]
			],
			[
				1,
				[
					'saisie' => 'input',
					'options' => [
						'nom' => 'input'
					]
				],
				[
					'saisie' => 'input',
					'options' => [
						'nom' => 'input2'
					]
				]
			]
		];
	}

	/**
	 * @dataProvider dataComparerRappel
	*/
	public function testComparerRappel($expected, $a, $b) {
		$actual = saisies_comparer_rappel($a, $b);
		$this->assertEquals($expected, $actual);
	}

	public static function dataComparerParIdentifiant() {
		return [
			'sans_conteneur_id' => [
				// Expected
				[
					'ajoutees' => [
						'@n' => [
							'saisie' => 'input',
							'options' => [
								'nom' => 'nouvelle',
							],
							'identifiant' => '@n',
						]
					],
					'supprimees' => [
						'@s' => [
							'saisie' => 'input',
							'options' => [
								'nom' => 'supprimee',
							],
							'identifiant' => '@s',
						]
					],
					'modifiees' => [
						'@m' => [
							'saisie' => 'input',
							'options' => [
								'nom' => 'modifiees',
								'option' => 'hop',
							],
							'identifiant' => '@m',
						]
					],
					'identiques' => [
						'@i' => [
							'saisie' => 'input',
							'options' => [
								'nom' => 'identique',
							],
							'identifiant' => '@i',
						],
					]
				],
				// Anciennes
				[
					[
						'saisie' => 'input',
						'options' => [
							'nom' => 'supprimee',
						],
						'identifiant' => '@s'
					],
					[
						'saisie' => 'input',
						'options' => [
							'nom' => 'modifiees',
						],
						'identifiant' => '@m'
					],
					[
						'saisie' => 'input',
						'options' => [
							'nom' => 'identique',
						],
						'identifiant' => '@i'
					],
					[
					'saisie' => 'fieldset',
							'options' => [
								'nom' => 'identique_fieldset',
							],
							'saisies' => [
								[
									[
										'saisie' => 'input',
										'options' => [
											'nom' => 'label'
										]
									]
								]
							],
						]
				],
				// Nouvelle
				[
					[
						'saisie' => 'input',
						'options' => [
							'nom' => 'nouvelle',
						],
						'identifiant' => '@n'
					],
					[
						'saisie' => 'input',
						'options' => [
							'nom' => 'modifiees',
							'option' => 'hop',
						],
						'identifiant' => '@m'
					],
					[
						'saisie' => 'input',
						'options' => [
							'nom' => 'identique',
						],
						'identifiant' => '@i'
					],
					[
					'saisie' => 'fieldset',
							'options' => [
								'nom' => 'identique_fieldset',
							],
							'saisies' => [
								[
									[
										'saisie' => 'input',
										'options' => [
											'nom' => 'label'
										]
									]
								]
							],
					]
				],
				// Avec conteneur
				false,
			]
		];
	}

	/**
	 * @dataProvider dataComparerParIdentifiant
	*/
	public function testComparerParIdentifiant($expected, $anciennes, $nouvelles, $avec_conteneur = true) {
		$actual = saisies_comparer_par_identifiant($anciennes, $nouvelles, $avec_conteneur);
		$this->assertEquals($expected, $actual);
	}

	public static function dataSaisiesAutonomes() {
		return [
			[
				[
				'fieldset',
				'conteneur_inline',
				'hidden',
				'destinataires',
				'explication',
				'champ',
				]
			]
		];
	}

	/**
	 * @dataProvider dataSaisiesAutonomes
	*/
	public function testSaisiesAutonomes($expected) {
		$actual = saisies_autonomes();
		$this->assertEquals($expected, $actual);
	}

	public static function dataChercher() {
		$saisies = [
			[
				'saisie' => 'fieldset',
				'options' => [
					'nom' => 'fieldset_nom',
					'label' => 'fieldset_label',
				],
				'identifiant' => '@f',
				'saisies' => [
					[
						'saisie' => 'input',
						'identifiant' => '@i',
						'options' => [
							'label' => 'input_label',
							'nom' => 'input_nom',
						]
					]
				]
			]
		];

		return [
			'par_id' => [
				// Expected
				[
					'saisie' => 'input',
					'identifiant' => '@i',
					'options' => [
						'label' => 'input_label',
						'nom' => 'input_nom',
					]
				],
				// Provided
				$saisies,
				'@i',
			],
			'par_id_echec' => [
				// Expected
				null,
				// Provided
				$saisies,
				'@iii',
			],
			'par_nom' => [
				// Expected
				[
					'saisie' => 'input',
					'identifiant' => '@i',
					'options' => [
						'label' => 'input_label',
						'nom' => 'input_nom',
					]
				],
				// Provided
				$saisies,
				'input_nom',
			],
			'par_chemin' => [
				// Expected
				[
					'saisie' => 'input',
					'identifiant' => '@i',
					'options' => [
						'label' => 'input_label',
						'nom' => 'input_nom',
					]
				],
				// Provided
				$saisies,
				[0,'saisies',0],
			],
			'par_chemin_null' => [
				// Expected
				null,
				// Provided
				$saisies,
				[0,2],
			],
			'par_id_return_chemin' => [
				// Expected
				[0, 'saisies', 0],
				// Provided
				$saisies,
				'@i',
				true
			]
		];
	}

	/**
	 * @dataProvider dataChercher
	*/
	public function testChercher($expected, $saisies, $id_ou_nom_ou_chemin, $trouver_chemin = false) {
		$actual = saisies_chercher($saisies, $id_ou_nom_ou_chemin, $trouver_chemin);
		$this->assertEquals($expected, $actual);
	}

	static public function dataListerChampsParSection() {
		$input = [
			'options' => ['des_options_globales'],
			[
				'saisie' => 'fieldset',
				'options' => [
					'nom' => 'fieldset',
					'label' => 'fieldset_label',
				],
				'saisies' => [
					[
						'saisie' => 'fieldset',
						'options' => [
							'nom' => 'sous_fieldset',
							'label' => 'sous_fieldset_label',
						],
						'saisies' => [
								[
									'saisie' => 'conteneur_inline',
									'saisies' => [
										[
											'saisie' => 'input',
											'options' => [
												'nom' => 'input',
												'label' => 'input_label',
											]
										],
										[
											'saisie' => 'explication',
										]
									]
								]
						]
					]
				]
			]
		];
		return [
			'sans_option' => [
				// Expected
				[
					'fieldset' => [
						'sous_fieldset' => [
							'input' => [
								'saisie' => 'input',
								'options' => [
									'nom' => 'input',
									'label' => 'input_label',
								]
							]
						]
					]
				],
				// Provided
				$input
			],
			'forcer_type_simple' => [
				// Expected
				[
					'fieldset' => [
						'sous_fieldset' => [
						]
					]
				],
				// Provided
				$input,
				// Options
				[
					'forcer_type' => 'radio'
				]
			],
			'forcer_type_array' => [
				// Expected
				[
					'fieldset' => [
						'sous_fieldset' => [
							'input' => [
								'saisie' => 'input',
								'options' => [
									'nom' => 'input',
									'label' => 'input_label',
								]
							]
						]
					]
				],
				// Provided
				array_merge($input, [
					[
						'saisie' => 'fieldset',
						'options' => [
							'label' => 'fieldset_sans_rien',
							'nom' => 'fieldset_sans_rien',
						],
						'saisies' => []
					]
				]),
				// Options
				[
					'forcer_type' => ['radio', 'checkbox', 'input'],
					'sans_vide' => true,
				]
			],
			'callback_champ' => [
				// Expected
				[
					'fieldset' => [
						'sous_fieldset' => ['input' => 'input_label']
						]
				],
				// Provided
				$input,
				// Options
				[
					'callback_champ' => 'saisies_saisie_get_label'
				]
			],
			'callback_section' => [
				// Expected
				[
					'fieldset_label' => [
						'sous_fieldset_label' => [
							'input' => [
								'saisie' => 'input',
								'options' => [
									'nom' => 'input',
									'label' => 'input_label',
								]
							]
						]
					]
				],
				// Provided
				$input,
				// Options
				[
					'callback_section' => 'saisies_saisie_get_label'
				]
			],
			'profondeur_max_output' => [
				// Expected
				[
					'fieldset' => [
							'input' => [
								'saisie' => 'input',
								'options' => [
									'nom' => 'input',
									'label' => 'input_label',
								]
							]
						]
				],
				// Provided
				$input,
				// Options
				[
					'profondeur_max_output' => 1,
				]
			]
		];
	}


	/**
	 * @dataProvider dataListerChampsParSection()
	 * @uses conteneur_inline_est_avec_sous_saisies()
	 * @uses conteneur_inline_est_labelisable()
	 * @uses explication_est_champ()
	 * @uses fieldset_est_avec_sous_saisies()
	 * @uses saisies_saisie_est_avec_sous_saisies()
	 * @uses saisies_saisie_est_champ()
	 * @uses saisies_saisie_est_labelisable()
	 * @uses saisies_saisie_get_label()
	*/
	public function testListerChampsParSection($expected, $saisies, $options = []) {
		$actual = saisies_lister_champs_par_section($saisies, $options);
		$this->assertSame($expected, $actual);
	}

	public static function dataDontAvecOption() {
		return [
			'true' => [
				// Expected
				true,
				// Saisies
				[
					[
						'saisie' => 'fieldset',
						'saisies' => [
								[
								'saisie' => 'input',
								'options' => [
									'afficher_si' => 'oui'
								]
							]
						]
					]
				],
				// Option
				'afficher_si'
			],
			'false' => [
				// Expected
				false,
				// Saisies
				[
					[
						'saisie' => 'fieldset',
						'saisies' => [
							[
								'saisie' => 'input',
								'options' => [
									'afficher_si' => ''
								]
							]
						]
					]
				],
				// Option
				'afficher_si'
			],
			'vide' => [
				// Expected
				false,
				// saisies
				null,
				// option
				''
		]
		];
	}

	/**
	 * @dataProvider dataDontAvecOption
	**/
	public function testDontAvecOption($expected, $saisies, $option) {
		$actual = saisies_dont_avec_option($saisies, $option);
		$this->assertSame($expected, $actual);
	}
}
