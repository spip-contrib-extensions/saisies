<?php

namespace Spip\Saisies\Tests;

use PHPUnit\Framework\TestCase;

/**
 * @covers selection_valeurs_acceptables()
 * @covers radio_valeurs_acceptables()
 * @covers selection_multiple_valeurs_acceptables()
 * @covers checkbox_valeurs_acceptables()
 * @covers choix_grille_valeurs_acceptables()
 * @uses saisies_aplatir_tableau
 * @uses saisies_chaine2tableau
 * @uses saisies_normaliser_liste_choix
 * @uses saisies_saisie_est_gelee
 * @uses saisies_trouver_data
 * @uses saisies_valeur2tableau
 * @internal
 */
class VerifierValeursAcceptablesTest extends TestCase {

	public static function dataSelection() {
		return [
			'vide_pas_obligatoire' => [
				// Expected
				true,
				'',
				[
					'saisie' => 'selection',
					'options' => [
						'obligatoire' => '',
					]
				]
			],
			'geleee_ok' => [
				// Expected
				true,
				'non',
				[
					'saisie' => 'selection',
					'options' => [
						'defaut' => 'non',
						'disable' => 'on',
						'disable_avec_post' => 'on'
					]
				]
			],
			'geleee_pas_ok' => [
				// Expected
				false,
				'non',
				[
					'saisie' => 'selection',
					'options' => [
						'defaut' => 'oui',
						'disable' => 'on',
						'disable_avec_post' => 'on'
					]
				]
			],
			'in_data' => [
				// Expected
				true,
				'non',
				[
					'saisie' => 'selection',
					'options' => [
						'data' => [
							'oui' => 'oui',
							'non' => 'non',
						]
					]
				]
			],
			'out_data' => [
				// Expected
				false,
				'tartempion',
				[
					'saisie' => 'selection',
					'options' => [
						'data' => [
							'oui' => 'oui',
							'non' => 'non',
						]
					]
				]
			],
			'dans_disable' => [
				// Expected
				false,
				'oui',
				[
					'saisie' => 'selection',
					'options' => [
						'data' => [
							'oui' => 'oui',
							'non' => 'non',
						],
						'disable_choix' => ['oui']
					]
				]
			],
			'alternatif' => [
				// Expected
				true,
				'alternatif',
				[
					'saisie' => 'selection',
					'options' => [
						'data' => [
							'oui' => 'oui',
							'non' => 'non',
						],
						'choix_alternatif' => true
					]
				]
			],
			'multiple_ok' => [
				// Expected
				true,
				['oui', 'non'],
				[
					'saisie' => 'selection',
					'options' => [
						'data' => [
							'oui' => 'oui',
							'non' => 'non',
						],
						'multiple' => 'on',
					]
				]
			],
			'multiple_pas_ok' => [
				// Expected
				false,
				['oui2', 'non2'],
				[
					'saisie' => 'selection',
					'options' => [
						'data' => [
							'oui' => 'oui',
							'non' => 'non',
						],
						'multiple' => 'on',
					]
				]
			],
			'dans_depublie_mais_pas_ancienne_valeur' => [
				// Expected
				false,
				'oui',
				[
					'saisie' => 'selection',
					'options' => [
						'data' => [
							'oui' => 'oui',
							'non' => 'non',
						],
						'ancienne_valeur' => 'noncnon',
						'depublie_choix' => ['oui']
					],
				],
			],
			'dans_depublie_mais_ancienne_valeur' => [
				// Expected
				true,
				'oui',
				[
					'saisie' => 'selection',
					'options' => [
						'data' => [
							'oui' => 'oui',
							'non' => 'non',
						],
						'ancienne_valeur' => 'oui',
						'depublie_choix' => ['oui']
					],
				],
			]
		];
	}

	/**
	 * @dataProvider dataSelection
	 **/
	public function testSelection($expected, $valeur, array $saisie) {
		$actual = selection_valeurs_acceptables($valeur, $saisie);
		$this->assertEquals($expected, $actual);
	}

	/**
	 * @dataProvider dataSelection
	 * Normalement les radios et les selections c'est la même règle
	 **/
	public function testRadio($expected, $valeur, array $saisie) {
		$actual = radio_valeurs_acceptables($valeur, $saisie);
		$this->assertEquals($expected, $actual);
	}

	public static function dataSelectionMultiple() {
		return [
			'basic_true' => [
				// Expected
				true,
				// Valeur
				['a', 'b', 'c'],
				// Saisie
				[
					'options' => [
						'data' => [
							'a' => 'a_label',
							'b' => 'b_label',
							'c' => 'c_label',
						],
					]
				]
			],
			'basic_false' => [
				// Expected
				false,
				// Valeur
				['a', 'd'],
				// Saisie
				[
					'options' => [
						'data' => [
							'a' => 'a_label',
							'b' => 'b_label',
							'c' => 'c_label',
						],
					]
				]
			],
			'disable_false' => [
				// Expected
				false,
				// Valeur
				['a', 'b'],
				// Saisie
				[
					'options' => [
						'data' => [
							'a' => 'a_label',
							'b' => 'b_label',
							'c' => 'c_label',
						],
						'disable_choix' => 'a'
					]
				]
			],
			'string_true' => [
				// Expected
				true,
				// Valeur
				'a b c',
				// Saisie
				[
					'options' => [
						'data' => [
							'a' => 'a_label',
							'b' => 'b_label',
							'c' => 'c_label',
						],
					]
				]
			],
			'string_false' => [
				// Expected
				false,
				// Valeur
				'a b d',
				// Saisie
				[
					'options' => [
						'data' => [
							'a' => 'a_label',
							'b' => 'b_label',
							'c' => 'c_label',
						],
					]
				]
			],
			'string_vide' => [
				// Expected
				true,
				// Valeur
				'',
				// Saisie
				[
					'options' => [
						'data' => [
							'a' => 'a_label',
							'b' => 'b_label',
							'c' => 'c_label',
						],
					]
				]
			],
			'choix_alternatif_true' => [
				// Expected
				true,
				// Valeur
				['a', 'b', 'choix_alternatif' => 'hop'],
				// Saisie
				[
					'options' => [
						'data' => [
							'a' => 'a_label',
							'b' => 'b_label',
							'c' => 'c_label',
						],
						'choix_alternatif' => true,
					]
				]
			],
			'defaut_identique' => [
				// Expected
				true,
				// Valeur
				['a', 'b'],
				// Saisie
				[
					'options' => [
						'data' => [
							'a' => 'a_label',
							'b' => 'b_label',
							'c' => 'c_label',
							'd' => 'd_label',
						],
						'defaut' => 'a, b',
						'disable' => true,
						'disable_avec_post' => true
					]
				]
			],
			'defaut_manque' => [
				// Expected
				false,
				// Valeur
				['a'],
				// Saisie
				[
					'options' => [
						'data' => [
							'a' => 'a_label',
							'b' => 'b_label',
							'c' => 'c_label',
							'd' => 'd_label',
						],
						'defaut' => ['a', 'b'],
						'disable' => true,
						'disable_avec_post' => true
					]
				]
			],
			'defaut_trop' => [
				// Expected
				false,
				// Valeur
				['a', 'b', 'c'],
				// Saisie
				[
					'options' => [
						'data' => [
							'a' => 'a_label',
							'b' => 'b_label',
							'c' => 'c_label',
							'd' => 'd_label',
						],
						'defaut' => ['a', 'b'],
						'disable' => true,
						'disable_avec_post' => true
					]
				]
			],
			'defaut_totalement_different' => [
				// Expected
				false,
				// Valeur
				['c', 'd'],
				// Saisie
				[
					'options' => [
						'data' => [
							'a' => 'a_label',
							'b' => 'b_label',
							'c' => 'c_label',
							'd' => 'd_label',
						],
						'defaut' => ['a', 'b'],
						'disable' => true,
						'disable_avec_post' => true
					]
				]
			],
			'dans_depublie_mais_pas_ancienne_valeur' => [
				// Expected
				false,
				// Valeur
				['c', 'd'],
				// Saisie
				[
					'options' => [
						'data' => [
							'a' => 'a1',
							'b' => 'b1',
							'c' => 'c1',
							'd' => 'd1',
						],
						'depublie_choix' => 'c',
					]
				]
			],
			'dans_depublie_mais_dans_ancienne_valeur' => [
				// Expected
				true,
				// Valeur
				['c', 'd'],
				// Saisie
				[
					'options' => [
						'data' => [
							'a' => 'a1',
							'b' => 'b1',
							'c' => 'c1',
							'd' => 'd1',
						],
						'depublie_choix' => 'c',
						'ancienne_valeur' => ['a', 'c']
					]
				]
			]
		];
	}


	/**
	 * @dataProvider dataSelectionMultiple
	 **/
	public function testSelectionMultiple($expected, $valeur, array $saisie) {
		$actual = selection_multiple_valeurs_acceptables($valeur, $saisie);
		$this->assertEquals($expected, $actual);
	}
	/**
	 * @dataProvider dataSelectionMultiple
	 * Une checkbox c'est une selection multiple, _in fine_
	 **/
	public function test_checkbox($expected, $valeur, array $saisie) {
		$actual = checkbox_valeurs_acceptables($valeur, $saisie);
		$this->assertEquals($expected, $actual);
	}


	public static function dataChoixGrille() {
		return [
			'ok' => [
				// Expected
				true,
				// Valeur
				['a' => 'valeurA', 'b' => 'valeurB'],
				// saisie
				[
					'saisie' => 'choix_grille',
					'options' => [
						'data_rows' => [
							'a' => 'Ligne A',
							'b' => 'Ligne B'
						],
						'data_cols' => [
							'valeurA' => 'col A',
							'valeurB' => 'col B'
						]
					]
				]
			],
			'pas_ok' => [
				// Expected
				false,
				// Valeur
				['a' => 'valeurA', 'b' => 'valeur_HACK_B'],
				// saisie
				[
					'saisie' => 'choix_grille',
					'options' => [
						'data_rows' => [
							'a' => 'Ligne A',
							'b' => 'Ligne B'
						],
						'data_cols' => [
							'valeurA' => 'col A',
							'valeurB' => 'col B'
						]
					]
				]
			]
		];
	}

	/**
	 * @dataProvider dataChoixGrille
	 **/
	public function testChoixGrille($expected, $valeur, array $saisie) {
		$actual = choix_grille_valeurs_acceptables($valeur, $saisie);
		$this->assertEquals($expected, $actual);
	}
}
