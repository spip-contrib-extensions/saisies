<?php

namespace Spip\Saisies\Tests;

use PHPUnit\Framework\TestCase;

/**
 * @covers saisies_identifier()
 * @covers saisie_identifier()
 * @covers saisies_supprimer_identifiants()
 * @uses saisies_lister_par_identifiant()
 */
class IdentifiantsTest extends TestCase {

	public static function dataIdentifier() {
		return [
			[// Expected
				3,
				// Provided
				[
					[
						'saisie' => 'fieldset',
						'options' => ['nom' => 'a'],
						'saisies' => [
							[
								'saisie' => 'input',
								'options' => ['nom' => 'b'],
							],
							[
								'saisie' => 'input',
								'options' => ['nom' => 'c'],
							]
						]
					]
				]
			]
		];
	}

	/**
	 * @dataProvider dataIdentifier
	 */
	public function testIdentifier($expected, $saisies) {
		$saisies = saisies_identifier($saisies);
		$saisies_par_identifiant = saisies_lister_par_identifiant($saisies);
		$saisies_par_identifiant_regeneres = saisies_lister_par_identifiant(saisies_identifier($saisies, true));

		$this->assertEquals(count($saisies_par_identifiant), $expected);
		$this->assertNotEquals($saisies_par_identifiant, $saisies_par_identifiant_regeneres);

		// For the case we don't provide a []
		$this->assertEquals([], saisies_identifier(''));
	}

	public static function dataSupprimerIdentifiants() {
		return [
			[
				// Expected
				[
					[
						'saisie' => 'fieldset',
						'options' => ['nom' => 'a'],
						'saisies' => [
							[
								'saisie' => 'input',
								'options' => ['nom' => 'b'],
							],
							[
								'saisie' => 'input',
								'options' => ['nom' => 'c'],
							]
						]
					]
				],
				// Provided
				[
					[
						'saisie' => 'fieldset',
						'options' => ['nom' => 'a'],
						'identifiant' => 'a',
						'saisies' => [
							[
								'saisie' => 'input',
								'options' => ['nom' => 'b'],
								'identifiant' => 'b',
							],
							[
								'saisie' => 'input',
								'options' => ['nom' => 'c'],
								'identifiant' => 'c',
							]
						]
					]
				],
			]
		];
	}

	/**
	 * @dataProvider dataSupprimerIdentifiants
	*/
	public function testSupprimerIdentifiants($expected, $provided) {
		$actual = saisies_supprimer_identifiants($provided);
		$this->assertEquals($expected, $actual);
	}
}
