<?php

namespace Spip\Saisies\Tests;

use PHPUnit\Framework\TestCase;

/**
 * @covers saisies_verifier()
 * @covers saisies_set_request_recursivement()
 * @covers saisies_appliquer_depublie_recursivement()
 * @covers saisies_saisie_verifier_obligatoire()
 * @uses saisie_nom2name()
 * @uses saisies_afficher_si_liste_masquees()
 * @uses saisies_afficher_si_masquees_set_request_empty_string()
 * @uses saisies_get_valeur_saisie()
 * @uses saisies_liste_set_request()
 * @uses saisies_lister_par_nom()
 * @uses saisies_request()
 * @uses saisies_saisie_est_fichier()
 * @uses saisies_set_request()
 * @uses saisies_afficher_si_filtrer_parse_condition()
 * @uses saisies_afficher_si_secure()
 * @uses saisies_afficher_si_verifier_syntaxe()
 * @uses saisies_evaluer_afficher_si()
 * @uses saisies_parser_condition_afficher_si()
 * @uses saisies_transformer_condition_afficher_si()
 * @uses saisies_verifier_afficher_si()
 * @uses saisies_lister_par_etapes()
 * @uses saisies_lister_finales()
 * @uses saisie_editable()
 * @uses saisies_name_suffixer()
 * @uses saisies_supprimer_depublie()
 * @uses saisies_supprimer_callback()
 * @runTestsInSeparateProcesses
 * @internal
 */

class VerifierTest extends TestCase {

	public static function dataVerifier() {
		return [
			'mono_etape_obligatoire_afficher_si_rempli' => [
				// Expected
				[
					'obligatoire' => 'info_obligatoire',
				],
				// Formulaire
				[
					[
						'saisie' => 'input',
						'options' => [
							'nom' => 'obligatoire',
							'obligatoire' => 'oui',
							'afficher_si' => 'true'
						]
					],
					[
						'saisie' => 'input',
						'options' => [
							'nom' => 'pasobligatoire',
							'obligatoire' => 'non',
						]
					]
				],
				// Masquees empty string
				true,
				// Pas d'etape
				null,
				// Valeur explicite
				[
					'obligatoire' => ''
				]
			],
			'mono_etape_obligatoire_afficher_si_pas_rempli' => [
				// Expected
				[
				],
				// Formulaire
				[
					[
						'saisie' => 'input',
						'options' => [
							'nom' => 'obligatoire',
							'obligatoire' => 'oui',
							'afficher_si' => 'false'
						]
					],
					[
						'saisie' => 'input',
						'options' => [
							'nom' => 'pasobligatoire',
							'obligatoire' => 'non',
						]
					]
				],
				// Masquees empty string
				true,
				// Pas d'etape
				null,
				// Valeur explicite
				[
					'obligatoire' => ''
				]
			],
			'mono_etape_verifier_entier_ancienne_syntaxe' => [
				// Expected
				[
					'entier' => 'pas_entier',
				],
				// Formulaire
				[
					[
						'saisie' => 'input',
						'options' => [
							'nom' => 'entier',
						],
						'verifier' => [
							'type' => 'entier',
						]
					],
				],
				// Masquees empty string
				false,
				// Pas d'etape
				null,
				// Valeur explicite
				[
					'entier' => 'hop'
				]
			],
			'mono_etape_verifier_entier_nouvelle_syntaxe' => [
				// Expected
				[
					'entier' => 'pas_entier<br />pas_entier',
				],
				// Formulaire
				[
					[
						'saisie' => 'input',
						'options' => [
							'nom' => 'entier',
						],
						'verifier' => [
							[
								'type' => 'entier',
							],
							[
								'type' => 'entier',
							],
						]
					],
				],
				// Masquees empty string
				false,
				// Pas d'etape
				null,
				// Valeur explicite
				[
					'entier' => 'hop'
				]
			],
			'mono_etape_verifier_entier_depublie' => [
				// Expected
				[
				],
				// Formulaire
				[
					[
						'saisie' => 'input',
						'options' => [
							'nom' => 'entier',
							'depublie' => 'on'
						],
						'verifier' => [
							[
								'type' => 'entier',
							]
						]
					],
				],
				// Masquees empty string
				false,
				// Pas d'etape
				null,
				// Valeur explicite
				[
					'entier' => 'hop'
				]
			],
			'mono_etape_normaliser_entier_ancienne_syntaxe' => [
				// Expected
				[
				],
				// Formulaire
				[
					[
						'saisie' => 'input',
						'options' => [
							'nom' => 'entier',
						],
						'verifier' => [
							[
								'type' => 'entier',
							],
							[
								'type' => 'entier',
							],
						]
					],
				],
				// Masquees empty string
				false,
				// Pas d'etape
				null,
				// Valeur explicite
				[
					'entier' => 22
				]
			],
			'multi_etape_obligatoire' => [
				// Expected
				[
					'obligatoire' => 'info_obligatoire',
				],
				// Formulaire
				[
					'options' => ['etapes_activer' => true],
					[
						'saisie' => 'fieldset',
						'options' => [
							'label' => 'etape_1',
							'nom' => 'etape_1',
						],
						'saisies' => [
							[
								'saisie' => 'input',
								'options' => [
									'nom' => 'obligatoire',
									'obligatoire' => 'oui',
								]
							],
							[
								'saisie' => 'input',
								'options' => [
									'nom' => 'pasobligatoire',
									'obligatoire' => 'non',
								]
							]
						]
					],
					[
						'saisie' => 'fieldset',
						'options' => [
							'nom' => 'etape_2',
							'label' => 'etape_2'
						],
						'saisies' => [
							[
								'saisie' => 'input',
								'options' => [
									'nom' => 'obligatoire_2',
									'obligatoire' => 'oui',
								]
							],
							[
								'saisie' => 'input',
								'options' => [
									'nom' => 'pasobligatoire_2',
									'obligatoire' => 'non',
								]
							]
						]
					]
				],
				// Masquees empty string
				true,
				// Pas d'etape
				1,
				// Valeur explicite
				[
					'obligatoire' => ''
				]
			],
			'multi_etape_obligatoire_etape_masquee' => [
				// Expected
				[
				],
				// Formulaire
				[
					'options' => ['etapes_activer' => true],
					[
						'saisie' => 'fieldset',
						'options' => [
							'label' => 'etape_1',
							'nom' => 'etape_1',
							'afficher_si' => 'false',
						],
						'saisies' => [
							[
								'saisie' => 'input',
								'options' => [
									'nom' => 'obligatoire',
									'obligatoire' => 'oui',
								]
							],
							[
								'saisie' => 'input',
								'options' => [
									'nom' => 'pasobligatoire',
									'obligatoire' => 'non',
								]
							]
						]
					],
					[
						'saisie' => 'fieldset',
						'options' => [
							'nom' => 'etape_2',
							'label' => 'etape_2'
						],
						'saisies' => [
							[
								'saisie' => 'input',
								'options' => [
									'nom' => 'obligatoire_2',
									'obligatoire' => 'oui',
								]
							],
							[
								'saisie' => 'input',
								'options' => [
									'nom' => 'pasobligatoire_2',
									'obligatoire' => 'non',
								]
							]
						]
					]
				],
				// Masquees empty string
				true,
				// Etape à valider
				1,
				// Valeur explicite
				[
					'obligatoire' => ''
				]
			],
			'multi_etape_obligatoire_afficher_si_pas_rempli_etape1' => [
				// Expected
				[
				],
				// Formulaire
				[
					'options' => ['etapes_activer' => true, 'etapes_ignorer_recapitulatif' => true],
					[
						'saisie' => 'fieldset',
						'options' => [
							'nom' => 'f1',
							'label' => 'f1',
						],
						'saisies' => [
							[
								'saisie' => 'input',
								'options' => [
									'nom' => 'obligatoire',
									'obligatoire' => 'oui',
									'afficher_si' => 'false'
								]
							],
							[
								'saisie' => 'input',
								'options' => [
									'nom' => 'pasobligatoire',
									'obligatoire' => 'non',
								]
							]
						]
					],
					[
						'saisie' => 'fieldset',
						'options' => [
							'nom' => 'f2',
							'label' => 'f2',
						],
						'saisies' => [
							[
								'saisie' => 'input',
								'options' => [
									'nom' => 'pasobligatoire',
									'obligatoire' => 'non',
								]
							]
						]
					]
				],
				//
				true,
				1,
				['obligatoire' => 'hop']
			],
			'multi_etape_obligatoire_afficher_si_pas_rempli_etape2' => [
				// Expected
				[
				],
				// Formulaire
				[
					'options' => ['etapes_activer' => true, 'etapes_ignorer_recapitulatif' => true],
					[
						'saisie' => 'fieldset',
						'options' => [
							'nom' => 'f1',
							'label' => 'f1',
						],
						'saisies' => [
							[
								'saisie' => 'input',
								'options' => [
									'nom' => 'obligatoire',
									'obligatoire' => 'oui',
									'afficher_si' => 'false'
								]
							],
							[
								'saisie' => 'input',
								'options' => [
									'nom' => 'pasobligatoire',
									'obligatoire' => 'non',
								]
							]
						]
					],
					[
						'saisie' => 'fieldset',
						'options' => [
							'nom' => 'f2',
							'label' => 'f2',
						],
						'saisies' => [
							[
								'saisie' => 'input',
								'options' => [
									'nom' => 'pasobligatoire',
									'obligatoire' => 'non',
								]
							]
						]
					]
				],
				//
				true,
				2,
				['obligatoire' => 'hop']
			],
		];
	}

	/**
	 * @dataProvider dataVerifier
	 * @uses saisies_lister_avec_option()
	 * @uses saisies_chaine2tableau()
	**/
	public function testVerifier(array $expected, array $formulaire, bool $saisies_masquees_empty_string = true, ?int $etape = null, ?array $valeurs = null) {

		$erreurs = saisies_verifier($formulaire, $saisies_masquees_empty_string, $etape, $valeurs);
		$this->assertEquals($expected, $erreurs);
		// Verifier que les afficher_si ont bien été passé à `''` si jamais ils ne sont pas affichés
		$saisies_avec_afficher_si = saisies_lister_avec_option('afficher_si', saisies_lister_finales($formulaire));
		foreach ($saisies_avec_afficher_si as $saisie) {
			if (
					$saisie['saisie'] != 'fieldset'
					&& !saisies_evaluer_afficher_si($saisie['options']['afficher_si'], $valeurs, $formulaire)
			) {
				$actual = saisies_liste_set_request('get');
				$actual = $actual[$saisie['options']['nom']] ?? [$valeurs[$saisie['options']['nom']]];
				$actual = end($actual);
				$saisies_par_etapes = saisies_lister_par_etapes($formulaire);
				if (!$saisies_par_etapes) {
					$saisies_par_etapes = $formulaire;
				}
				if (
					empty($erreurs)
					&&  ($etape === count($saisies_par_etapes)
					&& !_request('aller_a_etape', $valeurs))
					|| (!$etape)
				) {
					$this->assertEquals('', $actual);
				} else {
					$this->assertEquals($valeurs[$saisie['options']['nom']], $actual);
				}
			}
		}
	}

	public static function dataAppliquerPublierRecursivement() {
		return [
			'basic' => [
				// Expected
				[
					[
						'saisie' => 'fieldset',
						'options' => ['depublie' => 'on'],
						'saisies' => [
							[
								'saisie' => 'fieldset',
								'options' => ['depublie' => 'on'],
								'saisies' => [
									[
										'saisie' => 'input1',
										'options' => ['depublie' => 'on']
									]
								]
							]
						]
					],
					[
						'saisie' => 'input2',
						'options' => ['depublie' => 'on']
					],
					[
						'saisie' => 'input3',
					]
				],
				// Provided
				[
					[
						'saisie' => 'fieldset',
						'options' => ['depublie' => 'on'],
						'saisies' => [
							[
								'saisie' => 'fieldset',
								'saisies' => [
									[
										'saisie' => 'input1',
									]
								]
							]
						]
					],
					[
						'saisie' => 'input2',
						'options' => ['depublie' => 'on']
					],
					[
						'saisie' => 'input3',
					]
				]
			],
			'second_niveau' => [
				// Expected
				[
					[
						'saisie' => 'fieldset1',
						'options' => [],
						'saisies' => [
							[
								'saisie' => 'fieldset2',
								'options' => ['depublie' => 'on'],
								'saisies' => [
									[
										'saisie' => 'input1',
										'options' => ['depublie' => 'on']
									]
								]
							]
						]
					],
					[
						'saisie' => 'input2',
						'options' => ['depublie' => 'on']
					],
					[
						'saisie' => 'input3',
					]
				],
				// Provided
				[
					[
						'saisie' => 'fieldset1',
						'options' => [],
						'saisies' => [
							[
								'saisie' => 'fieldset2',
								'options' => [
									'depublie' => 'on',
								],
								'saisies' => [
									[
										'saisie' => 'input1',
									]
								]
							]
						]
					],
					[
						'saisie' => 'input2',
						'options' => ['depublie' => 'on']
					],
					[
						'saisie' => 'input3',
					]
				]
			]
		];
	}

	/**
	 * @dataProvider dataAppliquerPublierRecursivement
	**/
	public function testAppliquerPublierRecursivement(array $expected, array $provided) {
		$actual = saisies_appliquer_depublie_recursivement($provided);
		$this->assertEquals($expected, $actual);
	}

	public static function dataVerifierObligatoire() {
		return [
			'message_obligation_pas_rempli' => [
				// Expected
				'erreur_obligatoire',
				// Saisie
				[
					'saisie' => 'input',
					'options' => [
						'obligatoire' => 'on',
						'erreur_obligatoire' => 'erreur_obligatoire'
					]
				],
				// Valeur
				''
			],
			'obligation_pas_rempli_apres_trim' => [
				// Expected
				'info_obligatoire',
				// Saisie
				[
					'saisie' => 'input',
					'options' => [
						'obligatoire' => 'on',
					]
				],
				// Valeur
				'   '
			],
			'obligation_0' => [
				// Expected
				'',
				// Saisie
				[
					'saisie' => 'input',
					'options' => [
						'obligatoire' => 'on',
					]
				],
				// Valeur
				'0'
			],
			'obligation_pas_remplie_mais_depublie' => [
				// Expected
				'',
				// Saisie
				[
					'saisie' => 'input',
					'options' => [
						'obligatoire' => 'on',
						'depublie' => 'on'
					]
				],
				// Valeur
				''
			],
			'obligation_non' => [
				// Expected
				'',
				// Saisie
				[
					'saisie' => 'input',
					'options' => [
						'obligatoire' => 'non',
					]
				],
				// Valeur
				''
			],
			'sans_obligation' => [
				// Expected
				'',
				// Saisie
				[
					'saisie' => 'input',
					'options' => [
					]
				],
				// Valeur
				''
			],
			'tableau_non_rempli' => [
				// Expected
				'info_obligatoire',
				// Saisie
				[
					'saisie' => 'input',
					'options' => [
						'obligatoire' => 'on',
					]
				],
				// Valeur
				[]
			],
			'tableau_rempli' => [
				// Expected
				'',
				// Saisie
				[
					'saisie' => 'input',
					'options' => [
						'obligatoire' => 'on',
					]
				],
				// Valeur
				['']
			],
			'fichier_rempli' => [
				// Expected
				'',
				// Saisie
				[
					'saisie' => 'input',
					'options' => [
						'obligatoire' => 'on',
						'type' => 'file',
					]
				],
				// Valeur
				['']
			],
			'fichier_pas_rempli' => [
				// Expected
				'info_obligatoire',
				// Saisie
				[
					'saisie' => 'input',
					'options' => [
						'obligatoire' => 'on',
						'type' => 'file',
					]
				],
				// Valeur
				null
			],
			'grille_ok' => [
				// Expected
				'',
				// Saisie
				[
					'saisie' => 'choix_grille',
					'options' => [
						'data_rows' => "a|a2\r\nb|b2",
						'obligatoire' => 'on',
					]
				],
				['a' => 'valeur', 'b' => 'valeurb']
			],
			'grille_pas_ok' => [
				// Expected
				'info_obligatoire',
				// Saisie
				[
					'saisie' => 'choix_grille',
					'options' => [
						'data_rows' => "a|a2\r\nb|b2",
						'obligatoire' => 'on',
					]
				],
				['a' => 'valeur']
			],
			'grille_checkbox_ok' => [
				// Expected
				'',
				// Saisie
				[
					'saisie' => 'choix_grille',
					'options' => [
						'data_rows' => "a|a2\r\nb|b2",
						'obligatoire' => 'on',
						'multiple' => 'on'
					]
				],
				['a' => ['valeur1', 'valeur2'], 'b' => ['valeurb1', 'valeurb2']]
			],
			'grille_checkbox_pas_ok' => [
				// Expected
				'info_obligatoire',
				// Saisie
				[
					'saisie' => 'choix_grille',
					'options' => [
						'data_rows' => "a|a2\r\nb|b2",
						'obligatoire' => 'on',
						'multiple' => 'on',
					]
				],
				['a' => ['valeur']]
			],
		];

	}

	/**
	 * @dataProvider dataVerifierObligatoire
	 * @uses saisies_chaine2tableau()
	**/
	public function testVerifierObligatoire($expected, $saisie, $valeur) {
		$actual = saisies_saisie_verifier_obligatoire($saisie, $valeur);
		$this->assertSame($expected, $actual);
	}
}
