<?php

namespace Spip\Saisies\Tests;


use PHPUnit\Framework\TestCase;

/**
 * @covers saisies_saisie_est_tabulaire()
 * @covers saisies_saisie_est_fichier()
 * @covers saisies_saisie_est_gelee()
 * @covers saisies_saisie_est_avec_sous_saisies()
 * @covers saisies_saisie_est_labelisable()
 * @covers saisies_saisie_est_champ()
 * @covers saisies_saisie_get_label()
 * @uses saisie_nom2name()
 */
class InfoTest extends TestCase {


	public static function dataSaisieEstTabulaire() {
		return [
			'checkbox' => [
				true,
				[
					'saisie' => 'checkbox'
				]
			],
			'selection_multiple' => [
				true,
				[
					'saisie' => 'selection_multiple'
				]
			],
			'choix_grille' => [
				true,
				[
					'saisie' => 'choix_grille'
				]
			],
			'selection' => [
				false,
				[
					'saisie' => 'selection',
					'options' => [

					]
				]
			],
			'selection_avec_multiple' => [
				true,
				[
					'saisie' => 'selection',
					'options' => [
						'multiple' => 'on'
					]
				]
			],
			'selection_sans_multiple' => [
				false,
				[
					'saisie' => 'selection',
					'options' => [
						'multiple' => false
					]
				]
			],
			'input' => [
				false,
				[
					'saisie' => 'input',

				]
			]
		];
	}

	/**
	 * @dataProvider dataSaisieEstTabulaire
	**/
	public function testEstTabulaire($expected, $saisie) {
		$actual = saisies_saisie_est_tabulaire($saisie);
		$this->assertEquals($expected, $actual);
	}

	public static function dataSaisieEstFichier() {
		return [
			'checkbox' => [
				false,
				[
					'saisie' => 'checkbox'
				]
			],
			'input_pwd' => [
				false,
				[
					'saisie' => 'input',
					'options' => [
						'type' => 'password'
					]
				]
			],
			'input_standard' => [
				false,
				[
					'saisie' => 'input',
				]
			],
			'input_file' => [
				true,
				[
					'saisie' => 'input',
					'options' => [
						'type' => 'file'
					]
				]
			],
			'fichiers' => [
				true,
				[
					'saisie' => 'fichiers'
				]
			],
		];
	}

	/**
	 * @dataProvider dataSaisieEstFichier
	**/
	public function testEstFichier($expected, $saisie) {
		$actual = saisies_saisie_est_fichier($saisie);
		$this->assertEquals($expected, $actual);
	}


	public static function dataSaisieEstGelee() {

		return [
			'standard' => [
				// Expected
				false,
				// Saisie
				[
					'saisie' => 'input',
					'options' => []
				]
			],
			'read_only' => [
				// Expected
				true,
				// Saisie
				[
					'saisie' => 'input',
					'options' => [
						'readonly' => 'on'
					]
				]
			],
			'read_only_not' => [
				// Expected
				false,
				// Saisie
				[
					'saisie' => 'input',
					'options' => [
						'readonly' => ''
					]
				]
			],
			'disable' => [
				// Expected
				false,
				// Saisie
				[
					'saisie' => 'input',
					'options' => [
						'disable' => 'on'
					]
				],
			],
			'disable_avec_post' => [
				// Expected
				true,
				// Saisie
				[
					'saisie' => 'input',
					'options' => [
						'disable' => 'on',
						'disable_avec_post' => 'on'
					]
				]
			],
			'disable_avec_post_non' => [
				// Expected
				false,
				// Saisie
				[
					'saisie' => 'input',
					'options' => [
						'disable' => 'on',
						'disable_avec_post' => ''
					]
				]
			]
		];
	}

	/**
	 * @dataProvider dataSaisieEstGelee
	*/
	public function testSaisieEstGelee($expected, $saisie) {
		$actual = saisies_saisie_est_gelee($saisie);
		$this->assertEquals($expected, $actual);
	}

	public static function dataSaisieEstAvecSousSaisies() {
		return [
			'input' => [
				// Expected
				false,
				// Saisies
				[
					'saisie' => 'input',
				]
			],
			'fieldset_meme_sans_enfant' => [
				true,
				[
					'saisie' => 'fieldset',
				]
			],
			'conteneur_inline_meme_sans_enfant' => [
				true,
				[
					'saisie' => 'conteneur_inline',
				]
			]
		];
	}

	/**
	 * @dataProvider dataSaisieEstAvecSousSaisies
	 * @covers fieldset_est_avec_sous_saisies()
	 * @covers conteneur_inline_est_avec_sous_saisies()
	 **/
	public function testSaisieEstAvecSousSaisies($expected, $saisie) {
		$actual = saisies_saisie_est_avec_sous_saisies($saisie);
		$this->assertSame($expected, $actual);
	}


	public static function dataSaisieEstLabelisable() {
		return [
			'input_meme_sans_label' => [
				// Expected
				true,
				// Saisies
				[
					'saisie' => 'input',
				]
			],
			'conteneur_inline_meme_sans_enfant' => [
				false,
				[
					'saisie' => 'conteneur_inline',
				]
			]
		];
	}

	/**
	 * @dataProvider dataSaisieEstLabelisable
	 * @covers conteneur_inline_est_labelisable()
	 **/
	public function testSaisieEstLabelisable($expected, $saisie) {
		$actual = saisies_saisie_est_labelisable($saisie);
		$this->assertSame($expected, $actual);
	}

	public static function dataSaisieEstChamp() {
		return [
			'input' => [
				// Expected
				true,
				// Saisies
				[
					'saisie' => 'input',
				]
			],
			'conteneur_inline' => [
				false,
				[
					'saisie' => 'conteneur_inline',
				]
			],
			'fieldset' => [
				false,
				[
					'saisie' => 'fieldset',
				]
			],
			'explication' => [
				false,
				[
					'saisie' => 'explication',
				]
			],
		];
	}

	/**
	 * @dataProvider dataSaisieEstChamp
	 * @covers fieldset_est_champ()
	 * @covers explication_est_champ()
	 * @covers conteneur_inline_est_champ()
	 **/
	public function testSaisieEstChamp($expected, $saisie) {
		$actual = saisies_saisie_est_champ($saisie);
		$this->assertSame($expected, $actual);
	}


	public static function dataSaisieGetLabel() {
		return [
			'standard' => [
				// Expected
				'label',
				// Saisie
				[
					'saisie' => 'hop',
					'options' => [
						'label' => 'label'
					]
				]
			],
			'sans_label' => [
				// Expected
				'',
				// Saisie
				[
					'saisie' => 'hop',
				]
			],
			'case_label_case' => [
				// Expected
				'label_case',
				// Saisie
				[
					'saisie' => 'case',
					'options' => [
						'label' => 'label',
						'label_case' => 'label_case'
					]
				]
			],
			'case_label' => [
				// Expected
				'label',
				// Saisie
				[
					'saisie' => 'case',
					'options' => [
						'label' => 'label',
						'label_case' => ''
					]
				]
			],
			'explication_titre' => [
				// Expected
				'titre',
				// Saisie
				[
					'saisie' => 'explication',
					'options' => [
						'titre' => 'titre',
						'texte' => 'texte'
					]
				]
			],
			'explication_texte' => [
				// Expected
				'texte',
				// Saisie
				[
					'saisie' => 'explication',
					'options' => [
						'texte' => 'texte'
					]
				]
			]
		];
	}
	/**
	 * @dataProvider dataSaisieGetLabel
	 * @covers case_get_label()
	 * @covers explication_get_label()
	 **/
	public function testSaisieGetLabel($expected, $saisie) {
		$actual = saisies_saisie_get_label($saisie);
		$this->assertSame($expected, $actual);
	}
}
