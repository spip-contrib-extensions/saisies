<?php

namespace Spip\Saisies\Tests;

use PHPUnit\Framework\TestCase;

/**
 * @covers saisie_nom2classe()
 * @covers saisie_type2classe()
 * @covers saisie_nom2name()
 * @covers saisie_name2nom()
 * @covers saisies_cles_nom2name()
 * @covers saisies_name_suffixer()
 * @covers saisies_name_supprimer_suffixe()
 */
class NameTest extends TestCase {

	public static function dataNom2Classe() {
		return [
			'simple' => [
				// Expected
				'toto',
				// Provided = nom
				'toto'
			],
			'name' => [
				// Expected
				'toto_truc',
				// Provided = nom
				'toto[truc]'
			],
			'nom' => [
				// Expected
				'toto_truc',
				// Provided = nom
				'toto/truc'
			]
		];
	}

	/**
	 * @dataProvider dataNom2Classe
	*/
	public function testNom2Classe($expected, $nom) {
		$actual = saisie_nom2classe($nom);
		$this->assertEquals($expected, $actual);
	}


	public static function dataType2Classe() {
		return [
			'hidden' => [
				'saisie_hidden',
				'hidden'
			],
			'selecteur' => [
				'saisie_selecteur_article selecteur_item',
				'selecteur_article'
			],
		];
	}

	/**
	 * @dataProvider dataType2Classe
	*/
	public function testType2Classe($expected, $type) {
		$actual = saisie_type2classe($type);
		$this->assertEquals($expected, $actual);
	}

	public static function dataNom2name() {
		return [
			'simple' => [
				'toto',
				'toto'
			],
			'name' => [
				'toto[truc]',
				'toto[truc]'
			],
			'nom' => [
				'toto[truc]',
				'toto/truc'
			],
			'nom_numerique' => [
				'toto[truc][]',
				'toto/truc/'
			]
		];
	}

	/**
	 * @dataProvider dataNom2Name
	*/
	public function testNom2name($expected, $nom) {
		$actual = saisie_nom2name($nom);
		$this->assertEquals($expected, $actual);
	}

	public static function dataName2Nom() {
		return [
			'simple' => [
				'toto',
				'toto'
			],
			'nom' => [
				'toto/truc',
				'toto/truc'
			],
			'name' => [
				'toto/truc',
				'toto[truc]',
			],
			'name_numerique' => [
				'toto/truc/',
				'toto[truc][]',
			]
		];
	}

	/**
	 * @dataProvider dataName2nom
	*/
	public function testName2Nom($expected, $nom) {
		$actual = saisie_name2nom($nom);
		$this->assertEquals($expected, $actual);
	}


	public static function dataClesNom2name() {
		return [
			[
				[
					'toto' => 'message',
					'toto[truc]' => 'message',
					'toto[truc]' => 'message',
					'toto[truc][]' => 'message'
				],
				[
					'toto' => 'message',
					'toto[truc]' => 'message',
					'toto/truc' => 'message',
					'toto/truc/' => 'message'
				]
			],
			[
				'hop',
				'hop'
			]
		];
	}

	/**
	 * @dataProvider dataClesNom2Name
	*/
	public function testClesNom2name($expected, $nom) {
		$actual = saisies_cles_nom2name($nom);
		$this->assertEquals($expected, $actual);
	}

	public static function dataNameSuffixer() {
		return [
			'basic' => [
				// Expected
				'name_suffixe',
				// Name
				'name',
				// suffixe
				'suffixe'
			],
			'tabulaire' => [
				// Expected
				'name[sub1][sub_suffixe]',
				// Name
				'name[sub1][sub]',
				// suffixe
				'suffixe'
			],
		];
	}

	/**
	 * @dataProvider dataNameSuffixer
	**/
	public function testNameSuffixer($expected, $name, $suffixe) {
		$actual = saisies_name_suffixer($name, $suffixe);
		$this->assertSame($expected, $actual);
	}

	public static function dataNameSupprimerSuffixe() {
		return [
			'basic' => [
				// Expected
				'name',
				// Name
				'name_suffixe',
				// suffixe
				'suffixe'
			],
			'tabulaire' => [
				// Expected
				'name[sub1][sub]',
				// Name
				'name[sub1][sub_suffixe]',
				// suffixe
				'suffixe'
			],
		];
	}

	/**
	 * @dataProvider dataNameSupprimerSuffixe
	**/
	public function testNameSupprimerSuffixe($expected, $name, $suffixe) {
		$actual = saisies_name_supprimer_suffixe($name, $suffixe);
		$this->assertSame($expected, $actual);
	}
}
