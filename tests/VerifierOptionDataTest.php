<?php

namespace Spip\Saisies\Tests;

use PHPUnit\Framework\TestCase;

/**
 * @covers verifier_saisies_option_data_dist()
 * @internal
 */

class VerifierOptionDataTest extends TestCase {

	/**
	 * We don't cover the `'verifier_cles'` options
	 * as it depends of `verifier` plugin.
	**/
	public static function dataVerifierOptionData() {
		return [
			'ok_sous_groupe' => [
				// Expected
				'',
				// Provided
				"A|A\r\n"
				. "B|B\r\n"
				. "* Sous groupe\r\n"
				. "C|C\r\n"
				. "/*",
			],
			'interdire_sous_groupes' => [
				// Expected
				'saisies:verifier_saisies_option_data_sous_groupes_interdits',
				// Provided
				"A|A\r\n"
				. "B|B\r\n"
				. "* Sous groupe\r\n"
				. "C|C\r\n"
				. "/*",
				['interdire_sous_groupes' => true],
			],
			'doublons' => [
				// Expected
				'saisies:verifier_saisies_option_data_cles_doubles',
				// Provided
				"A|A\r\n"
				. "A|A\r\n"
			],
			'implicites' => [
				// Expected
				'saisies:verifier_saisies_option_data_cle_manquante',
				// Provided
				"1|Un\r\n"
				. "Deux\r\n"
			]
		];
	}

	/**
	 * @dataProvider dataVerifierOptionData
	**/
	public function testVerifierOptionData($expected, $valeurs, $options = []) {
		$actual = verifier_saisies_option_data_dist($valeurs, $options);
		$this->assertEquals($expected, $actual);
	}
}
