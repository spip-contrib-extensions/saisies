<?php

require_once __DIR__ . '/../vendor/autoload.php';

require_once (__DIR__ . "/../verifier/saisies_option_data.php");
require_once (__DIR__ . "/../formulaires/construire_formulaire.php");
require_once (__DIR__ . "/../saisies_afficher_si_js/defaut.php");
require_once (__DIR__ . "/../saisies_fonctions.php");
require_once (__DIR__ . "/../saisies_pipelines.php");
require_once (__DIR__ . "/../vendor/spip/ecrire/inc/filtres.php");
require_once (__DIR__ . "/../vendor/spip/ecrire/inc/filtres_mini.php");
require_once (__DIR__ . "/../vendor/spip/ecrire/inc/charsets.php");
require_once (__DIR__ . "/../vendor/spip/ecrire/charsets/iso-8859-1.php");
require_once (__DIR__ . "/../vendor/spip/ecrire/charsets/translit.php");
require_once (__DIR__ . "/../vendor/spip/ecrire/bootstrap/inc/logging.php");
define ('_DEFAULT_CHARSET', 'utf-8');
define ('_ROOT_RACINE', '/');
$GLOBALS['meta']['pcre_u'] = 'u';
$GLOBALS['meta']['charset'] = 'utf-8';
$GLOBALS['spip_lang'] = 'fr';
load_charset();
define('_LOG_CRITIQUE', false);
define('_LOG_ERREUR', false);
use Symfony\Component\Yaml\Yaml;
use Symfony\Component\Yaml\Exception\ParseException;
// decorate SPIP…
if (!function_exists('include_spip')) {
	function include_spip() {
		return true;
	}
	define('_ECRIRE_INC_VERSION', true);
}

if (!function_exists('spip_log')) {
	define('_LOG_CRITIQUE', false);
	define('_LOG_ERREUR', false);
	function spip_log() {
	}
}

if (!function_exists('session_get')) {
	function session_get($champ) {
		$i = [
			'email' => 'email@domaine',
			'nom' => 'NOM'
		];
		return $i[$champ] ?? '';
	}
}
if (!function_exists('set_request')) {
	/**
	 * Pour gérer l'appel à `_request()` dans le code de `saisie` sans charger le code de SPIP...
	 * @param string $action 'set' ou 'get'
	 * @param string $champ
	 * @param string $valeur
	 **/
	function pseudo_request($action, $champ, $valeur = '') {
		static $internal = [];
		if ($action === 'get') {
			return $internal[$champ] ?? [];
		}
		if ($action === 'set') {
			$internal[$champ] = $valeur;
		}
	}
	function _request($champ, $env = null) {
		if (!$env) {
			return pseudo_request('get', $champ);
		} else {
			return $env[$champ] ?? '';
		}
	}

	function set_request($champ, $valeur, &$env = null) {
		if ($env === null) {
			pseudo_request('set', $champ, $valeur);
		} else {
			$env[$champ] = $valeur;
		}
	}
}


if (!function_exists('pipeline')) {
	function pipeline($nom, $flux) {
		if (is_array($flux)) {
			if ($nom === 'saisies_lister_categories') {
				$flux['viapipeline'] = [
					'nom' => 'viapipeline',
				];
			}
			return $flux['data'] ?? $flux;
		} else {
			return $flux;
		}
	}
}

if (!function_exists('test_plugin_actif')) {
	function test_plugin_actif($plugin) {
		return $plugin === 'saisies';
	}
}

if (!function_exists('lire_config')) {
	function lire_config($champ) {
		return $GLOBALS['meta'][$champ] ?? saisies_request("config/$champ");
	}
	function ecrire_config($champ, $valeur) {
		saisies_set_request("config/$champ", $valeur);
	}
}

if (!function_exists('find_in_path')) {

	/**
	 * Pseudo find_in_path()
	 * Recherche uniquement dans le dossier tests/
	 */
	function find_in_path($file) {
		return __DIR__ . '/' . $file;
	}
}

if (!function_exists('yaml_decode_file')) {
	// Honteusement pompé depuis le plugin YAML v3.0.3


	/**
	 * Ici ce n'est pas le code du plugin YAML, mais un appel brut et direct à la libraire YAML de symfony
	 * @param string $fichier
	 *
	 * @return array|mixed
	 */
	function yaml_decode_file($fichier, $options = []) {

		$retour = Yaml::parseFile($fichier);
		return $retour;
	}

	/**
	 * pseudo yaml_charger_inclusions.
	 * Fort heureusement on n'utilise pas d'inclusion dans les fichiers de tests
	 * @param array;
	 * @return array;
	 **/
	function yaml_charger_inclusions($flux) {
		return $flux;
	}


}

if (!function_exists('charger_fonction')) {
	/**
	 * Version simplifier de charger_foncions, qui ne s'occupe pas de la recup en chemin
	 **/
	function charger_fonction($fonctions, $prefix) {
		$f = "{$prefix}_{$fonctions}";
		if (function_exists($f)) {
			return $f;
		}
		return false;
	}

	/*
	 * A termer on fera un appel à verifier en tant que composer
	**/
	function inc_verifier($valeur, $type, $options, &$normaliser = null) {
		if ($type = 'entier') {
			if (is_int($valeur)) {
				$normaliser = "+$valeur";//Pseudo normalisation, on verra bien
				return '';
			} else {
				return 'pas_entier';
			}
		}
	}

}



if (!function_exists('_T')) {
	function _T($i, $args = []) {
		if ($args) {
			return $i.json_encode($args);
		} else {
			return $i;
		}
	}
	function typo($i) {
		return $i;
	}
	function interdire_scripts($i) {
		return $i;
	}
}


if (!defined('vider_date')) {
	/**
	 * Pompé depuis SPIP 5 dev
	 *
	 * @param string $letexte
	 * @param bool $verif_format_date
	 * @return string
	 *     - La date entrée (si elle n'est pas considérée comme nulle)
	 *     - Une chaine vide
	 **/
	function vider_date($letexte, $verif_format_date = false): string {
		$letexte ??= '';
		if (
			!$verif_format_date
			|| in_array(strlen($letexte), [10,19]) && preg_match('/^\d{4}-\d{2}-\d{2}(\s\d{2}:\d{2}:\d{2})?$/', $letexte)
		) {
			if (strncmp('0000-00-00', $letexte, 10) == 0) {
				return '';
			}
			if (strncmp('0001-01-01', $letexte, 10) == 0) {
				return '';
			}
			if (strncmp('1970-01-01', $letexte, 10) == 0) {
				return '';
			}  // eviter le bug GMT-1
		}
		return $letexte;
	}
}



if (!defined('_T_ou_typo')) {
	/**
	 *  _T_ou_typo, trop dur à tester pour l'heure, c'est une sort de pseudo_mock
	 *  @param string
	 *  @return string
	 **/
	function _T_ou_typo(string $i): string {
		return $i;
	}
}

if (!function_exists('find_all_in_path')) {
	/**
	 * Pseudo find_all_in_path()
	 * Recherche uniquement dans le dossier tests/
	 */
	function find_all_in_path($rep, $pattern) {
		$r = __DIR__ . '/' . $rep;
		$d = opendir($r);
		$return = [];
		while ($f = readdir($d)) {
			if ($f != '.' && $f != '..') {
				if (preg_match("#$pattern#", $f)) {
					$return[$f] = "$r$f";
				}
			}
		}
		return $return;
	}
}


if (!function_exists('recuperer_fond')) {
	// Pour récuperer fond, on envoi un tableau qui donne le nom et les args du fond
	function recuperer_fond(string $nom, array $contexte): array{
		return ['nom' => $nom, 'contexte' => $contexte];
	}
}



foreach (scandir(__DIR__ . '/../inc') as $f) {
	if (str_contains($f, '.php')) {
		require_once (__DIR__ . "/../inc/$f");
	}
}
foreach (scandir(__DIR__ . '/../saisies') as $f) {
	if (str_contains($f, '.php')) {
		require_once (__DIR__ . "/../saisies/$f");
	}
}
