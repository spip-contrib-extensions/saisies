<?php

namespace Spip\Saisies\Tests;

use PHPUnit\Framework\TestCase;

/**
 * @covers saisie_option_contenu_vide()
 * @covers construire_formulaire_transformer_nom()
 * @covers construire_formulaire_transformer_afficher_si()
 * @covers construire_formulaire_config_inserer_option_depublie()
 * @covers saisie_identifier()
 * @covers saisies_chercher()
 * @covers saisies_inserer()
 * @covers saisies_inserer_selon_chemin()
 * @covers saisies_supprimer_identifiants()
 * @internal
 */

class ConstruireFormulaireTest extends TestCase {

	public static function dataOptionContenuVide() {
		return [
			'zero_chaine' => [
				True,
				'0',
			],
			'vide' => [
				False,
				'',
			],
			'zero_entier' => [
				False,
				0
			],
			'non_vide' => [
				True,
				'hop',
			],
		]
		;
	}

	/**
	 * @dataProvider dataOptionContenuVide
	 */
	public function testOptionContenuVide($expected, $actual) {
		$actual = saisie_option_contenu_vide($actual);
		$this -> assertSame($expected, $actual);
	}


	public static function dataTransformerNom() {
		return [
			'nom' => [
				'tableau[truc][nom_de_la_saisie]',
				'nom_de_la_saisie',
				'nom',
				'tableau[truc][@valeur@]',
			],
			'pas_nom' => [
				'defaut_de_la_saisie',
				'defaut_de_la_saisie',
				'defaut',
				'tableau[truc][@valeur@]',
			],
		]
		;
	}

	/**
	 * @dataProvider dataTransformerNom
	 */
	public function testTransformerNom($expected, $valeur, $cle, $transformation) {
		construire_formulaire_transformer_nom($valeur, $cle, $transformation);
		$this -> assertSame($expected, $valeur);
	}

	public static function dataTransformerAfficherSi() {
		return [
			'afficher_si_champ' => [
				'@options_globales[etapes_activer]@ == "on"',
				'@etapes_activer@ == "on"',
				'afficher_si',
				'options_globales'
			],
			'afficher_si_config' => [
				'@config:toto@ == "on"',
				'@config:toto@ == "on"',
				'afficher_si',
				'options_globales'
			],
			'afficher_si_plugin' => [
				'@plugin:toto@ == "on"',
				'@plugin:toto@ == "on"',
				'afficher_si',
				'options_globales'
			],
			'afficher_si_saisie_modifiee' => [
				'@saisie_modifiee[toto]@ == "on"',
				'@saisie_modifiee[toto]@ == "on"',
				'afficher_si',
				'options_globales'
			],
			'afficher_si_complexe' => [
				'@options_globales[etapes_activer]@ == "on" && @plugin:toto@ == "hop" && @config:titi@ == "hip"',
				'@etapes_activer@ == "on" && @plugin:toto@ == "hop" && @config:titi@ == "hip"',
				'afficher_si',
				'options_globales'
			],
			'pas_afficher_si' => [
				'@etapes_activer@ == "on"',
				'@etapes_activer@ == "on"',
				'defaut',
				'options_globales'
			]
		];
	}

	/**
	 * @dataProvider dataTransformerAfficherSi
	 */
	public function testTransformerAfficherSi($expected, $valeur, $cle, $name_html) {
		construire_formulaire_transformer_afficher_si($valeur, $cle, $name_html);
		$this -> assertSame($expected, $valeur);
	}

	public static function dataInsererOptionDepublie() {
		return [
			'exemple' => [
				// Expected
				[
					[
						'saisie' => 'fieldset',
						'options' => [
							'nom' => 'description'
						],
						'saisies' => [
							[
								'saisie' => 'inputexistante'
							],
						]
					],
					[
						'saisie' => 'fieldset',
						'options' => [
							'nom' => 'affichage'
						],
						'saisies' => [
							[
								'saisie' => 'case',
								'options' => [
									'nom' => 'depublie',
									'label_case' => '<:saisies:option_depublie_label_case:>',
									'explication' => '<:saisies:option_depublie_explication:>',
									'conteneur_class' => 'pleine_largeur',
								]
							],
							[
								'saisie' => 'inputexistante2',
							]
						]
					]
				],
				// Provided
				[
					[
						'saisie' => 'fieldset',
						'options' => [
							'nom' => 'description'
						],
						'saisies' => [
							[
								'saisie' => 'inputexistante',
							]
						]
					],
					[
						'saisie' => 'fieldset',
						'options' => [
							'nom' => 'affichage'
						],
						'saisies' => [
							[
								'saisie' => 'inputexistante2',
							]
						]
					]
				]
			],
			'sans_affichage' => [
				// Expected
				[
					[
						'saisie' => 'fieldset',
						'options' => [
							'nom' => 'description'
						],
						'saisies' => [
							[
								'saisie' => 'case',
								'options' => [
									'nom' => 'depublie',
									'label_case' => '<:saisies:option_depublie_label_case:>',
									'explication' => '<:saisies:option_depublie_explication:>',
									'conteneur_class' => 'pleine_largeur',
								]
							],
							[
								'saisie' => 'inputexistante'
							],
						]
					],
					[
						'saisie' => 'fieldset',
						'options' => [
							'nom' => 'pasaffichage'
						],
						'saisies' => [
							[
								'saisie' => 'inputexistante2',
							]
						]
					]
				],
				// Provided
				[
					[
						'saisie' => 'fieldset',
						'options' => [
							'nom' => 'description'
						],
						'saisies' => [
							[
								'saisie' => 'inputexistante',
							]
						]
					],
					[
						'saisie' => 'fieldset',
						'options' => [
							'nom' => 'pasaffichage'
						],
						'saisies' => [
							[
								'saisie' => 'inputexistante2',
							]
						]
					]
				]
			]
		];
	}
	/**
	 * @dataProvider dataInsererOptionDepublie
	 */
	public function testInsererOptionsDepublie($expected, $valeur) {
		$actual = saisies_supprimer_identifiants(construire_formulaire_config_inserer_option_depublie($valeur));
		$this -> assertSame($expected, $actual);
	}
}
