<?php

namespace Spip\Saisies\Tests;

use PHPUnit\Framework\TestCase;

/**
 * @covers saisies_supprimer()
 * @covers saisies_mapper_option()
 * @covers saisies_transformer_noms()
 * @covers saisies_transformer_noms_auto()
 * @covers saisies_transformer_option()
 * @covers saisies_encapsuler_noms()
 * @covers saisies_inserer()
 * @covers saisies_inserer_apres()
 * @covers saisies_inserer_avant()
 * @covers saisies_inserer_selon_chemin()
 * @covers saisies_dupliquer()
 * @covers saisies_fieldsets_en_onglets()
 * @covers saisies_supprimer_sans_reponse()
 * @covers saisies_supprimer_callback()
 * @covers saisies_supprimer_depublie()
 * @covers saisies_supprimer_depublie_sans_reponse()
 * @covers saisies_saisie_possede_reponse()
 * @covers saisies_deplacer()
 * @covers saisies_deplacer_avant()
 * @covers saisies_deplacer_apres()
 * @covers saisies_modifier()
 * @covers saisies_supprimer_option()
 * @covers saisies_inserer_html()
 * @covers saisies_wrapper_fieldset()
 * @covers saisies_mapper_verifier()
 * @uses saisie_identifier()
 * @uses saisies_chercher()
 * @uses saisies_supprimer_identifiants()
 * @uses saisies_chercher()
 * @uses saisies_comparer()
 * @uses saisies_chaine2tableau()
 * @uses saisies_generer_nom()
 * @uses saisies_lister_champs()
 * @uses saisies_lister_par_nom()
 * @uses saisie_nom2name()
 * @uses saisie_nom2classe()
 * @uses saisies_request()
 * @uses saisies_request_from_FILES()
 * @uses spip_log
 * @uses spip_logger
 */

class ManipulerTest extends TestCase {

	public static function dataSupprimerCallback() {
		return [
			'defaut' => [
				// Expected
				[
					['saisie' => true,
					'options' => [
						'bool' => true
					],
					'saisies' => [
							[
								'saisie' => true,
								'options' => [
									'bool' => true
								]
							]
						]
					]
				],
				// saisies
				[
					[
						'saisie' => false,
						'options' => [
							'bool' => false
						],
					],
					[
						'saisie' => true,
						'options' => [
							'bool' => true
						],
						'saisies' => [
								[
								'saisie' => false,
								'options' => [
									'bool' => false
									]
								],
								[
								'saisie' => true,
								'options' => [
									'bool' => true
									]
							],
						],
					]
				],
				// callback
				function ($saisie) {
					return $saisie['options']['bool'];
				}
			],
		];
	}

	/**
	 * @dataProvider dataSupprimerCallback
	 */
	public function testSupprimerCallback($expected, $provided, $function) {
		$actual = saisies_supprimer_callback($provided, $function);
		$this->assertEquals($expected, $actual);
	}

	public static function dataSupprimerDepublie() {
		return [
			'defaut' => [
				// Expected
				[
					['saisie' => true,
					'options' => [
						'depublie' => false
					],
					'saisies' => [
							[
								'saisie' => true,
								'options' => [
									'depublie' => ''
								]
							],
							[
								'saisie' => true,
								'options' => [
								]
							]
						]
					]
				],
				// saisies
				[
					[
						'saisie' => false,
						'options' => [
							'depublie' => 'on'
						],
					],
					[
						'saisie' => false,
						'options' => [
							'depublie' => true
						],
						'saisies' => [
								[
								'saisie' => false,
								'options' => [
									'depublie' => 'on'
									]
								],
								[
								'saisie' => true,
								'options' => [
									'depublie' => ''
									]
								],
								[
								'saisie' => true,
								'options' => [
									]
								]
						],
					],
					[
						'saisie' => true,
						'options' => [
							'depublie' => false
						],
						'saisies' => [
								[
								'saisie' => false,
								'options' => [
									'depublie' => 'on'
									]
								],
								[
								'saisie' => true,
								'options' => [
									'depublie' => ''
									]
								],
								[
								'saisie' => true,
								'options' => [
									]
								]
						],
					]
				],
			]
		];
	}
	/**
	 * @dataProvider dataSupprimerDepublie
	 */
	public function testSupprimerDepublie($expected, $provided) {
		$actual = saisies_supprimer_depublie($provided);
		$this->assertEquals($expected, $actual);
	}

	public static function dataSupprimerDepublieSansReponse() {
		return [
			'defaut' => [
				// Expected
				[
					[
						'saisie' => true,
						'options' => [
							'depublie' => false,
							'nom' => 's6',
						],
						'saisies' => [
							[
								'saisie' => true,
								'options' => [
									'depublie' => '',
									'nom' => 's8'
								]
							],
							[
								'saisie' => true,
								'options' => [
									'nom' => 's9',
								]
							]
						]
					],
					[
						'saisie' => true,
						'options' => [
							'depublie' => true,
							'nom' => 's10',
						],
						'saisies' => [
							[
								'saisie' => true,
								'options' => [
									'nom' => 's11',
								]
							],
							[
								'saisie' => true,
								'options' => [
									'nom' => 's12'
								]
							]
						]
					],
					[
						'saisie' => true,
						'options' => [
							'nom' => 's17',
							'depublie' => 'on',
						]
					]
				],
				// saisies
				[
					[
						'saisie' => false,
						'options' => [
							'depublie' => 'on',
							'nom' => 's1'
						],
					],
					[
						'saisie' => false,
						'options' => [
							'depublie' => true,
							'nom' => 's2'

						],
						'saisies' => [
							[
								'saisie' => false,
								'options' => [
									'depublie' => 'on',
									'nom' => 's3'
								]
							],
							[
								'saisie' => true,
								'options' => [
									'depublie' => '',
									'nom' => 's4'
								]
							],
							[
								'saisie' => true,
								'options' => [
									'nom' => 's5'
								]
							]
						],
					],
					[
						'saisie' => true,
						'options' => [
							'depublie' => false,
							'nom' => 's6'
						],
						'saisies' => [
							[
								'saisie' => false,
								'options' => [
									'depublie' => 'on',
									'nom' => 's7'
								]
							],
							[
								'saisie' => true,
								'options' => [
									'depublie' => '',
									'nom' => 's8'
								]
							],
							[
								'saisie' => true,
								'options' => [
									'nom' => 's9'
								]
							]
						],
					],
					[
						'saisie' => true,
						'options' => [
							'depublie' => true,
							'nom' => 's10',
						],
						'saisies' => [
							[
								'saisie' => true,
								'options' => [
									'nom' => 's11',
								]
							],
							[
								'saisie' => true,
								'options' => [
									'nom' => 's12',
								]
							]
						]
					],
					[
						'saisie' => true,
						'options' => [
							'depublie' => true,
							'nom' => 's13',
						],
						'saisies' => [
							[
								'saisie' => true,
								'options' => [
									'nom' => 's14',
								]
							],
							[
								'saisie' => true,
								'options' => [
									'nom' => 's15',
								]
							]
						]
					],
					[
						'saisie' => true,
						'options' => [
							'nom' => 's16',
							'depublie' => 'on',
						]
					],
					[
						'saisie' => true,
						'options' => [
							'nom' => 's17',
							'depublie' => 'on',
						]
					]
				],
				// Valeurs
				[
					's11' => 'cocorico',
					's17' => 'cocorico'
				]
			]
		];
	}
	/**
	 * @dataProvider dataSupprimerDepublieSansReponse
	 * @uses saisies_saisie_est_fichier
	 */
	public function testSupprimerDepublieSansReponse($expected, $saisies, $reponses) {
		$actual = saisies_supprimer_depublie_sans_reponse($saisies, $reponses);
		$this->assertEquals($expected, $actual);
	}


	public static function dataSupprimer() {
		return [
			'nom' => [
				[
					[
						'saisie' => 'toto',
						'options' => [
							'label' => 'label',
							'nom' => 'ongarde'
						]
					],
					'options' => [
						'option_generale1' => 'hop',
						'option_generale2' => 'hop',
					]
				],
				[
					[
						'saisie' => 'toto',
						'options' => [
							'label' => 'label',
							'nom' => 'ongarde'
						]
					],
					[
						'saisie' => 'toto',
						'options' => [
							'label' => 'label_ongardepas',
							'nom' => 'ongardepas'
						]
					],
					'options' => [
						'option_generale1' => 'hop',
						'option_generale2' => 'hop',
					]
				],
				'ongardepas'
			],
			'id' => [
				[
					[
						'saisie' => 'toto',
						'options' => [
							'label' => 'label',
							'nom' => 'ongarde'
						]
					]
				],
				[
					[
						'saisie' => 'toto',
						'options' => [
							'label' => 'label',
							'nom' => 'ongarde'
						]
					],
					[
						'saisie' => 'toto',
						'options' => [
							'label' => 'label_ongardepas',
							'nom' => 'ongardepas',
						],
						'identifiant' => '@hash@'
					],
				],
				'@hash@'
			],
			'chemin' => [
				[
					[
						'saisie' => 'toto',
						'options' => [
							'label' => 'label',
							'nom' => 'ongarde'
						]
					]
				],
				[
					[
						'saisie' => 'toto',
						'options' => [
							'label' => 'label',
							'nom' => 'ongarde'
						]
					],
					[
						'saisie' => 'toto',
						'options' => [
							'label' => 'label_ongardepas',
							'nom' => 'ongardepas',
						],
					],
				],
				[1]
			],
			'id_profond' => [
				[
					[
						'saisie' => 'fieldset',
						'options' => [
							'label' => 'fieldset',
							'nom' => 'fieldset',
						],
						'saisies' => [
							[
								'saisie' => 'toto',
								'options' => [
									'label' => 'label',
									'nom' => 'ongarde'
								]
							]
						]
					]
				],
				[
					[
						'saisie' => 'fieldset',
						'options' => [
							'label' => 'fieldset',
							'nom' => 'fieldset',
						],
						'saisies' => [
							[
								'saisie' => 'toto',
								'options' => [
									'label' => 'label',
									'nom' => 'ongarde'
								]
							],
							[
								'saisie' => 'toto',
								'options' => [
									'label' => 'label_ongardepas',
									'nom' => 'ongardepas',
								],
								'identifiant' => '@hash@'
							],
						]
					]
				],
				'@hash@'
			]
		];
	}

	/**
	 * @dataProvider dataSupprimer
	 */
	public function testSupprimer($expected, $provided, $id_ou_nom_ou_chemin) {
		$actual = saisies_supprimer($provided, $id_ou_nom_ou_chemin);
		$this->assertEquals($expected, $actual);
	}

	public static function dataMapper() {
		return [
			[
				[
					[
						'saisie' => 'choix_grille_1',
						'options' =>
						[
							'nom' => 'choix_grille_1',
							'data_rows' => ['a' => 'a', 'b' => 'b'],
							'data_cols' => ['a' => 'a', 'b' => 'b']
						]
					],
				],
				[

					[
						'saisie' => 'choix_grille_1',
						'options' =>
						[
							'nom' => 'choix_grille_1',
							'data_rows' => 'a|a
							b|b',
							'data_cols' => 'a|a
							b|b',
						]
					]
				],
				['data_cols', 'data_rows'],
				'saisies_chaine2tableau',
			]
		];
	}

	/**
	 * @dataProvider dataMapper
	 */
	public function testMapper($expected, $provided, $args, $function) {
		$actual = saisies_mapper_option($provided, $args, $function);
		$this->assertEquals($expected, $actual);
	}

	public static function dataTransformerNoms() {
		return  [
			[
				[
					[
						'saisie' => 'input',
						'options' =>
						[
							'nom' => '1_input',
						]
					],
				],
				[
					[
						'saisie' => 'input',
						'options' =>
						[
							'nom' => 'input_1',
						]
					]
				],
				'#input_(\d*)#',
				'$1_input'
			],
		];
	}

	/**
	 * @dataProvider dataTransformerNoms
	 */
	public function testTransformerNoms($expected, $saisies, $masque, $remplacement) {
		$actual = saisies_transformer_noms($saisies, $masque, $remplacement);
		$this->assertEquals($expected, $actual);
	}

	public static function dataTransformerOption() {

		return  [
			[
				[
					[
						'saisie' => 'input',
						'options' =>
						[
							'nom' => '1_input',
						]
					],
				],
				[
					[
						'saisie' => 'input',
						'options' =>
						[
							'nom' => 'input_1',
						]
					]
				],
				'nom',
				'#input_(\d*)#',
				'$1_input'
			],
			[
				[
					[
						'saisie' => 'input',
						'options' =>
						[
							'nom' => 'input_1',
							'conteneur_class' => ' pleine_largeur',
						]
					],
				],
				[
					[
						'saisie' => 'input',
						'options' =>
						[
							'nom' => 'input_1',
						]
					]
				],
				'conteneur_class',
				'#(.*)#',
				'\1 pleine_largeur'
			],
			[
				[
					[
						'saisie' => 'input',
						'options' =>
						[
							'nom' => 'input_1',
							'data' => '',
						]
					],
				],
				[
					[
						'saisie' => 'input',
						'options' =>
						[
							'nom' => 'input_1',
							'data' => ['data']
						]
					]
				],
				'data',
				'#input_(\d*)#',
				'$1_input'
			],
		];
	}

	/**
	 * @dataProvider dataTransformerOption
	 */
	public function testTransformerOption($expected, $saisies, $option, $masque, $remplacement) {
		$actual = saisies_transformer_option($saisies, $option, $masque, $remplacement);
		$this->assertEquals($expected, $actual);
	}


	public static function dataEncapsulerNoms() {
		return [
			'cas_simple' => [
				[
					[
						'saisie' => 'input',
						'options' => [
							'nom' => 'prefixe[bidule]'
						]
					]
				],
				[
					[
						'saisie' => 'input',
						'options' => [
							'nom' => 'bidule'
						]
					]
				],
				'prefixe'
			],
			'cas_compose' => [
				[
					[
						'saisie' => 'input',
						'options' => [
							'nom' => 'prefixe[machin][chose]'
						]
					]
				],
				[
					[
						'saisie' => 'input',
						'options' => [
							'nom' => 'machin[chose]'
						]
					]
				],
				'prefixe'
			],
			'cas_arbo_recursif' => [
				[
					[
						'saisie' => 'fieldset',
						'options' => [
							'nom' => 'prefixe[machin][chose]'
						],
						'saisies' => [
							[
								'saisie' => 'input',
								'options' => [
									'nom' => 'prefixe[machin][bidule]'
								]
							]
						]
					]
				],
				[
					[
						'saisie' => 'fieldset',
						'options' => [
							'nom' => 'machin[chose]'
						],
						'saisies' => [
							[
								'saisie' => 'input',
								'options' => [
									'nom' => 'machin[bidule]'
								]
							]
						]
					]
				],
				'prefixe'
			],
			'cas_arbo_non_recursif_drole_idee' => [
				[
					[
						'saisie' => 'fieldset',
						'options' => [
							'nom' => 'prefixe[machin][chose]'
						],
						'saisies' => [
							[
								'saisie' => 'input',
								'options' => [
									'nom' => 'machin[bidule]'
								]
							]
						]
					]
				],
				[
					[
						'saisie' => 'fieldset',
						'options' => [
							'nom' => 'machin[chose]'
						],
						'saisies' => [
							[
								'saisie' => 'input',
								'options' => [
									'nom' => 'machin[bidule]'
								]
							]
						]
					]
				],
				'prefixe',
				false
			],
		];
	}

	/**
	 * @dataProvider dataEncapsulerNoms
	 */
	public function testEncapsulerNoms($expected, $saisies, $prefixe, $recursif = true) {
		$actual = saisies_encapsuler_noms($saisies, $prefixe, $recursif);
		$this -> assertEquals($expected, $actual);
	}


	public static function dataInserer() {
		return
			[
				// Premier test unitaire : pas de chemin
				[
					[
						[
							'saisie' => 'input',
							'options' =>
							[
								'nom' => 'input_1',
							]
						],
						[
							'saisie' => 'input',
							'options' =>
							[
								'nom' => 'input_a_inserer'
							]
						]
					],
					[
						[
							'saisie' => 'input',
							'options' =>
							[
								'nom' => 'input_1',
							]
						]
					],
					[
						'saisie' => 'input',
						'options' =>
						[
							'nom' => 'input_a_inserer'
						]
					],
				],
				// Deuxième test unitaire : saisies sans fieldset, on insere avant
				[
					[
						[
							'saisie' => 'input',
							'options' =>
							[
								'nom' => 'input_a_inserer'
							]
						],
						[
							'saisie' => 'input',
							'options' =>
							[
								'nom' => 'input_1',
							]
						],
					],
					[
						[
							'saisie' => 'input',
							'options' =>
							[
								'nom' => 'input_1',
							]
						]
					],
					[
						'saisie' => 'input',
						'options' =>
						[
							'nom' => 'input_a_inserer'
						]
					],
					[0]
				],
				// Troisième test unitaire : saisie avec fieldset, on insère en indiquant la saisie devant laquelle inserer
				[
					[
						[
							'saisie' => 'fieldset',
							'options' => ['nom' => 'fieldset'],
							'saisies' => [
								0 =>
								[
									'saisie' => 'input',
									'options' =>
									[
										'nom' => 'input_a_inserer'
									]
								],
								1 =>
								[
									'saisie' => 'input',
									'options' =>
									[
										'nom' => 'input_1',
									]
								],
							]
						],
					],
					[
						[
							'saisie' => 'fieldset',
							'options' => ['nom' => 'fieldset'],
							'saisies' => [
								[
									'saisie' => 'input',
									'options' =>
									[
										'nom' => 'input_1',
									]
								]
							],
						]
					],
					[
						'saisie' => 'input',
						'options' =>
						[
							'nom' => 'input_a_inserer'
						]
					],
					'input_1'
				],
				// Quatrième test unitaire : saisie avec fieldset, on insère en indiquant le fieldset à la fin duquel insérer
				[
					[
						[
							'saisie' => 'fieldset',
							'options' => ['nom' => 'fieldset'],
							'saisies' => [
								0 =>
								[
									'saisie' => 'input',
									'options' =>
									[
										'nom' => 'input_1'
									]
								],
								[
									'saisie' => 'input',
									'options' =>
									[
										'nom' => 'input_a_inserer',
									]
								],
							]
						],
					],
					[
						[
							'saisie' => 'fieldset',
							'options' => ['nom' => 'fieldset'],
							'saisies' => [
								[
									'saisie' => 'input',
									'options' =>
									[
										'nom' => 'input_1',
									]
								]
							],
						]
					],
					[
						'saisie' => 'input',
						'options' =>
						[
							'nom' => 'input_a_inserer'
						]
					],
					'[fieldset]'
				],
				// Cinquième test unitaire : saisie avec fieldset, on insère en indiquant le fieldset et la position au sein du fieldset
				[
					[
						[
							'saisie' => 'fieldset',
							'options' => ['nom' => 'fieldset'],
							'saisies' => [
								0 =>
								[
									'saisie' => 'input',
									'options' =>
									[
										'nom' => 'input_a_inserer',
									]
								],
								1 =>
								[
									'saisie' => 'input',
									'options' =>
									[
										'nom' => 'input_1'
									]
								],
							]
						],
					],
					[
						[
							'saisie' => 'fieldset',
							'options' => ['nom' => 'fieldset'],
							'saisies' => [
								[
									'saisie' => 'input',
									'options' =>
									[
										'nom' => 'input_1',
									]
								]
							],
						]
					],
					[
						'saisie' => 'input',
						'options' =>
						[
							'nom' => 'input_a_inserer'
						]
					],
					'[fieldset][0]'
				],
			];
	}

	/**
	 * @dataProvider dataInserer
	 */
	public function testInserer($expected, $saisies, $inserer, $chemin = []) {
		$actual = saisies_supprimer_identifiants(saisies_inserer($saisies, $inserer, $chemin));
		$this->assertEquals($expected, $actual);
	}



	public static function dataInsererApres() {
		return  [
			'standard' => [
				[
					[
						'saisie' => 'fieldset',
						'options' => ['nom' => 'fieldset'],
						'saisies' => [
							0 =>
							[
								'saisie' => 'input',
								'options' =>
								[
									'nom' => 'input_1',
								]
							],
							1 =>
							[
								'saisie' => 'input',
								'options' =>
								[
									'nom' => 'input_a_inserer'
								]
							],
						]
					],
				],
				[
					[
						'saisie' => 'fieldset',
						'options' => ['nom' => 'fieldset'],
						'saisies' => [
							[
								'saisie' => 'input',
								'options' =>
								[
									'nom' => 'input_1',
								]
							]
						],
					]
				],
				[
					'saisie' => 'input',
					'options' =>
					[
						'nom' => 'input_a_inserer'
					]
				],
				'input_1'
			],
			'pas_de_saisie' => [
				[
					[
						'saisie' => 'fieldset',
						'options' => ['nom' => 'fieldset'],
						'saisies' => [
							0 =>
							[
								'saisie' => 'input',
								'options' =>
								[
									'nom' => 'input_1',
								]
							],
						]
					],
					[
						'saisie' => 'input',
						'options' =>
						[
							'nom' => 'input_a_inserer'
						]
					]
				],
				[
					[
						'saisie' => 'fieldset',
						'options' => ['nom' => 'fieldset'],
						'saisies' => [
							[
								'saisie' => 'input',
								'options' =>
								[
									'nom' => 'input_1',
								]
							]
						],
					]
				],
				[
					'saisie' => 'input',
					'options' =>
					[
						'nom' => 'input_a_inserer'
					]
				],
				'input_inconnu'
			]
		];
	}

	/**
	 * @dataProvider dataInsererApres
	 */
	public function testInsererApres($expected, $saisies, $inserer, $chemin = []) {
		$actual = saisies_supprimer_identifiants(saisies_inserer_apres($saisies, $inserer, $chemin));
		$this->assertEquals($expected, $actual);
	}


	public static function dataInsererAvant() {
		return  [
			'par_chemin' => [
				[
					[
						'saisie' => 'fieldset',
						'options' => ['nom' => 'fieldset'],
						'saisies' => [
							[
								'saisie' => 'input',
								'options' =>
								[
									'nom' => 'input_a_inserer'
								]
							],
							[
								'saisie' => 'input',
								'options' =>
								[
									'nom' => 'input_1',
								]
							]
						]
					],
				],
				[
					[
						'saisie' => 'fieldset',
						'options' => ['nom' => 'fieldset'],
						'saisies' => [
							[
								'saisie' => 'input',
								'options' =>
								[
									'nom' => 'input_1',
								]
							]
						],
					]
				],
				[
					'saisie' => 'input',
					'options' =>
					[
						'nom' => 'input_a_inserer'
					]
				],
				[0,'saisies',0],
			],
			'par_nom' => [
				[
					[
						'saisie' => 'fieldset',
						'options' => ['nom' => 'fieldset'],
						'saisies' => [
							[
								'saisie' => 'input',
								'options' =>
								[
									'nom' => 'input_a_inserer'
								]
							],
							[
								'saisie' => 'input',
								'options' =>
								[
									'nom' => 'input_1',
								]
							]
						]
					],
				],
				[
					[
						'saisie' => 'fieldset',
						'options' => ['nom' => 'fieldset'],
						'saisies' => [
							[
								'saisie' => 'input',
								'options' =>
								[
									'nom' => 'input_1',
								]
							]
						],
					]
				],
				[
					'saisie' => 'input',
					'options' =>
					[
						'nom' => 'input_a_inserer'
					]
				],
				'input_1'
			],
		];
	}

	/**
	 * @dataProvider dataInsererAvant
	 */
	public function testInsererAvant($expected, $saisies, $inserer, $chemin = []) {
		$actual = saisies_supprimer_identifiants(saisies_inserer_avant($saisies, $inserer, $chemin));
		$this->assertEquals($expected, $actual);
	}

	public static function dataInsererSelonChemin() {
		return  [
			'fin_fieldset' => [
				[
					'options' => ['option_general' => 'hop'],
					[
						'saisie' => 'fieldset',
						'options' => ['nom' => 'fieldset'],
						'saisies' => [
							[
								'saisie' => 'input',
								'options' =>
								[
									'nom' => 'input_1',
								]
							],
							[
								'saisie' => 'input',
								'options' =>
								[
									'nom' => 'input_a_inserer'
								]
							]
						]
					],
				],
				[
					'options' => ['option_general' => 'hop'],
					[
						'saisie' => 'fieldset',
						'options' => ['nom' => 'fieldset'],
						'saisies' => [
							[
								'saisie' => 'input',
								'options' =>
								[
									'nom' => 'input_1',
								]
							]
						],
					]
				],
				[
					'saisie' => 'input',
					'options' =>
					[
						'nom' => 'input_a_inserer'
					]
				],
				[0, 'saisies', 1]
			],
			'hors_fieldset' => [
				[
					[
						'saisie' => 'fieldset',
						'options' => ['nom' => 'fieldset'],
						'saisies' => [
							[
								'saisie' => 'input',
								'options' =>
								[
									'nom' => 'input_1',
								]
							]
						]
					],
					[
						'saisie' => 'input',
						'options' =>
						[
							'nom' => 'input_a_inserer'
						]
					],
				],
				[
					[
						'saisie' => 'fieldset',
						'options' => ['nom' => 'fieldset'],
						'saisies' => [
							[
								'saisie' => 'input',
								'options' =>
								[
									'nom' => 'input_1',
								]
							]
						],
					]
				],
				[
					'saisie' => 'input',
					'options' =>
					[
						'nom' => 'input_a_inserer'
					]
				],
				[1]
			],
			'debut_fieldset' => [
				[
					[
						'saisie' => 'fieldset',
						'options' => ['nom' => 'fieldset'],
						'saisies' => [
							[
								'saisie' => 'input',
								'options' =>
								[
									'nom' => 'input_a_inserer'
								]
							],
							[
								'saisie' => 'input',
								'options' =>
								[
									'nom' => 'input_1',
								]
							]
						]
					],
				],
				[
					[
						'saisie' => 'fieldset',
						'options' => ['nom' => 'fieldset'],
						'saisies' => [
							[
								'saisie' => 'input',
								'options' =>
								[
									'nom' => 'input_1',
								]
							]
						],
					]
				],
				[
					'saisie' => 'input',
					'options' =>
					[
						'nom' => 'input_a_inserer'
					]
				],
				[0, 'saisies', 0]
			],
			'position_negative' => [
				[
					[
						'saisie' => 'fieldset',
						'options' => ['nom' => 'fieldset'],
						'saisies' => [
							[
								'saisie' => 'input',
								'options' =>
								[
									'nom' => 'input_a_inserer'
								]
							],
							[
								'saisie' => 'input',
								'options' =>
								[
									'nom' => 'input_1',
								]
							]
						]
					],
				],
				[
					[
						'saisie' => 'fieldset',
						'options' => ['nom' => 'fieldset'],
						'saisies' => [
							[
								'saisie' => 'input',
								'options' =>
								[
									'nom' => 'input_1',
								]
							]
						],
					]
				],
				[
					'saisie' => 'input',
					'options' =>
					[
						'nom' => 'input_a_inserer'
					]
				],
				[0, 'saisies', -2]
			],
			'debut_fieldset_vide' => [
				[
					[
						'saisie' => 'fieldset',
						'options' => ['nom' => 'fieldset'],
						'saisies' => [
							[
								'saisie' => 'input',
								'options' =>
								[
									'nom' => 'input_a_inserer'
								]
							],
						]
					],
				],
				[
					[
						'saisie' => 'fieldset',
						'options' => ['nom' => 'fieldset'],
					]
				],
				[
					'saisie' => 'input',
					'options' =>
					[
						'nom' => 'input_a_inserer'
					]
				],
				[0, 'saisies', 0]
			]
		];
	}

	/**
	 * @dataProvider dataInsererSelonChemin
	 */
	public function testInsererSelonChemin($expected, $saisies, $inserer, $chemin = []) {
		$actual = saisies_supprimer_identifiants(saisies_inserer_selon_chemin($saisies, $inserer, $chemin));
		$this->assertEquals($expected, $actual);
	}


	public static function dataDupliquer() {
		return  [
			'avec_label' => [
				// Sortie attendue
				[
					[
						'saisie' => 'fieldset',
						'options' => [
							'nom' => 'fieldset',
							'label' => 'fieldset',
						],
						'saisies' => [
							[
								'saisie' => 'input',
								'options' => [
									'nom' => 'input',
									'label' => 'input',
								],
							],
							[
								'saisie' => 'input',
								'options' => [
									'nom' => 'input_1',
									'label' => 'input saisies:construire_action_dupliquer_copie',
								],
							]
						]
					]
				],
				// Entrée
				[
					['saisie' => 'fieldset',
					'options' => [
						'nom' => 'fieldset',
						'label' => 'fieldset',
					],
					'saisies' => [
						[
							'saisie' => 'input',
							'options' => [
								'nom' => 'input',
								'label' => 'input',
							],
						]
					]
					]
				],
				// Saisie à dupliquer
				'input'
			],
			'sans_label' => [
				// Sortie attendue
				[
					[
						'saisie' => 'fieldset',
						'options' => [
							'nom' => 'fieldset',
							'label' => 'fieldset',
						],
						'saisies' => [
							[
								'saisie' => 'input',
								'options' => [
									'nom' => 'input',
								],
							],
							[
								'saisie' => 'input',
								'options' => [
									'nom' => 'input_1',
								],
							]
						]
					]
				],
				// Entrée
				[
					['saisie' => 'fieldset',
					'options' => [
						'nom' => 'fieldset',
						'label' => 'fieldset',
					],
					'saisies' => [
						[
							'saisie' => 'input',
							'options' => [
								'nom' => 'input',
							],
						]
					]
					]
				],
				// Saisie à dupliquer
				'input'
			]
		];
	}

	/**
	 * @dataProvider dataDupliquer
	 */
	public function testDupliquer($expected, $saisies, $inserer, $chemin = []) {
		$actual = saisies_supprimer_identifiants(saisies_dupliquer($saisies, $inserer, $chemin));
		$this->assertEquals($expected, $actual);
	}


	public static function dataFieldsetsEnOnglets() {
		return  [
			'horizontal' => [
				// Expected
				[
					[
						'saisie' => 'fieldset',
						'options' => [
							'onglet' => 'on',
							'nom' => '1',
						],
						'identifiant' => '_1'
					],
					[
						'saisie' => 'fieldset',
						'options' => [
							'onglet' => 'on',
							'nom' => '2',
						],
						'identifiant' => '_2'
					],
					[
						'saisie' => 'input',
						'options' => [
							'nom' => '3'
						]
					]
				],
				// Provided
				[
					[
						'saisie' => 'fieldset',
						'options' => [
							'nom' => '1'
						]
					],
					[
						'saisie' => 'fieldset',
						'options' => [
							'nom' => '2',
						]
					],
					[
						'saisie' => 'input',
						'options' => [
							'nom' => '3',
						]
					]
				]
			],
			'vertical' => [
				// Expected
				[
					[
						'saisie' => 'fieldset',
						'options' => [
							'onglet' => 'on',
							'onglet_vertical' => 'on',
							'nom' => '1',
						],
						'identifiant' => '_1'
					],
					[
						'saisie' => 'fieldset',
						'options' => [
							'onglet' => 'on',
							'onglet_vertical' => 'on',
							'nom' => '2',
						],
						'identifiant' => '_2'
					],
					[
						'saisie' => 'input',
						'options' => [
							'nom' => '3'
						]
					]
				],
				// Provided
				[
					[
						'saisie' => 'fieldset',
						'options' => [
							'nom' => '1'
						]
					],
					[
						'saisie' => 'fieldset',
						'options' => [
							'nom' => '2',
						]
					],
					[
						'saisie' => 'input',
						'options' => [
							'nom' => '3',
						]
					]
				],
				'',
				true
			]
		];
	}

	/**
	 * @dataProvider dataFieldsetsEnOnglets
	 */
	public function testFieldsetsEnOnglets($expected, $saisies, $identifiant_prefixe = '', $vertical = false) {
		$actual = saisies_fieldsets_en_onglets($saisies, $identifiant_prefixe, $vertical);
		$this->assertEquals($expected, $actual);
	}

	public static function dataSansReponse() {
		set_request('ca_disparait', '');
		set_request('ca_reste', 'chewingum');
		set_request('ca_reste_malgres_tout', 0);
		set_request('input_dans_fieldset_qui_disparait', '');
		set_request('input_dans_fieldset_qui_reste', 'on reste');
		set_request('date_vide', '0000-00-00 00:00:00');
		$GLOBALS['_FILES']['fichier_qui_reste'] = [
			'name' => 'toto.jpg',
			'type' => 'image/jpg',
			'tmp_name' => 'xyz.jpg',
			'size' => 222,
			'error' => 0
		];
		return [
			'test1' =>
			[
				//Sortie
				[
					[
						'saisie' => 'input',
						'options' =>
						[
							'nom' => 'ca_reste',
						]
					],
					[
						'saisie' => 'fichiers',
						'options' =>
						[
							'nom' => 'fichier_qui_reste',
						]
					],
					[
						'saisie' => 'input',
						'options' =>
						[
							'nom' => 'ca_reste_malgres_tout',
						]
					],
					[
						'saisie' => 'fieldset',
						'options' => [
							'nom' => 'fieldset_qui_reste'
						],
						'saisies' => [
							[
								'saisie' => 'input',
								'options' => [
									'nom' => 'input_dans_fieldset_qui_reste'
								]
							]
						]
					],
					'options' => [
						'option_globale' => 'toto'
					]
				],
				// Entrée
				[
					[
						'saisie' => 'input',
						'options' =>
						[
							'nom' => 'ca_disparait',
						]
					],
					[
						'saisie' => 'input',
						'options' =>
						[
							'nom' => 'ca_disparait',
						]
					],
					[
						'saisie' => 'input',
						'options' =>
						[
							'nom' => 'ca_reste',
						]
					],
					[
						'saisie' => 'fichiers',
						'options' =>
						[
							'nom' => 'fichier_qui_reste',
						]
					],
					[
						'saisie' => 'input',
						'options' =>
						[
							'nom' => 'ca_reste_malgres_tout',
						]
					],
					[
						'saisie' => 'fieldset',
						'options' => [
							'nom' => 'fieldset_qui_reste'
						],
						'saisies' => [
							[
								'saisie' => 'input',
								'options' => [
									'nom' => 'input_dans_fieldset_qui_reste'
								]
							]
						]
					],
					[
						'saisie' => 'fieldset',
						'options' => [
							'nom' => 'fieldset_qui_disparait'
						],
						'saisies' => [
							[
								'saisie' => 'input',
								'options' => [
									'nom' => 'input_dans_fieldset_qui_disparait'
								]
							]
						]
					],
					'options' => [
						'option_globale' => 'toto'
					]
				],
			]
		];
	}

	/**
	 * @dataProvider dataSansReponse
	 * @uses saisies_saisie_est_fichier()
	 */
	public function testSansReponse($expected, $provided, $tableau = []) {
		$actual = saisies_supprimer_sans_reponse($provided, $tableau);
		$this->assertEquals($expected, $actual);
	}

	public static function dataDeplacer() {
		return  [
			// Premier test unitaire : pas de chemin
			[
				[
					[
						'saisie' => 'input',
						'options' =>
						[
							'nom' => 'input_a_deplacer'
						]
					],
					[
						'saisie' => 'input',
						'options' =>
						[
							'nom' => 'input_1',
						]
					],
				],
				[
					[
						'saisie' => 'input',
						'options' =>
						[
							'nom' => 'input_1',
						]
					],
					[
						'saisie' => 'input',
						'options' =>
						[
							'nom' => 'input_a_deplacer'
						]
					]
				],
				'input_a_deplacer',
				'input_1',
			],
			// Deuxième test unitaire : saisie avec fieldset, on déplace en indiquant la saisie devant laquelle deplacer
			[
				[
					[
						'saisie' => 'fieldset',
						'options' => ['nom' => 'fieldset'],
						'saisies' => [
							[
								'saisie' => 'input',
								'options' =>
								[
									'nom' => 'input_a_deplacer'
								]
							],
							[
								'saisie' => 'input',
								'options' =>
								[
									'nom' => 'input_1',
								]
							],
						]
					],
				],
				[
					[
						'saisie' => 'fieldset',
						'options' => ['nom' => 'fieldset'],
						'saisies' => [
							[
								'saisie' => 'input',
								'options' =>
								[
									'nom' => 'input_1',
								]
							]
						],
					],
					[
						'saisie' => 'input',
						'options' =>
						[
							'nom' => 'input_a_deplacer'
						]
					],
				],
				'input_a_deplacer',
				'input_1'
			],
			// Troisième test unitaire : saisie avec fieldset, on insère en indiquant le fieldset à la fin duquel insérer
			[
				[
					[
						'saisie' => 'fieldset',
						'options' => ['nom' => 'fieldset'],
						'saisies' => [
							[
								'saisie' => 'input',
								'options' =>
								[
									'nom' => 'input_1'
								]
							],
							[
								'saisie' => 'input',
								'options' =>
								[
									'nom' => 'input_a_deplacer',
								]
							],
						]
					],
				],
				[
					[
						'saisie' => 'fieldset',
						'options' => ['nom' => 'fieldset'],
						'saisies' => [
							[
								'saisie' => 'input',
								'options' =>
								[
									'nom' => 'input_1',
								]
							]
						],
					],
					[
						'saisie' => 'input',
						'options' =>
						[
							'nom' => 'input_a_deplacer'
						]
					],
				],
				'input_a_deplacer',
				'[fieldset]'
			],
			// Quatrième test unitaire : saisie avec fieldset, on déplace en indiquant le fieldset et la position au sein du fieldset
			[
				[
					[
						'saisie' => 'fieldset',
						'options' => ['nom' => 'fieldset'],
						'saisies' => [
							[
								'saisie' => 'input',
								'options' =>
								[
									'nom' => 'input_a_deplacer',
								]
							],
							[
								'saisie' => 'input',
								'options' =>
								[
									'nom' => 'input_1'
								]
							],
						]
					],
				],
				[
					[
						'saisie' => 'fieldset',
						'options' => ['nom' => 'fieldset'],
						'saisies' => [
							[
								'saisie' => 'input',
								'options' =>
								[
									'nom' => 'input_1',
								]
							]
						],
					],
					[
						'saisie' => 'input',
						'options' =>
						[
							'nom' => 'input_a_deplacer'
						]
					],
				],
				'input_a_deplacer',
				'[fieldset][0]'
			],
			// 4eme test: on se trompe dans le mode de déplacement -> on reste tel quel
			[
				[
					[
						'saisie' => 'input',
						'options' =>
						[
							'nom' => 'input_a_deplacer'
						]
					],
					[
						'saisie' => 'input',
						'options' =>
						[
							'nom' => 'input_1',
						]
					],
				],
				[
					[
						'saisie' => 'input',
						'options' =>
						[
							'nom' => 'input_a_deplacer'
						]
					],
					[
						'saisie' => 'input',
						'options' =>
						[
							'nom' => 'input_1',
						]
					],
				],
				'input_a_deplacer',
				'input_1',
				'nope',
			],
			// 5eme test: on ne précise pas où -> tout à la fin
			[
				[
					[
						'saisie' => 'input',
						'options' =>
						[
							'nom' => 'input_1',
						]
					],
					[
						'saisie' => 'input',
						'options' =>
						[
							'nom' => 'input_2',
						]
					],
					[
						'saisie' => 'input',
						'options' =>
						[
							'nom' => 'input_a_deplacer'
						]
					],
				],
				[
					[
						'saisie' => 'input',
						'options' =>
						[
							'nom' => 'input_a_deplacer'
						]
					],
					[
						'saisie' => 'input',
						'options' =>
						[
							'nom' => 'input_1',
						]
					],
					[
						'saisie' => 'input',
						'options' =>
						[
							'nom' => 'input_2',
						]
					],
				],
				'input_a_deplacer',
			],
			// 6eme test : $ou a des crochets, mais vides
			[
				[
					[
						'saisie' => 'input',
						'options' =>
						[
							'nom' => 'input_1',
						]
					],
					[
						'saisie' => 'input',
						'options' =>
						[
							'nom' => 'input_2',
						]
					],
					[
						'saisie' => 'input',
						'options' =>
						[
							'nom' => 'input_a_deplacer'
						]
					],
				],
				[
					[
						'saisie' => 'input',
						'options' =>
						[
							'nom' => 'input_a_deplacer'
						]
					],
					[
						'saisie' => 'input',
						'options' =>
						[
							'nom' => 'input_1',
						]
					],
					[
						'saisie' => 'input',
						'options' =>
						[
							'nom' => 'input_2',
						]
					],
				],
				'input_a_deplacer',
				'[]'
			],
			// 7eme test: on se trompe dans la destination -> on reste tel quel
			[
				[
					[
						'saisie' => 'input',
						'options' =>
						[
							'nom' => 'input_a_deplacer'
						]
					],
					[
						'saisie' => 'input',
						'options' =>
						[
							'nom' => 'input_1',
						]
					],
				],
				[
					[
						'saisie' => 'input',
						'options' =>
						[
							'nom' => 'input_a_deplacer'
						]
					],
					[
						'saisie' => 'input',
						'options' =>
						[
							'nom' => 'input_1',
						]
					],
				],
				'input_a_deplacer',
				'input_inexistant',
			]
		];
	}

	/**
	 * @dataProvider dataDeplacer
	 */
	public function testDeplacer($expected, $saisies, $chemin = '', $ou = '', $avant_ou_apres = 'avant') {
		$actual = saisies_supprimer_identifiants(saisies_deplacer($saisies, $chemin, $ou, $avant_ou_apres));
		$this->assertEquals($expected, $actual);
	}

	public static function dataDeplacerApres() {

		return  [
			// Premier test unitaire : pas de chemin
			[
				[
					[
						'saisie' => 'input',
						'options' =>
						[
							'nom' => 'input_1',
						]
					],
					[
						'saisie' => 'input',
						'options' =>
						[
							'nom' => 'input_a_deplacer'
						]
					],
				],
				[
					[
						'saisie' => 'input',
						'options' =>
						[
							'nom' => 'input_a_deplacer'
						]
					],
					[
						'saisie' => 'input',
						'options' =>
						[
							'nom' => 'input_1',
						]
					]
				],
				'input_a_deplacer',
				'input_1',
			],
			// Deuxième test unitaire : saisie avec fieldset, on déplace en indiquant la saisie après laquelle deplacer
			[
				[
					[
						'saisie' => 'fieldset',
						'options' => ['nom' => 'fieldset'],
						'saisies' => [
							[
								'saisie' => 'input',
								'options' =>
								[
									'nom' => 'input_1',
								]
							],
							[
								'saisie' => 'input',
								'options' =>
								[
									'nom' => 'input_a_deplacer'
								]
							],
						]
					],
				],
				[
					[
						'saisie' => 'fieldset',
						'options' => ['nom' => 'fieldset'],
						'saisies' => [
							[
								'saisie' => 'input',
								'options' =>
								[
									'nom' => 'input_1',
								]
							]
						],
					],
					[
						'saisie' => 'input',
						'options' =>
						[
							'nom' => 'input_a_deplacer'
						]
					],
				],
				'input_a_deplacer',
				'input_1'
			],
			// Troisième test unitaire : saisie avec fieldset, on insère apres le fieldset
			[
				[
					[
						'saisie' => 'fieldset',
						'options' => ['nom' => 'fieldset'],
						'saisies' => [
							[
								'saisie' => 'input',
								'options' =>
								[
									'nom' => 'input_1'
								]
							],
						]
					],
					[
						'saisie' => 'input',
						'options' =>
						[
							'nom' => 'input_a_deplacer',
						]
					],
				],
				[
					[
						'saisie' => 'fieldset',
						'options' => ['nom' => 'fieldset'],
						'saisies' => [
							[
								'saisie' => 'input',
								'options' =>
								[
									'nom' => 'input_1',
								]
							]
						],
					],
					[
						'saisie' => 'input',
						'options' =>
						[
							'nom' => 'input_a_deplacer'
						]
					],
				],
				'input_a_deplacer',
				'fieldset'
			],
			// Quatrième test unitaire : saisie avec fieldset, on déplace en indiquant le fieldset et la position au sein du fieldset (on insère après cette position)
			[
				[
					[
						'saisie' => 'fieldset',
						'options' => ['nom' => 'fieldset'],
						'saisies' => [
							[
								'saisie' => 'input',
								'options' =>
								[
									'nom' => 'input_1'
								]
							],
							[
								'saisie' => 'input',
								'options' =>
								[
									'nom' => 'input_2'
								]
							],
							[
								'saisie' => 'input',
								'options' =>
								[
									'nom' => 'input_a_deplacer',
								]
							],
							[
								'saisie' => 'input',
								'options' =>
								[
									'nom' => 'input_3'
								]
							],
						]
					],
				],
				[
					[
						'saisie' => 'fieldset',
						'options' => ['nom' => 'fieldset'],
						'saisies' => [
							[
								'saisie' => 'input',
								'options' =>
								[
									'nom' => 'input_1',
								],
							],
							[
								'saisie' => 'input',
								'options' =>
								[
									'nom' => 'input_2',
								],
							],
							[
								'saisie' => 'input',
								'options' =>
								[
									'nom' => 'input_3',
								],
							],
						]
					],
					[
						'saisie' => 'input',
						'options' =>
						[
							'nom' => 'input_a_deplacer'
						]
					],
				],
				'input_a_deplacer',
				'[fieldset][1]'
			],
			//Plein de tests inspirés des retours de tcharlss en https://git.spip.net/spip-contrib-extensions/saisies/pulls/156
			[
				[
					[
						'saisie' => 'input',
						'options' =>
						[
							'nom' => 'racine1',
							'label' => 'Racine 1',
						],
					],
					[
						'saisie' => 'input',
						'options' =>
						[
							'nom' => 'cible',
							'label' => 'Saisie déplacée',
							'conteneur_class' => 'highlight',
						],
					],
					[
						'saisie' => 'fieldset',
						'options' =>
						[
							'nom' => 'parent',
						],
						'saisies' =>
						[
							[
								'saisie' => 'input',
								'options' =>
								[
									'nom' => 'enfant1',
									'label' => 'Enfant 1',
								],
							],
							[
								'saisie' => 'input',
								'options' =>
								[
									'nom' => 'enfant2',
									'label' => 'Enfant 2',
								],
							],
						],
					],
					[
						'saisie' => 'input',
						'options' =>
						[
							'nom' => 'racine2',
							'label' => 'Racine 2',
						],
					],
				],
				[
					[
						'saisie' => 'input',
						'options' =>
						[
							'nom' => 'racine1',
							'label' => 'Racine 1',
						],
					],
					[
						'saisie' => 'fieldset',
						'options' =>
						[
							'nom' => 'parent',
						],
						'saisies' =>
						[
							[
								'saisie' => 'input',
								'options' =>
								[
									'nom' => 'enfant1',
									'label' => 'Enfant 1',
								],
							],
							[
								'saisie' => 'input',
								'options' =>
								[
									'nom' => 'enfant2',
									'label' => 'Enfant 2',
								],
							],
						],
					],
					[
						'saisie' => 'input',
						'options' =>
						[
							'nom' => 'racine2',
							'label' => 'Racine 2',
						],
					],
					[
						'saisie' => 'input',
						'options' =>
						[
							'nom' => 'cible',
							'label' => 'Saisie déplacée',
							'conteneur_class' => 'highlight',
						],
					],
				],
				'cible',
				'racine1',
			],
			[
				[
					[
						'saisie' => 'input',
						'options' =>
						[
							'nom' => 'racine1',
							'label' => 'Racine 1',
						],
					],
					[
						'saisie' => 'fieldset',
						'options' =>
						[
							'nom' => 'parent',
						],
						'saisies' =>
						[
							[
								'saisie' => 'input',
								'options' =>
								[
									'nom' => 'enfant1',
									'label' => 'Enfant 1',
								],
							],
							[
								'saisie' => 'input',
								'options' =>
								[
									'nom' => 'cible',
									'label' => 'Saisie déplacée',
									'conteneur_class' => 'highlight',
								],
							],
							[
								'saisie' => 'input',
								'options' =>
								[
									'nom' => 'enfant2',
									'label' => 'Enfant 2',
								],
							],
						],
					],
					[
						'saisie' => 'input',
						'options' =>
						[
							'nom' => 'racine2',
							'label' => 'Racine 2',
						],
					],
				],
				[
					[
						'saisie' => 'input',
						'options' =>
						[
							'nom' => 'racine1',
							'label' => 'Racine 1',
						],
					],
					[
						'saisie' => 'fieldset',
						'options' =>
						[
							'nom' => 'parent',
						],
						'saisies' =>
						[
							[
								'saisie' => 'input',
								'options' =>
								[
									'nom' => 'enfant1',
									'label' => 'Enfant 1',
								],
							],
							[
								'saisie' => 'input',
								'options' =>
								[
									'nom' => 'enfant2',
									'label' => 'Enfant 2',
								],
							],
						],
					],
					[
						'saisie' => 'input',
						'options' =>
						[
							'nom' => 'racine2',
							'label' => 'Racine 2',
						],
					],
					[
						'saisie' => 'input',
						'options' =>
						[
							'nom' => 'cible',
							'label' => 'Saisie déplacée',
							'conteneur_class' => 'highlight',
						],
					],
				],
				'cible',
				'enfant1',
			],
			[
				[
					[
						'saisie' => 'input',
						'options' =>
						[
							'nom' => 'racine1',
							'label' => 'Racine 1',
						],
					],
					[
						'saisie' => 'fieldset',
						'options' =>
						[
							'nom' => 'parent',
						],
						'saisies' =>
						[
							[
								'saisie' => 'input',
								'options' =>
								[
									'nom' => 'enfant1',
									'label' => 'Enfant 1',
								],
							],
							[
								'saisie' => 'input',
								'options' =>
								[
									'nom' => 'enfant2',
									'label' => 'Enfant 2',
								],
							],
							[
								'saisie' => 'input',
								'options' =>
								[
									'nom' => 'cible',
									'label' => 'Saisie déplacée',
									'conteneur_class' => 'highlight',
								],
							],
						],
					],
					[
						'saisie' => 'input',
						'options' =>
						[
							'nom' => 'racine2',
							'label' => 'Racine 2',
						],
					],
				],
				[
					[
						'saisie' => 'input',
						'options' =>
						[
							'nom' => 'racine1',
							'label' => 'Racine 1',
						],
					],
					[
						'saisie' => 'fieldset',
						'options' =>
						[
							'nom' => 'parent',
						],
						'saisies' =>
						[
							[
								'saisie' => 'input',
								'options' =>
								[
									'nom' => 'enfant1',
									'label' => 'Enfant 1',
								],
							],
							[
								'saisie' => 'input',
								'options' =>
								[
									'nom' => 'enfant2',
									'label' => 'Enfant 2',
								],
							],
						],
					],
					[
						'saisie' => 'input',
						'options' =>
						[
							'nom' => 'racine2',
							'label' => 'Racine 2',
						],
					],
					[
						'saisie' => 'input',
						'options' =>
						[
							'nom' => 'cible',
							'label' => 'Saisie déplacée',
							'conteneur_class' => 'highlight',
						],
					],
				],
				'cible',
				'enfant2',
			],
			[
				[
					[
						'saisie' => 'input',
						'options' =>
						[
							'nom' => 'racine1',
							'label' => 'Racine 1',
						],
					],
					[
						'saisie' => 'fieldset',
						'options' =>
						[
							'nom' => 'parent',
						],
						'saisies' =>
						[
							[
								'saisie' => 'input',
								'options' =>
								[
									'nom' => 'enfant1',
									'label' => 'Enfant 1',
								],
							],
							[
								'saisie' => 'input',
								'options' =>
								[
									'nom' => 'enfant2',
									'label' => 'Enfant 2',
								],
							],
						],
					],
					[
						'saisie' => 'input',
						'options' =>
						[
							'nom' => 'cible',
							'label' => 'Saisie déplacée',
							'conteneur_class' => 'highlight',
						],
					],
					[
						'saisie' => 'input',
						'options' =>
						[
							'nom' => 'racine2',
							'label' => 'Racine 2',
						],
					],
				],
				[
					[
						'saisie' => 'input',
						'options' =>
						[
							'nom' => 'racine1',
							'label' => 'Racine 1',
						],
					],
					[
						'saisie' => 'fieldset',
						'options' =>
						[
							'nom' => 'parent',
						],
						'saisies' =>
						[
							[
								'saisie' => 'input',
								'options' =>
								[
									'nom' => 'enfant1',
									'label' => 'Enfant 1',
								],
							],
							[
								'saisie' => 'input',
								'options' =>
								[
									'nom' => 'enfant2',
									'label' => 'Enfant 2',
								],
							],
						],
					],
					[
						'saisie' => 'input',
						'options' =>
						[
							'nom' => 'racine2',
							'label' => 'Racine 2',
						],
					],
					[
						'saisie' => 'input',
						'options' =>
						[
							'nom' => 'cible',
							'label' => 'Saisie déplacée',
							'conteneur_class' => 'highlight',
						],
					],
				],
				'cible',
				'parent',
			],
			[
				[
					[
						'saisie' => 'input',
						'options' =>
						[
							'nom' => 'racine1',
							'label' => 'Racine 1',
						],
					],
					[
						'saisie' => 'fieldset',
						'options' =>
						[
							'nom' => 'parent',
						],
						'saisies' =>
						[
							[
								'saisie' => 'input',
								'options' =>
								[
									'nom' => 'enfant1',
									'label' => 'Enfant 1',
								],
							],
							[
								'saisie' => 'input',
								'options' =>
								[
									'nom' => 'enfant2',
									'label' => 'Enfant 2',
								],
							],
						],
					],
					[
						'saisie' => 'input',
						'options' =>
						[
							'nom' => 'racine2',
							'label' => 'Racine 2',
						],
					],
					[
						'saisie' => 'input',
						'options' =>
						[
							'nom' => 'cible',
							'label' => 'Saisie déplacée',
							'conteneur_class' => 'highlight',
						],
					],
				],
				[
					[
						'saisie' => 'input',
						'options' =>
						[
							'nom' => 'racine1',
							'label' => 'Racine 1',
						],
					],
					[
						'saisie' => 'fieldset',
						'options' =>
						[
							'nom' => 'parent',
						],
						'saisies' =>
						[
							[
								'saisie' => 'input',
								'options' =>
								[
									'nom' => 'enfant1',
									'label' => 'Enfant 1',
								],
							],
							[
								'saisie' => 'input',
								'options' =>
								[
									'nom' => 'enfant2',
									'label' => 'Enfant 2',
								],
							],
						],
					],
					[
						'saisie' => 'input',
						'options' =>
						[
							'nom' => 'racine2',
							'label' => 'Racine 2',
						],
					],
					[
						'saisie' => 'input',
						'options' =>
						[
							'nom' => 'cible',
							'label' => 'Saisie déplacée',
							'conteneur_class' => 'highlight',
						],
					],
				],
				'cible',
				'racine2',
			],
			[
				[
					[
						'saisie' => 'input',
						'options' =>
						[
							'nom' => 'racine1',
							'label' => 'Racine 1',
						],
					],
					[
						'saisie' => 'fieldset',
						'options' =>
						[
							'nom' => 'parent',
						],
						'saisies' =>
						[
							[
								'saisie' => 'input',
								'options' =>
								[
									'nom' => 'enfant1',
									'label' => 'Enfant 1',
								],
							],
							[
								'saisie' => 'input',
								'options' =>
								[
									'nom' => 'cible',
									'label' => 'Saisie déplacée',
									'conteneur_class' => 'highlight',
								],
							],
							[
								'saisie' => 'input',
								'options' =>
								[
									'nom' => 'enfant2',
									'label' => 'Enfant 2',
								],
							],
						],
					],
					[
						'saisie' => 'input',
						'options' =>
						[
							'nom' => 'racine2',
							'label' => 'Racine 2',
						],
					],
				],
				[
					[
						'saisie' => 'input',
						'options' =>
						[
							'nom' => 'racine1',
							'label' => 'Racine 1',
						],
					],
					[
						'saisie' => 'fieldset',
						'options' =>
						[
							'nom' => 'parent',
						],
						'saisies' =>
						[
							[
								'saisie' => 'input',
								'options' =>
								[
									'nom' => 'enfant1',
									'label' => 'Enfant 1',
								],
							],
							[
								'saisie' => 'input',
								'options' =>
								[
									'nom' => 'enfant2',
									'label' => 'Enfant 2',
								],
							],
						],
					],
					[
						'saisie' => 'input',
						'options' =>
						[
							'nom' => 'racine2',
							'label' => 'Racine 2',
						],
					],
					[
						'saisie' => 'input',
						'options' =>
						[
							'nom' => 'cible',
							'label' => 'Saisie déplacée',
							'conteneur_class' => 'highlight',
						],
					],
				],
				'cible',
				'[parent][0]',
			],
			[
				[
					[
						'saisie' => 'input',
						'options' =>
						[
							'nom' => 'racine1',
							'label' => 'Racine 1',
						],
					],
					[
						'saisie' => 'fieldset',
						'options' =>
						[
							'nom' => 'parent',
						],
						'saisies' =>
						[
							[
								'saisie' => 'input',
								'options' =>
								[
									'nom' => 'enfant1',
									'label' => 'Enfant 1',
								],
							],
							[
								'saisie' => 'input',
								'options' =>
								[
									'nom' => 'enfant2',
									'label' => 'Enfant 2',
								],
							],
							[
								'saisie' => 'input',
								'options' =>
								[
									'nom' => 'cible',
									'label' => 'Saisie déplacée',
									'conteneur_class' => 'highlight',
								],
							],
						],
					],
					[
						'saisie' => 'input',
						'options' =>
						[
							'nom' => 'racine2',
							'label' => 'Racine 2',
						],
					],
				],
				[
					[
						'saisie' => 'input',
						'options' =>
						[
							'nom' => 'racine1',
							'label' => 'Racine 1',
						],
					],
					[
						'saisie' => 'fieldset',
						'options' =>
						[
							'nom' => 'parent',
						],
						'saisies' =>
						[
							[
								'saisie' => 'input',
								'options' =>
								[
									'nom' => 'enfant1',
									'label' => 'Enfant 1',
								],
							],
							[
								'saisie' => 'input',
								'options' =>
								[
									'nom' => 'enfant2',
									'label' => 'Enfant 2',
								],
							],
						],
					],
					[
						'saisie' => 'input',
						'options' =>
						[
							'nom' => 'racine2',
							'label' => 'Racine 2',
						],
					],
					[
						'saisie' => 'input',
						'options' =>
						[
							'nom' => 'cible',
							'label' => 'Saisie déplacée',
							'conteneur_class' => 'highlight',
						],
					],
				],
				'cible',
				'[parent][1]',
			]
		];
	}

	/**
	 * @dataProvider dataDeplacerApres
	 */
	public function testDeplacerApres($expected, $saisies, $chemin, $ou = '') {
		$actual = saisies_supprimer_identifiants(saisies_deplacer_apres($saisies, $chemin, $ou));
		$this->assertEquals($expected, $actual);
	}



	public static function dataDeplacerAvant() {

		return  [
			// Premier test unitaire : pas de chemin
			[
				[
					[
						'saisie' => 'input',
						'options' =>
						[
							'nom' => 'input_a_deplacer'
						]
					],
					[
						'saisie' => 'input',
						'options' =>
						[
							'nom' => 'input_1',
						]
					],
				],
				[
					[
						'saisie' => 'input',
						'options' =>
						[
							'nom' => 'input_1',
						]
					],

					[
						'saisie' => 'input',
						'options' =>
						[
							'nom' => 'input_a_deplacer'
						]
					]
				],
				'input_a_deplacer',
				'input_1',
			],
			// Deuxième test unitaire : saisie avec fieldset, on déplace en indiquant la saisie devant laquelle deplacer
			[
				[
					[
						'saisie' => 'fieldset',
						'options' => ['nom' => 'fieldset'],
						'saisies' => [
							[
								'saisie' => 'input',
								'options' =>
								[
									'nom' => 'input_a_deplacer'
								]
							],
							[
								'saisie' => 'input',
								'options' =>
								[
									'nom' => 'input_1',
								]
							],
						]
					],
				],
				[
					[
						'saisie' => 'fieldset',
						'options' => ['nom' => 'fieldset'],
						'saisies' => [
							[
								'saisie' => 'input',
								'options' =>
								[
									'nom' => 'input_1',
								]
							]
						],
					],
					[
						'saisie' => 'input',
						'options' =>
						[
							'nom' => 'input_a_deplacer'
						]
					],
				],
				'input_a_deplacer',
				'input_1'
			],
			// Troisième test unitaire : saisie avec fieldset, on insère avant le fieldset
			[
				[
					[
						'saisie' => 'input',
						'options' =>
						[
							'nom' => 'input_a_deplacer',
						]
					],
					[
						'saisie' => 'fieldset',
						'options' => ['nom' => 'fieldset'],
						'saisies' => [
							0 =>
							[
								'saisie' => 'input',
								'options' =>
								[
									'nom' => 'input_1'
								]
							],
						]
					],
				],
				[
					[
						'saisie' => 'fieldset',
						'options' => ['nom' => 'fieldset'],
						'saisies' => [
							[
								'saisie' => 'input',
								'options' =>
								[
									'nom' => 'input_1',
								]
							]
						],
					],
					[
						'saisie' => 'input',
						'options' =>
						[
							'nom' => 'input_a_deplacer'
						]
					],
				],
				'input_a_deplacer',
				'fieldset'
			],
			// Quatrième test unitaire : saisie avec fieldset, on déplace en indiquant le fieldset et la position au sein du fieldset
			[
				[
					[
						'saisie' => 'fieldset',
						'options' => ['nom' => 'fieldset'],
						'saisies' => [
							0 =>
							[
								'saisie' => 'input',
								'options' =>
								[
									'nom' => 'input_1'
								]
							],
							[
								'saisie' => 'input',
								'options' =>
								[
									'nom' => 'input_a_deplacer',
								]
							],
							[
								'saisie' => 'input',
								'options' =>
								[
									'nom' => 'input_2'
								]
							],
						]
					],
				],
				[
					[
						'saisie' => 'fieldset',
						'options' => ['nom' => 'fieldset'],
						'saisies' => [
							[
								'saisie' => 'input',
								'options' =>
								[
									'nom' => 'input_1',
								],
							],
							[
								'saisie' => 'input',
								'options' =>
								[
									'nom' => 'input_2',
								],
							],
						]
					],
					[
						'saisie' => 'input',
						'options' =>
						[
							'nom' => 'input_a_deplacer'
						]
					],
				],
				'input_a_deplacer',
				'[fieldset][1]'
			],
			// Plein de tests inspirés de retour de tcharlss en https://git.spip.net/spip-contrib-extensions/saisies/pulls/156
			[
				[
					[
						'saisie' => 'input',
						'options' =>
						[
							'nom' => 'cible',
							'label' => 'Saisie déplacée',
							'conteneur_class' => 'highlight',
						],
					],
					[
						'saisie' => 'input',
						'options' =>
						[
							'nom' => 'racine1',
							'label' => 'Racine 1',
						],
					],
					[
						'saisie' => 'fieldset',
						'options' =>
						[
							'nom' => 'parent',
						],
						'saisies' =>
						[
							[
								'saisie' => 'input',
								'options' =>
								[
									'nom' => 'enfant1',
									'label' => 'Enfant 1',
								],
							],
							[
								'saisie' => 'input',
								'options' =>
								[
									'nom' => 'enfant2',
									'label' => 'Enfant 2',
								],
							],
						],
					],
					[
						'saisie' => 'input',
						'options' =>
						[
							'nom' => 'racine2',
							'label' => 'Racine 2',
						],
					],
				],
				[
					[
						'saisie' => 'input',
						'options' =>
						[
							'nom' => 'racine1',
							'label' => 'Racine 1',
						],
					],
					[
						'saisie' => 'fieldset',
						'options' =>
						[
							'nom' => 'parent',
						],
						'saisies' =>
						[
							[
								'saisie' => 'input',
								'options' =>
								[
									'nom' => 'enfant1',
									'label' => 'Enfant 1',
								],
							],
							[
								'saisie' => 'input',
								'options' =>
								[
									'nom' => 'enfant2',
									'label' => 'Enfant 2',
								],
							],
						],
					],
					[
						'saisie' => 'input',
						'options' =>
						[
							'nom' => 'racine2',
							'label' => 'Racine 2',
						],
					],
					[
						'saisie' => 'input',
						'options' =>
						[
							'nom' => 'cible',
							'label' => 'Saisie déplacée',
							'conteneur_class' => 'highlight',
						],
					],
				],
				'cible',
				'racine1',
			],
			[
				[
					[
						'saisie' => 'input',
						'options' =>
						[
							'nom' => 'racine1',
							'label' => 'Racine 1',
						],
					],
					[
						'saisie' => 'fieldset',
						'options' =>
						[
							'nom' => 'parent',
						],
						'saisies' =>
						[
							[
								'saisie' => 'input',
								'options' =>
								[
									'nom' => 'cible',
									'label' => 'Saisie déplacée',
									'conteneur_class' => 'highlight',
								],
							],
							[
								'saisie' => 'input',
								'options' =>
								[
									'nom' => 'enfant1',
									'label' => 'Enfant 1',
								],
							],
							[
								'saisie' => 'input',
								'options' =>
								[
									'nom' => 'enfant2',
									'label' => 'Enfant 2',
								],
							],
						],
					],
					[
						'saisie' => 'input',
						'options' =>
						[
							'nom' => 'racine2',
							'label' => 'Racine 2',
						],
					],
				],
				[
					[
						'saisie' => 'input',
						'options' =>
						[
							'nom' => 'racine1',
							'label' => 'Racine 1',
						],
					],
					[
						'saisie' => 'fieldset',
						'options' =>
						[
							'nom' => 'parent',
						],
						'saisies' =>
						[
							[
								'saisie' => 'input',
								'options' =>
								[
									'nom' => 'enfant1',
									'label' => 'Enfant 1',
								],
							],
							[
								'saisie' => 'input',
								'options' =>
								[
									'nom' => 'enfant2',
									'label' => 'Enfant 2',
								],
							],
						],
					],
					[
						'saisie' => 'input',
						'options' =>
						[
							'nom' => 'racine2',
							'label' => 'Racine 2',
						],
					],
					[
						'saisie' => 'input',
						'options' =>
						[
							'nom' => 'cible',
							'label' => 'Saisie déplacée',
							'conteneur_class' => 'highlight',
						],
					],
				],
				'cible',
				'enfant1',
			],
			[
				[
					[
						'saisie' => 'input',
						'options' =>
						[
							'nom' => 'racine1',
							'label' => 'Racine 1',
						],
					],
					[
						'saisie' => 'fieldset',
						'options' =>
						[
							'nom' => 'parent',
						],
						'saisies' =>
						[
							[
								'saisie' => 'input',
								'options' =>
								[
									'nom' => 'enfant1',
									'label' => 'Enfant 1',
								],
							],
							[
								'saisie' => 'input',
								'options' =>
								[
									'nom' => 'cible',
									'label' => 'Saisie déplacée',
									'conteneur_class' => 'highlight',
								],
							],
							[
								'saisie' => 'input',
								'options' =>
								[
									'nom' => 'enfant2',
									'label' => 'Enfant 2',
								],
							],
						],
					],
					[
						'saisie' => 'input',
						'options' =>
						[
							'nom' => 'racine2',
							'label' => 'Racine 2',
						],
					],
				],
				[
					[
						'saisie' => 'input',
						'options' =>
						[
							'nom' => 'racine1',
							'label' => 'Racine 1',
						],
					],
					[
						'saisie' => 'fieldset',
						'options' =>
						[
							'nom' => 'parent',
						],
						'saisies' =>
						[
							[
								'saisie' => 'input',
								'options' =>
								[
									'nom' => 'enfant1',
									'label' => 'Enfant 1',
								],
							],
							[
								'saisie' => 'input',
								'options' =>
								[
									'nom' => 'enfant2',
									'label' => 'Enfant 2',
								],
							],
						],
					],
					[
						'saisie' => 'input',
						'options' =>
						[
							'nom' => 'racine2',
							'label' => 'Racine 2',
						],
					],
					[
						'saisie' => 'input',
						'options' =>
						[
							'nom' => 'cible',
							'label' => 'Saisie déplacée',
							'conteneur_class' => 'highlight',
						],
					],
				],
				'cible',
				'enfant2',
			],
			[
				[
					[
						'saisie' => 'input',
						'options' =>
						[
							'nom' => 'racine1',
							'label' => 'Racine 1',
						],
					],
					[
						'saisie' => 'input',
						'options' =>
						[
							'nom' => 'cible',
							'label' => 'Saisie déplacée',
							'conteneur_class' => 'highlight',
						],
					],
					[
						'saisie' => 'fieldset',
						'options' =>
						[
							'nom' => 'parent',
						],
						'saisies' =>
						[
							[
								'saisie' => 'input',
								'options' =>
								[
									'nom' => 'enfant1',
									'label' => 'Enfant 1',
								],
							],
							[
								'saisie' => 'input',
								'options' =>
								[
									'nom' => 'enfant2',
									'label' => 'Enfant 2',
								],
							],
						],
					],
					[
						'saisie' => 'input',
						'options' =>
						[
							'nom' => 'racine2',
							'label' => 'Racine 2',
						],
					],
				],
				[
					[
						'saisie' => 'input',
						'options' =>
						[
							'nom' => 'racine1',
							'label' => 'Racine 1',
						],
					],
					[
						'saisie' => 'fieldset',
						'options' =>
						[
							'nom' => 'parent',
						],
						'saisies' =>
						[
							[
								'saisie' => 'input',
								'options' =>
								[
									'nom' => 'enfant1',
									'label' => 'Enfant 1',
								],
							],
							[
								'saisie' => 'input',
								'options' =>
								[
									'nom' => 'enfant2',
									'label' => 'Enfant 2',
								],
							],
						],
					],
					[
						'saisie' => 'input',
						'options' =>
						[
							'nom' => 'racine2',
							'label' => 'Racine 2',
						],
					],
					[
						'saisie' => 'input',
						'options' =>
						[
							'nom' => 'cible',
							'label' => 'Saisie déplacée',
							'conteneur_class' => 'highlight',
						],
					],
				],
				'cible',
				'parent',
			],
			[
				[
					[
						'saisie' => 'input',
						'options' =>
						[
							'nom' => 'racine1',
							'label' => 'Racine 1',
						],
					],
					[
						'saisie' => 'fieldset',
						'options' =>
						[
							'nom' => 'parent',
						],
						'saisies' =>
						[
							[
								'saisie' => 'input',
								'options' =>
								[
									'nom' => 'enfant1',
									'label' => 'Enfant 1',
								],
							],
							[
								'saisie' => 'input',
								'options' =>
								[
									'nom' => 'enfant2',
									'label' => 'Enfant 2',
								],
							],
						],
					],
					[
						'saisie' => 'input',
						'options' =>
						[
							'nom' => 'cible',
							'label' => 'Saisie déplacée',
							'conteneur_class' => 'highlight',
						],
					],
					[
						'saisie' => 'input',
						'options' =>
						[
							'nom' => 'racine2',
							'label' => 'Racine 2',
						],
					],
				],
				[
					[
						'saisie' => 'input',
						'options' =>
						[
							'nom' => 'racine1',
							'label' => 'Racine 1',
						],
					],
					[
						'saisie' => 'fieldset',
						'options' =>
						[
							'nom' => 'parent',
						],
						'saisies' =>
						[
							[
								'saisie' => 'input',
								'options' =>
								[
									'nom' => 'enfant1',
									'label' => 'Enfant 1',
								],
							],
							[
								'saisie' => 'input',
								'options' =>
								[
									'nom' => 'enfant2',
									'label' => 'Enfant 2',
								],
							],
						],
					],
					[
						'saisie' => 'input',
						'options' =>
						[
							'nom' => 'racine2',
							'label' => 'Racine 2',
						],
					],
					[
						'saisie' => 'input',
						'options' =>
						[
							'nom' => 'cible',
							'label' => 'Saisie déplacée',
							'conteneur_class' => 'highlight',
						],
					],
				],
				'cible',
				'racine2',
			],
			[
				[
					[
						'saisie' => 'input',
						'options' =>
						[
							'nom' => 'racine1',
							'label' => 'Racine 1',
						],
					],
					[
						'saisie' => 'fieldset',
						'options' =>
						[
							'nom' => 'parent',
						],
						'saisies' =>
						[
							[
								'saisie' => 'input',
								'options' =>
								[
									'nom' => 'cible',
									'label' => 'Saisie déplacée',
									'conteneur_class' => 'highlight',
								],
							],
							[
								'saisie' => 'input',
								'options' =>
								[
									'nom' => 'enfant1',
									'label' => 'Enfant 1',
								],
							],
							[
								'saisie' => 'input',
								'options' =>
								[
									'nom' => 'enfant2',
									'label' => 'Enfant 2',
								],
							],
						],
					],
					[
						'saisie' => 'input',
						'options' =>
						[
							'nom' => 'racine2',
							'label' => 'Racine 2',
						],
					],
				],
				[
					[
						'saisie' => 'input',
						'options' =>
						[
							'nom' => 'racine1',
							'label' => 'Racine 1',
						],
					],
					[
						'saisie' => 'fieldset',
						'options' =>
						[
							'nom' => 'parent',
						],
						'saisies' =>
						[
							[
								'saisie' => 'input',
								'options' =>
								[
									'nom' => 'enfant1',
									'label' => 'Enfant 1',
								],
							],
							[
								'saisie' => 'input',
								'options' =>
								[
									'nom' => 'enfant2',
									'label' => 'Enfant 2',
								],
							],
						],
					],
					[
						'saisie' => 'input',
						'options' =>
						[
							'nom' => 'racine2',
							'label' => 'Racine 2',
						],
					],
					[
						'saisie' => 'input',
						'options' =>
						[
							'nom' => 'cible',
							'label' => 'Saisie déplacée',
							'conteneur_class' => 'highlight',
						],
					],
				],
				'cible',
				'[parent][0]',
			],
			[
				[
					[
						'saisie' => 'input',
						'options' =>
						[
							'nom' => 'racine1',
							'label' => 'Racine 1',
						],
					],
					[
						'saisie' => 'fieldset',
						'options' =>
						[
							'nom' => 'parent',
						],
						'saisies' =>
						[
							[
								'saisie' => 'input',
								'options' =>
								[
									'nom' => 'enfant1',
									'label' => 'Enfant 1',
								],
							],
							[
								'saisie' => 'input',
								'options' =>
								[
									'nom' => 'cible',
									'label' => 'Saisie déplacée',
									'conteneur_class' => 'highlight',
								],
							],
							[
								'saisie' => 'input',
								'options' =>
								[
									'nom' => 'enfant2',
									'label' => 'Enfant 2',
								],
							],
						],
					],
					[
						'saisie' => 'input',
						'options' =>
						[
							'nom' => 'racine2',
							'label' => 'Racine 2',
						],
					],
				],
				[
					[
						'saisie' => 'input',
						'options' =>
						[
							'nom' => 'racine1',
							'label' => 'Racine 1',
						],
					],
					[
						'saisie' => 'fieldset',
						'options' =>
						[
							'nom' => 'parent',
						],
						'saisies' =>
						[
							[
								'saisie' => 'input',
								'options' =>
								[
									'nom' => 'enfant1',
									'label' => 'Enfant 1',
								],
							],
							[
								'saisie' => 'input',
								'options' =>
								[
									'nom' => 'enfant2',
									'label' => 'Enfant 2',
								],
							],
						],
					],
					[
						'saisie' => 'input',
						'options' =>
						[
							'nom' => 'racine2',
							'label' => 'Racine 2',
						],
					],
					[
						'saisie' => 'input',
						'options' =>
						[
							'nom' => 'cible',
							'label' => 'Saisie déplacée',
							'conteneur_class' => 'highlight',
						],
					],
				],
				'cible',
				'[parent][1]',
			]
		];
	}

	/**
	 * @dataProvider dataDeplacerAvant
	 */
	public function testDeplacerAvant($expected, $saisies, $chemin, $ou = '') {
		$actual = saisies_supprimer_identifiants(saisies_deplacer_avant($saisies, $chemin, $ou));
		$this->assertEquals($expected, $actual);
	}

	public static function dataModifier() {
		return [
			'remplacer' => [
				// Expected
				[
					[
						'saisie' => 'input',
						'options' => [
							'label' => 'nouveaulabel',
							'nom' => 'nouveaunom',
						],
						'verifier' => [],
					]
				],
				// Provided
				[
					[
						'saisie' => 'input',
						'options' => [
							'nom' => 'input',
							'label' => 'label',
						],
						'verifier' => [],
					]
				],
				'input',
				[
					'options' => [
						'label' => 'nouveaulabel',
						'nom' => 'nouveaunom',
					],
				],
			],
			'fusion_profond' => [
				// Expected
				[
					[
						'saisie' => 'fieldset',
						'saisies' => [
							[
								'saisie' => 'input',
								'options' => [
									'nom' => 'input',
									'label' => 'label',
									'type' => 'hop',
								]
							]
						]
					]
				],
					// Provided
					[
						[
							'saisie' => 'fieldset',
							'saisies' => [
								[
									'saisie' => 'input',
									'options' => [
										'nom' => 'input',
										'label' => 'label',
									]
								]
							]
						]
					],
					'input',
					[
						'options' => [
							'type' => 'hop',
						]
					],
					true
				],
				'nouveau_type' => [
					// Expected
					[
						[
							'saisie' => 'textarea',
							'options' => [
								'nom' => 'input',
								'label' => 'label',
							]
						]
					],
				// Provided
				[
					[
						'saisie' => 'input',
						'options' => [
							'nom' => 'input',
							'label' => 'label',
						]
					]
				],
				'input',
				[
					'nouveau_type_saisie' => 'textarea'
				],
				true
			],
			'nouveau_type_vieille_syntaxe' => [
				// Expected
				[
					[
						'saisie' => 'textarea',
						'options' => [
							'nom' => 'input',
							'label' => 'label',
						]
					]
				],
				// Provided
				[
					[
						'saisie' => 'input',
						'options' => [
							'nom' => 'input',
							'label' => 'label',
						]
					]
				],
				'input',
				[
					'options' => ['nouveau_type_saisie' => 'textarea']
				],
				true
			],
			'remplacer_enfants' => [
				// Expected
				[
					[
						'saisie' => 'fieldset',
						'options' => [
							'label' => 'fieldset',
							'nom' => 'fieldset',
						],
						'saisies' => [
							[
								'saisie' => 'input_noueau'
							]
						]
					]
				],
				// Saisies
				[
					[
						'saisie' => 'fieldset',
						'options' => [
							'label' => 'fieldset',
							'nom' => 'fieldset',
						],
						'saisies' => [
							[
								'saisie' => 'input_ancien'
							]
						]
					]
				],
				// id_ou_nom_ou_chemin
				'fieldset',
				// modifs
				[
					'saisie' => 'fieldset',
					'options' => [
						'label' => 'fieldset'
					],
					'saisies' => [
						[
							'saisie' => 'input_noueau'
						]
					]
				]
			],
			'remplacer_pas_enfant' => [
				// Expected
				[
					[
						'saisie' => 'fieldset',
						'options' => [
							'label' => 'fieldset_toto',
							'nom' => 'fieldset',
						],
						'saisies' => [
							[
								'saisie' => 'input_ancien'
							]
						]
					]
				],
				// Saisies
				[
					[
						'saisie' => 'fieldset',
						'options' => [
							'label' => 'fieldset',
							'nom' => 'fieldset',
						],
						'saisies' => [
							[
								'saisie' => 'input_ancien'
							]
						]
					]
				],
				// id_ou_nom_ou_chemin
				'fieldset',
				// modifs
				[
					'saisie' => 'fieldset',
					'options' => [
						'label' => 'fieldset_toto'
					],
				]
			]
		];
	}
	/**
	 * @dataProvider dataModifier
	 */
	public function testModifier($expected, $saisies, $id_ou_nom_ou_chemin, $modifs, $fusion = false) {
		$actual = saisies_modifier($saisies, $id_ou_nom_ou_chemin, $modifs, $fusion);
		$this->assertEquals($expected, $actual);
	}

	public static function dataSupprimerOption() {
		return [
			'recursif' => [
				[
					[
						'saisie' => 'fieldset',
						'options' => [],
						'saisies' => [
							[
								'saisie' => 'fieldset',
								'options' => [],
							]
						]
					]
				],
				[
					[
						'saisie' => 'fieldset',
						'options' => ['label' => 'toto'],
						'saisies' => [
							[
								'saisie' => 'fieldset',
								'options' => ['label' => 'toto'],
							]
						]
					]
				],
				'label'
			],
			'pasrecursif' => [
				[
					[
						'saisie' => 'fieldset',
						'options' => [],
						'saisies' => [
							[
								'saisie' => 'fieldset',
								'options' => ['label'=>'toto'],
							]
						]
					]
				],
				[
					[
						'saisie' => 'fieldset',
						'options' => ['label' => 'toto'],
						'saisies' => [
							[
								'saisie' => 'fieldset',
								'options' => ['label' => 'toto'],
							]
						]
					]
				],
				'label',
				false
			]
		];
	}

	/**
	 * @dataProvider dataSupprimerOption
	 */
	public function testSupprimerOption($expected, $saisies, $option, $recursif = true) {
		$actual = saisies_supprimer_option($saisies, $option, $recursif);
		$this->assertEquals($expected, $actual);
	}

	public static function dataTransformerNomsAuto() {
		return [
			'fieldset' => [
				// Expected
				[[
					'saisie' => 'fieldset',
					'options' => [
						'nom' => 'fieldset_1',
						'label' => 'Main fieldset',
					],
					'saisies' => [
						[
							'saisie' => 'input',
							'options' => [
								'nom' => 'input_2',
								'label' => 'Input in fieldset',
							]
						],
					]
				]],
				// Provided form
				[
					[
						'saisie' => 'input',
						'options' => [
							'nom' => 'input_1',
						]

					],
					[
						'saisie' => 'fieldset',
						'options' => [
							'nom' => 'fieldset',
							'label' => 'Main fieldset',
						],
						'saisies' => [
							[
								'saisie' => 'input',
								'options' => [
									'nom' => 'input',
									'label' => 'Input in fieldset',
								]
							],
						]
					]
				],
				// Provided saisies
				[
					[
						'saisie' => 'fieldset',
						'options' => [
							'nom' => 'fieldset',
							'label' => 'Main fieldset',
						],
						'saisies' => [
							[
								'saisie' => 'input',
								'options' => [
									'nom' => 'input',
									'label' => 'Input in fieldset',
								]
							],
						]
					],
				]
			]
		];
	}

	/**
	 * @dataProvider dataTransformerNomsAuto
	 */
	public function testTransformerNomsAuto($expected, $formulaire, $saisie) {
		$actual = saisies_transformer_noms_auto($formulaire, $saisie);
		$this->assertEquals($expected, $actual);
	}

	public static function dataInsererHtml() {
		return
			[
				'debut' => [
					// Expected
					[
						'saisie' => 'input',
						'options' => ['inserer_debut' => '<strong>hop</strong>']
					],
					//Provided
					[
						'saisie' => 'input',
					],
					'<strong>hop</strong>',
					'debut',
				],
				'fin' => [
					// Expected
					[
						'saisie' => 'input',
						'options' => ['inserer_fin' => '<strong>hop</strong>']
					],
					//Provided
					[
						'saisie' => 'input',
					],
					'<strong>hop</strong>',
					'fin',
				],
				'sans_precision' => [
					// Expected
					[
						'saisie' => 'input',
						'options' => ['inserer_fin' => '<strong>hop</strong>']
					],
					//Provided
					[
						'saisie' => 'input',
					],
					'<strong>hop</strong>'
				]
			];
	}

	/**
	 * @dataProvider dataInsererHtml
	 */
	public function testInsererHtml($expected, $saisie, $html, $ou = '') {
		$actual = saisies_inserer_html($saisie, $html, $ou);
		$this->assertEquals($expected, $actual);
	}

	public static function dataWrapperFieldset() {
		return [
			'basic' => [
				// Expected
				[
					'options' => ['optionglobal' => 'global'],
					[
						'saisie' => 'fieldset',
						'options' => ['optionsfieldset' => 'fieldset'],
						'saisies' => [
							[
								'saisie' => 'a'
							]
						]
					]
				],
				// Saisies
				[
					'options' => ['optionglobal' => 'global'],
					[
						'saisie' => 'a'
					]
				],
				// Options
				['optionsfieldset' => 'fieldset'],
			]
		];
	}
	/**
	 * @dataProvider dataWrapperFieldset
	**/
	public function testWrapperFieldset($expected, $saisies, $options) {
		$actual = saisies_wrapper_fieldset($saisies, $options);
		$this->assertEquals($expected, $actual);
	}

	public static function dataMapperVerifier() {
		return [
			'basic' => [
				// Expected
				[
					[
						'saisie' => 'a',
						'verifier' => [
							'a' => []
						]
					],
					[
						'saisie' => 'a',
						'verifier' => [
							'a' => []
						]
					],
					[
						'saisie' => 'a',
						'verifier' => [
							[
								'type' => 'vieillesyntaxe'
							],
							'a' => []
						]
					]
				],
				// Provided
				[
					[
						'saisie' => 'a',
						'verifier' => [
						]
					],
					[
						'saisie' => 'a',
					],
					[
						'saisie' => 'a',
						'verifier' => [
							'type' => 'vieillesyntaxe'
						]
					]
				],
			],
			'recursif' => [
				// Expected
				[
					['saisies' => [
						[
							'saisie' => 'a',
							'verifier' => [
								'a' => []
							]
						],
						[
							'saisie' => 'a',
							'verifier' => [
								'a' => []
							]
						],
						[
							'saisie' => 'a',
							'verifier' => [
								'dejala' => [],
								'a' => []
							]
						]
					]
					]
				],
				// Provided
				[
					['saisies' => [
						[
							'saisie' => 'a',
							'verifier' => [
							]
						],
						[
							'saisie' => 'a',
						],
						[
							'saisie' => 'a',
							'verifier' => [
								'dejala' => []
							]
						]
					],
					]
				]
			]
		];
	}

	/**
	 * @dataProvider dataMapperVerifier
	**/
	public function testManipulerVerifier($expected, $saisies) {
		$actual = saisies_mapper_verifier($saisies, 'Spip\Saisies\Tests\add_verifier_homonymous_2_type');
		$this->assertEquals($expected, $actual);
	}
}


/**
 * Used for testManipulerVerifier
**/
function add_verifier_homonymous_2_type($verifier, $saisie) {
	return array_merge($verifier, [$saisie['saisie'] => []]);
}
