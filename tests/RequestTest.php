<?php

namespace Spip\Saisies\Tests;

use PHPUnit\Framework\TestCase;

/**
 * @covers saisies_request()
 * @covers saisies_set_request()
 * @covers saisies_get_valeur_saisie()
 * @covers saisies_liste_set_request()
 * @uses saisie_nom2name()
 * @uses saisies_saisie_est_fichier()
 */
class RequestTest extends TestCase {

	public static function dataRequest() {
		pseudo_request('set', 'tableau_request', ['a' => ['b' => 'c']]);
		pseudo_request('set', 'plat_request', 'a');

		return [
			'tableau_request' => [
				'c',
				'tableau_request[a][b]',
			],
			'plat_request' => [
				'a',
				'plat_request'
			]
		];
	}

	/**
	 * @dataProvider dataRequest
	 */
	public function testRequest($expected, $variable) {
		$actual = saisies_request($variable);
		$this->assertEquals($expected, $actual);
	}


	public static function dataSetRequest() {
		return [
			'tableau_request2' => [
				'c',
				'tableau_request[a][b]',
			],
			'plat_request2' => [
				'a',
				'plat_request'
			]
		];
	}

	/**
	 * @dataProvider datasetRequest
	 * Note: for this test, we expect that `saisies_request()` work,
	 * as the two features are reciprocal.
	 */
	public function testSetRequest($expected, $variable) {
		saisies_set_request($variable, $expected);
		$actual = saisies_request($variable);
		$this->assertEquals($expected, $actual);
	}

	public static function dataGetValeurSaisie() {
		return [
			'normal' => [
				// Expected
				1,
				// Saisie
				[
					'saisie' => 'input',
					'options' => ['nom' => 'normal']
				],
				// Value in POST
				1
			],
			'tableau_vide' => [
				// Expected
				[],
				// Saisie
				[
					'saisie' => 'date',
					'options' => ['nom' => 'tableau_vide']
				],
				// Value in Post
				['',''],
			],
			'tableau_pas_vide' => [
				// Expected
				[0,0],
				// Saisie
				[
					'saisie' => 'date',
					'options' => ['nom' => 'tableau_pas_vide']
				],
				// Value in Post
				[0,0],
			]
		];
	}

	/**
	 * @dataProvider dataGetValeurSaisie
	**/
	public function testGetValeurSaisie($expected, $saisie, $post_value) {
		$name = $saisie['options']['nom'];
		saisies_set_request($name, $post_value);
		$actual = saisies_get_valeur_saisie($saisie);
		$this->assertEquals($expected, $actual);
	}

	/**
	 * @runInSeparateProcess
	**/
	public function testSaisiesListeSetRequest() {
		set_request('input', 'test0');
		saisies_liste_set_request('set', 'input', 'test1');
		saisies_liste_set_request('set', 'input', 'test2');
		saisies_liste_set_request('set', 'input', 'test3');
		$actual = saisies_liste_set_request('get');
		$this->assertEquals(['test0', 'test1', 'test2', 'test3'], $actual['input']);
	}

}
