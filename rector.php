<?php

declare(strict_types=1);

use Rector\Config\RectorConfig;
use Rector\PHPUnit\Set\PHPUnitSetList;

return RectorConfig::configure()
    ->withPaths([
        __DIR__ . '/action',
        __DIR__ . '/balise',
        __DIR__ . '/champs_extras',
        __DIR__ . '/ecrire',
        __DIR__ . '/formulaires',
        __DIR__ . '/inc',
        __DIR__ . '/inclure',
        __DIR__ . '/lang',
        __DIR__ . '/saisies',
        __DIR__ . '/saisies-vues',
        __DIR__ . '/saisies_afficher_si_js',
        __DIR__ . '/tests',
        __DIR__ . '/verifier',
    ])
    // uncomment to reach your current PHP version
    // ->withPhpSets()
    ->withTypeCoverageLevel(0)
    ->withDeadCodeLevel(0)
		->withCodeQualityLevel(0)
		->withSets([
			PHPUnitSetList::PHPUNIT_110,
			PHPUnitSetList::ANNOTATIONS_TO_ATTRIBUTES
    ]);
